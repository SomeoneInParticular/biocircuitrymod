/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.blocks;

import biocircuitry.BioCircuitry;
import biocircuitry.core.tileentities.BlockTileEntity;
import biocircuitry.core.tileentities.TileBroodBin;
import static biocircuitry.core.tileentities.TileBroodBin.BROOD_BIN_SIZE;

import biocircuitry.client.gui.ModGUIHandler;

import javax.annotation.Nullable;

import biocircuitry.utils.IDebugStickable;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

/**
 * Brood Bin in which worms breed
 * @author SomeoneInParticular
 */
public class BlockBroodBin extends BlockTileEntity<TileBroodBin> implements IDebugStickable {
    /**
     * Build the block with default attributes
     */
    public BlockBroodBin() {
        super(Material.WOOD, "brood_bin");
        super.setCreativeTab(BioCircuitry.CREATIVE_TAB);
        super.setSoundType(SoundType.WOOD);
    }

    /**
     * Fetch the class of tile entities attached to all blocks of this type, a Brood Bin entity
     * @return The brood bin entity class
     */
    @Override
    public Class<TileBroodBin> getTileEntityClass() {
        return TileBroodBin.class;
    }

    /**
     * Build a new tile entity for the block
     * @param world World in which the block exists
     * @param state The target block and its current state
     * @return A Brood Bin tile Entity
     */
    @Nullable
    @Override
    public TileBroodBin createTileEntity(World world, IBlockState state) {
        return new TileBroodBin();
    }

    /**
     * Present the player who use's the Brood Bin with the appropriate GUI
     * @param world world within which the block exists
     * @param pos Position of the block within that world
     * @param state State of the block (in our case, it only has one state)
     * @param player Player interacting with the block
     * @param hand Which hand the player used to interact with the block
     * @param side Side the player interacted with the block from
     * @param hitX Where on the block the activation was triggered (X-position)
     * @param hitY Where on the block the activation was triggered (Y-position)
     * @param hitZ Where on the block the activation was triggered (Z-position)
     * @return Whether the block activation was successful
     */
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            TileBroodBin tile = getTileEntity(world, pos);
            IItemHandler itemHandler = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            ItemStack heldItem = player.getHeldItem(hand);
            if (!player.isSneaking()) {
                player.openGui(BioCircuitry.instance, ModGUIHandler.BROOD_BIN_ID, world, pos.getX(), pos.getY(), pos.getZ());
            }
        }
        return true;
    }

    /**
     * Makes sure the block's contents spill out when it is broken
     * @param world World the block exists within
     * @param pos Position where the block exists within that world
     * @param state State the block was in when it was destroyed
     */
    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileBroodBin tile = getTileEntity(world, pos);
        IItemHandler itemHandler = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.NORTH);
        if (itemHandler != null) {
            for (int i = 0; i < BROOD_BIN_SIZE; i++) {
                ItemStack stack = itemHandler.getStackInSlot(i);
                if (!stack.isEmpty()) {
                    EntityItem item = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), stack);
                    world.spawnEntity(item);
                }
            }
        }
        super.breakBlock(world, pos, state);
    }

    @Override
    public String hitWithDebugStick(World world, BlockPos pos) {
        TileBroodBin bin = this.getTileEntity(world, pos);
        return bin.getDebugString();
    }
}
