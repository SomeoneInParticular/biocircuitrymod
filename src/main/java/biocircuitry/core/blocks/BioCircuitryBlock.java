/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.blocks;

import biocircuitry.BioCircuitry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

import static biocircuitry.BioCircuitry.proxy;

/**
 * Basic block implementation; father of most blocks in this mod
 * @author SomeoneInParticular
 */
public class BioCircuitryBlock extends Block {
    
    protected String name;
    
    public BioCircuitryBlock(Material material, String name) {
        super(material);
        
        this.name = name;
        this.setHardness(1);
        
        setUnlocalizedName(name);
        setRegistryName(name);
        
        super.setCreativeTab(BioCircuitry.CREATIVE_TAB);
    }

    /**
     * Register the block's model for rendering
     * @param itemBlock The item which to register the renderer for
     */
    public void registerItemModel(Item itemBlock) {
        proxy.registerItemRenderer(itemBlock, 0, name);
    }

    /**
     * Build the block's internal representation
     * @return The block's internal representation
     */
    public Item createItemBlock() {
        assert this.getRegistryName() != null;
        return new ItemBlock(this).setRegistryName(this.getRegistryName());
    }
}
