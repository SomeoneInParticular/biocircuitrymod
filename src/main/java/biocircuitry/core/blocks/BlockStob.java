/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.blocks;

import biocircuitry.BioCircuitry;
import biocircuitry.api.ecology.IGenomeFactory;
import biocircuitry.capabilities.GeneticProducer;
import biocircuitry.client.audio.ModSounds;
import biocircuitry.core.items.ModItems;
import biocircuitry.genomics.WormGenome;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.ArrayList;

import static biocircuitry.api.BioCircuitryAPI.WORM_ECOLOGY_REGISTRY;
import static biocircuitry.capabilities.GeneticProducerProvider.GENETIC_PRODUCER_CAPABILITY;

/**
 * A wooden stake, placed in the ground, which generates worms
 * appropriate to the environment its in when hit with a
 * Rooping Iron
 */
public class BlockStob extends BioCircuitryBlock {

    private static final AxisAlignedBB BOUNDING_BOX = new AxisAlignedBB(
            (5d/16d), 0, (5d/16d), (11d/16d), (8d/16d), (11d/16d)
    );

    /**
     * Build the block with default attributes
     */
    public BlockStob() {
        super(Material.CIRCUITS, "stob");
        super.setCreativeTab(BioCircuitry.CREATIVE_TAB);
        super.setSoundType(SoundType.WOOD);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack hittingItem = playerIn.getHeldItem(hand);
        if (!worldIn.isAreaLoaded(pos, 3)) {
            return false;
        }
        // Server side stuff
        if (hittingItem.getItem() == ModItems.ROOPING_IRON) {
            // Run item checks server side only
            if (!worldIn.isRemote) {
                // Identify all items within range
                ArrayList<EntityItem> stackList = (ArrayList<EntityItem>) worldIn.getEntitiesWithinAABB(
                        EntityItem.class, new AxisAlignedBB(
                                (pos.getX() - 3), (pos.getY() - 2), (pos.getZ() - 3),
                                (pos.getX() + 3), (pos.getY() + 2), (pos.getZ() + 3)
                        ));
                // Determine if a worm should spawn, for each item detected prior
                for (EntityItem entityItem : stackList) {
                    Item item = entityItem.getItem().getItem();
                    ArrayList<IGenomeFactory<WormGenome>> validOptions =
                            WORM_ECOLOGY_REGISTRY.getValidFactories(worldIn, pos, playerIn, item);
                    // If no
                    if (validOptions.isEmpty()) {
                        continue;
                    }
                    IGenomeFactory<WormGenome> chosenFactory =
                            validOptions.get(BioCircuitry.RANDOM.nextInt(validOptions.size()));
                    ItemStack newWorm = new ItemStack(ModItems.WORM);
                    GeneticProducer cap = newWorm.getCapability(GENETIC_PRODUCER_CAPABILITY, null);
                    if (cap != null) {
                        cap.setWormGenome(chosenFactory.generateGenome());
                    }
                    // Consume the bait
                    entityItem.getItem().setCount(entityItem.getItem().getCount()-1);
                    // Spawn the worm
                    worldIn.spawnEntity(new EntityItem(worldIn, entityItem.posX, entityItem.posY, entityItem.posZ, newWorm));
                    // Reduce the durability of the item
                    hittingItem.damageItem(1, playerIn);
                }
            }
            if (worldIn.isRemote) {
                float randomPitch = (BioCircuitry.RANDOM.nextInt(30)+80)/60f;
                Minecraft.getMinecraft().world.playSound(
                        pos, ModSounds.WORM_GRUNT_SOUND, SoundCategory.BLOCKS, 0.4f,
                        randomPitch, false
                );
            }
            return true;
        }
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
    }

    @Override
    @Deprecated
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return BOUNDING_BOX;
    }

    @Override
    @Deprecated
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    @Deprecated
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        return super.canPlaceBlockAt(worldIn, pos) && canBlockStay(worldIn, pos);
    }

    private boolean canBlockStay(World world, BlockPos pos) {
        return world.getBlockState(pos.down()).getMaterial().isSolid();
    }

    @Override
    @Deprecated
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }
}
