/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.blocks;

import biocircuitry.BioCircuitry;

import static biocircuitry.capabilities.GeneticProducerProvider.GENETIC_PRODUCER_CAPABILITY;

import biocircuitry.capabilities.GeneticProducer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Omniscient sequencer, useful primarily for debugging
 * @author SomeoneInParticular
 */
public class BlockOmniSequencer extends BioCircuitryBlock {
    // Basic constructor
    public BlockOmniSequencer() {
        super(Material.GLASS, "omni_sequencer");
        
        setHardness(3f);
        setResistance(5f);
    }

    /**
     * Describe the item's details when right clicked on by an item (Placeholder effect)
     * @param world world within which the block exists
     * @param pos Position of the block within that world
     * @param state State of the block (in our case, it only has one state)
     * @param player Player interacting with the block
     * @param hand Which hand the player used to interact with the block
     * @param side Side the player interacted with the block from
     * @param hitX Where on the block the activation was triggered (X-position)
     * @param hitY Where on the block the activation was triggered (Y-position)
     * @param hitZ Where on the block the activation was triggered (Z-position)
     * @return Whether the block activation was successful
     */
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            Minecraft mc = Minecraft.getMinecraft();
            ItemStack held = player.getHeldItem(hand);
            GeneticProducer capability = held.getCapability(GENETIC_PRODUCER_CAPABILITY, null); // Pulled from held item
            if (capability == null) {
                if (held.isEmpty()) {
                    mc.player.sendChatMessage("NO ITEM TO ANALYZE");
                } else {
                    String local = BioCircuitry.proxy.localize(held.getUnlocalizedName() + ".name");
                    mc.player.sendChatMessage(local + " HAS NO GENOME");
                }
            } else {
                mc.player.sendChatMessage("Genome: " + capability.getWormGenome().toString());
                mc.player.sendChatMessage("Alignments: " + capability.createGameteChromList().toString());
            }
        }
        return true;
    }
}
