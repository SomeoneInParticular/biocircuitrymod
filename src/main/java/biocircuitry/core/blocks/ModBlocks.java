/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.blocks;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Registry manager for blocks in the mod
 * @author SomeoneInParticular
 */
public class ModBlocks {
    // Generic block instances for use w/ registration
    public static BlockOmniSequencer omni_sequencer = new BlockOmniSequencer();
    public static BlockBroodBin brood_bin = new BlockBroodBin();
    public static BlockFeedBin feed_bin = new BlockFeedBin();
    public static BlockStob stob = new BlockStob();
    
    /**
     * Register all blocks to the Forge registry
     * @param registry The registry to be registered on
     */
    public static void register(IForgeRegistry<Block> registry) {
        registry.registerAll(
                omni_sequencer,
                brood_bin,
                feed_bin,
                stob
        );

        // Register relevant tile entities
        registerTileEntities();
    }

    /**
     * Register tile entities for the mod
     */
    private static void registerTileEntities() {
        GameRegistry.registerTileEntity(brood_bin.getTileEntityClass(), brood_bin.getRegistryName());
        GameRegistry.registerTileEntity(feed_bin.getTileEntityClass(), feed_bin.getRegistryName());
    }

    /**
     * Register the item models for every block
     * @param registry The registry registering everything
     */
    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(
                omni_sequencer.createItemBlock(),
                brood_bin.createItemBlock(),
                feed_bin.createItemBlock(),
                stob.createItemBlock()
        );
    }

    /**
     * Register the models for use by the game's renderer with the game
     */
    public static void registerModels() {
        omni_sequencer.registerItemModel(Item.getItemFromBlock(omni_sequencer));
        brood_bin.registerItemModel(Item.getItemFromBlock(brood_bin));
        feed_bin.registerItemModel(Item.getItemFromBlock(feed_bin));
        stob.registerItemModel(Item.getItemFromBlock(stob));
    }
}
