/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.proxy;

import biocircuitry.capabilities.GeneticProducer;
import biocircuitry.capabilities.GeneticProducerStorage;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.capabilities.CapabilityManager;

/**
 * Shared proxy, for code needing to be run for both client and server
 * @author SomeoneInParticular
 */
public class ProxyCommon {
    /**
     * Registers an item; placeholder for later usage
     * @param item Item to register
     */
    public void registerItem(Item item) {
        // Does nothing
    }
    
    /**
     * Registers a block; placeholder for later usage
     * @param block Block to be registered
     */
    public void registerBlock(Block block) {
        // Does nothing
    }

    /**
     * Registers a renderer for an item; only used by the client
     * @param item Item to register a renderer for
     * @param meta Type of renderer to register
     * @param id ID the renderer should use
     */
    public void registerItemRenderer(Item item, int meta, String id) {
        // Does nothing
    }

    /**
     * Registers item/block capabilities for the mod
     */
    public void registerCapabilities() {
        CapabilityManager.INSTANCE.register(
                GeneticProducer.class,
                new GeneticProducerStorage(),
                GeneticProducer::new
        );
    }

    /**
     * Creates a translated set of strings for a list of objects
     * @param trans_key The language key to use
     * @param args The objects for which translation is required
     * @return The translated set of strings for the passed objects
     */
    public String localize(String trans_key, Object... args) {
        return new TextComponentTranslation(trans_key, args).getFormattedText();
    }
}
