/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.proxy;

import biocircuitry.BioCircuitry;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.resources.I18n;

import net.minecraft.item.Item;

import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Client code manager
 * @author SomeoneInParticular
 */
@SideOnly(Side.CLIENT)
public class ProxyClient extends ProxyCommon {
    /**
     * Creates the resource location (texture/model) for the designated item
     * @param item Item bound to this mod's renderer
     * @param meta Value indicating how the model should load its items
     * @param id Tag to assign to the resource location
     */
    @Override
    public void registerItemRenderer(Item item, int meta, String id) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(
                BioCircuitry.MOD_ID + ":" + id, "inventory"));
    }
    
    // Localize to client compatible form
    public String localize(String trans_key, Object... args) {
        return I18n.format(trans_key, args);
    }
}
