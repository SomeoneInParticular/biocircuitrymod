/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.items;

import biocircuitry.BioCircuitry;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Basic item implementation for all items in this mod
 * @author SomeoneInParticular
 */
public class BioCircuitryItem extends Item {
    
    protected String name;

    /**
     * Generic constructor
     * @param name Name the item should take within the registry
     */
    public BioCircuitryItem(String name) {
        this.name = name;
        setUnlocalizedName(name);
        setRegistryName(name);
        this.setCreativeTab(BioCircuitry.CREATIVE_TAB);
    }

    /**
     * Register the item's model with Forge
     */
    public void registerItemModel() {
        BioCircuitry.proxy.registerItemRenderer(this, 0, name);
    }

    /**
     * Set the item's creative tab (usually our own custom tab)
     * @param tab The tab in which the item should appear
     * @return The item that was registered
     */
    @Override
    public BioCircuitryItem setCreativeTab(CreativeTabs tab) {
            super.setCreativeTab(tab);
            return this;
    }

}
