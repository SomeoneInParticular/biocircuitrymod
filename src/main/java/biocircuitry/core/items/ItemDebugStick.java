/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.items;

import biocircuitry.BioCircuitry;
import biocircuitry.utils.IDebugStickable;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Simple debugging stick for aiding trouble-shooting in-game
 */
public class ItemDebugStick extends BioCircuitryItem {
    /**
     * Generic constructor
     */
    public ItemDebugStick() {
        super("debug_stick");
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        Block targetBlock = worldIn.getBlockState(pos).getBlock();
        if (targetBlock instanceof IDebugStickable) {
            IDebugStickable block = (IDebugStickable) targetBlock;
            BioCircuitry.LOGGER.info(block.hitWithDebugStick(worldIn, pos));
        } else {
            BioCircuitry.LOGGER.info("BLOCK '" + targetBlock.getLocalizedName() + "' LACKS DEBUG STICK FUNCTIONALITY");
        }
        return EnumActionResult.PASS;
    }
}
