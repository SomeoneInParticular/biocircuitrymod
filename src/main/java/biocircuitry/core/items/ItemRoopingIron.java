/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.items;

public class ItemRoopingIron extends BioCircuitryItem {
    /**
     * Generic constructor
     */
    public ItemRoopingIron() {
        super("rooping_iron");
        this.setMaxDamage(128);
        this.setMaxStackSize(1);
    }

    @Override
    public boolean isDamageable() {
        return true;
    }
}
