/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.items;

import net.minecraft.item.Item;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Handles registration for items in this mod
 * @author SomeoneInParticular
 */
public class ModItems {
    // List of items to register
    public static BioCircuitryItem WORM = new ItemWorm();
    public static BioCircuitryItem DEAD_WORM = new BioCircuitryItem("dead_worm");
    public static BioCircuitryItem DEBUG_STICK = new ItemDebugStick();
    public static BioCircuitryItem ROOPING_IRON = new ItemRoopingIron();
    public static BioCircuitryItem HANDBOOK = new ItemHandbook();
    
    /**
     * Register all items for the mod
     * @param registry The registry registering everything
     */
    public static void register(IForgeRegistry<Item> registry) {
        registry.registerAll(
                WORM, DEAD_WORM, DEBUG_STICK, ROOPING_IRON, HANDBOOK
        );
    }
    
    /**
     * Registers all item models for the mod
     */
    public static void registerModels() {
        WORM.registerItemModel();
        DEAD_WORM.registerItemModel();
        DEBUG_STICK.registerItemModel();
        ROOPING_IRON.registerItemModel();
        HANDBOOK.registerItemModel();
    }
}
