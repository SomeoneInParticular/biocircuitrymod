/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.items;

import biocircuitry.BioCircuitry;
import biocircuitry.client.gui.ModGUIHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemHandbook extends BioCircuitryItem {
    /**
     * Generic constructor
     */
    public ItemHandbook() {
        super("handbook");
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand handIn) {
        player.openGui(BioCircuitry.instance, ModGUIHandler.HANDBOOK_NAVI_ID, world, 0, 0, 0);
        return ActionResult.newResult(EnumActionResult.SUCCESS, player.getHeldItem(handIn));
    }
}
