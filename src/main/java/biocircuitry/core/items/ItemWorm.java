/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.items;

import biocircuitry.capabilities.GeneticProducer;
import biocircuitry.capabilities.GeneticProducerProvider;
import biocircuitry.genomics.structures.WormChromosome;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static biocircuitry.capabilities.GeneticProducerProvider.GENETIC_PRODUCER_CAPABILITY;
import static biocircuitry.core.items.ModItems.WORM;

/**
 * Worm item, bred and used in brood bins for resource refinement
 * @author SomeoneInParticular
 */
public class ItemWorm extends BioCircuitryItem {
    // Generic constructor
    public ItemWorm() {
        super("worm");
        super.setMaxStackSize(1);
        super.setMaxDamage(0);
    }
    
    // Get the damage metadata for the stack
    // Worms don't age or suffer harm currently, so this is simply to prevent misuse
    @Override
    public int getDamage(ItemStack stack) {
        if (super.getDamage(stack) != 0) {
            super.setDamage(stack, 0);
        }
        return 0;
    }

    /**
     * Attach a GeneticProducer capability to the worm, so they can be bred
     * @param stack Worm item to attach the capability too
     * @param old_nbt The original worm's NBT tag, to modify
     * @return A genome capability for the worm
     */
    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, @Nullable NBTTagCompound old_nbt) {
        return new GeneticProducerProvider();
    }

    /**
     * Adds user relevant information to be displayed to the user to the worm
     * @param stack The worm item to add info to
     * @param worldIn The world in which the worm exists
     * @param tooltip The tooltip to attach to the worm
     * @param flagIn The flag designating the type of tooltip to be added
     */
    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        GeneticProducer capability = stack.getCapability(GENETIC_PRODUCER_CAPABILITY, null);

        if (capability != null) {
            tooltip.add(capability.getInfo());
        } else {
            tooltip.add(
                    "According to all known laws of our code, there is no way a worm " +
                    "should be able to live without a genome. It lacks all genes required " +
                    "for it to survive. This worm, of course, does anyways. Because it " +
                    "doesn't care what us developers think is impossible.");
        }
    }

    /**
     * Adds a number of 'representative' variants of worms to the creative tab of our mod
     * @param tab The tab under which to add our worm variants
     * @param items The items to add to said tab
     */
    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (this.isInCreativeTab(tab)) {
            // Worm with the bare necessities
            WormChromosome chrom = new WormChromosome("gcatagttagtccatcggaagaagcctctc" + // start spacer
                    "atgggccgcctgtattgggaagtgattccgtaa" + // walS
                    "ggtaatgggatagtgatatacggtcg" + // spacer
                    "atggaaaaatttcatctgattctgagcttttaa" + // wafS
                    "tcgagtccgagaggcgagcggattcactcagcagagtgatgg" + // spacer
                    "atgaaactggtgacctgggtgatgcagcattaa" + // rnaS
                    "attaagcgtcttta" // end spacer
            );
            ArrayList<WormChromosome> chromList = new ArrayList<>();
            chromList.add(chrom);
            ItemStack subWorm = new ItemStack(WORM);
            GeneticProducer cpblt = subWorm.getCapability(GENETIC_PRODUCER_CAPABILITY, null);
            if (cpblt != null) {
                cpblt.buildFromParents(chromList, chromList);
                items.add(subWorm);
            }
            // Worm with bare necessities in a different order
            chrom = new WormChromosome("gcatagttagtccatcggaagaagcctctc" + // start spacer
                    "atgggccgcctgtattgggaagtgattccgtaa" + // walS
                    "tcgagtccgagaggcgagcggattcactcagcagagtgatgg" + // spacer
                    "atgaaactggtgacctgggtgatgcagcattaa" + // rnaS
                    "ggtaatgggatagtgatatacggtcg" + // spacer
                    "atggaaaaatttcatctgattctgagcttttaa" + // wafS
                    "attaagcgtcttta" // end spacer
            );
            chromList = new ArrayList<>();
            chromList.add(chrom);
            subWorm = new ItemStack(WORM);
            cpblt = subWorm.getCapability(GENETIC_PRODUCER_CAPABILITY, null);
            if (cpblt != null) {
                cpblt.buildFromParents(chromList, chromList);
                items.add(subWorm);
            }
        }
        // Add other sub-items that Forge/debugging tools might require
        super.getSubItems(tab, items);
    }
}
