/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.tileentities;

import static biocircuitry.capabilities.GeneticProducerProvider.GENETIC_PRODUCER_CAPABILITY;

import java.util.ArrayList;

import biocircuitry.capabilities.GeneticProducer;
import biocircuitry.core.items.ModItems;
import biocircuitry.genomics.structures.WormChromosome;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

/**
 * Entity bound to Brood Bin blocks. Enables worm breeding w/ semi-accurate non-Mendelian genetics
 * @author SomeoneInParticular
 */
public class TileBroodBin extends TileEntity implements ITickable {
    // Item to 'feed' the worms when breeding
    @GameRegistry.ObjectHolder("minecraft:sugar")  // Placeholder
    private static final Item SUGAR = null;
    // Worm item (makes sure worms, and only worms, are allowed to breed in the brood bin)
    @GameRegistry.ObjectHolder("biocircuitry:worm")
    private static final Item WORM = null;
    
    // Slot id numbers
    public static final int WORM_SLOT_1 = 0;
    public static final int WORM_SLOT_2 = 1;
    public static final int FEED_SLOT = 2;
    public static final int BREED_SLOT = 3;
    
    // Container slot count
    public static final int BROOD_BIN_SIZE = 4;
    
    // Item handler
    private ItemStackHandler inventory = new ItemStackHandler(BROOD_BIN_SIZE);

    /**
     * Save the entity's data to NBT (for example, on world save)
     * @param tag The data to save to NBT, in NBT tag format
     * @return The resulting tag produced, in compound NBT format
     */
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tag) {
        tag.setTag("inventory", inventory.serializeNBT());
        return super.writeToNBT(tag);
    }

    /**
     * Load the entity's data from an existing NBT tag (for example, on world load)
     * @param tag The data to load, from
     */
    @Override
    public void readFromNBT(NBTTagCompound tag) {
        inventory.deserializeNBT(tag.getCompoundTag("inventory"));
        super.readFromNBT(tag);
    }

    /**
     * Check if the side of the block being tested can hold items
     * @param capability The capability to test against, if any
     * @param facing The face of the block to test
     * @return True if the block face is capable of holding items
     */
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    /**
     * Get the inventory of the brood bin, if requested
     * @param capability The capability to test for
     * @param facing The face being tested for the capability
     * @param <T> The inventory type to be returned
     * @return The inventory or, if other capabilities are attached, that capability instance (if any)
     */
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T)inventory : super.getCapability(capability, facing);
    }

    // -- Debug info -- //
    public String getDebugString() {
        boolean can_breed = (
                this.inventory.getStackInSlot(FEED_SLOT).getItem() == SUGAR &&
                this.inventory.getStackInSlot(WORM_SLOT_1).getItem() == WORM &&
                this.inventory.getStackInSlot(WORM_SLOT_2).getItem() == WORM
        );
        return "CAN_BREED: " + can_breed;
    }

    /**
     * Breed the two worms in the brood bin if they have been fed appropriately (currently with sugar)
     */
    @Override
    public void update() {
        ItemStack sugar = this.inventory.getStackInSlot(FEED_SLOT);
        ItemStack mom = this.inventory.getStackInSlot(WORM_SLOT_1);
        ItemStack dad = this.inventory.getStackInSlot(WORM_SLOT_2);
        ItemStack baby = this.inventory.getStackInSlot(BREED_SLOT);
        if (SUGAR.equals(sugar.getItem())
                && baby.isEmpty()
                && WORM.equals(mom.getItem())
                && WORM.equals(dad.getItem())) {
            baby = new ItemStack(WORM);
            GeneticProducer babyCap = baby.getCapability(GENETIC_PRODUCER_CAPABILITY, null);
            ArrayList<WormChromosome> genome_maternal = this.inventory.getStackInSlot(WORM_SLOT_1).getCapability(GENETIC_PRODUCER_CAPABILITY, null).createGameteChromList();
            ArrayList<WormChromosome> genome_paternal = this.inventory.getStackInSlot(WORM_SLOT_2).getCapability(GENETIC_PRODUCER_CAPABILITY, null).createGameteChromList();
            babyCap.buildFromParents(genome_maternal, genome_paternal);
            sugar.shrink(1);
            if (!babyCap.isAlive()) {
                baby = new ItemStack(ModItems.DEAD_WORM);
            }
            this.inventory.setStackInSlot(BREED_SLOT, baby);
        }
    }
}
