/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.tileentities;

import biocircuitry.core.blocks.BioCircuitryBlock;

import javax.annotation.Nullable;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * Basic implementation for block-bound tile entities in this mod
 * @author SomeoneInParticular
 */
public abstract class BlockTileEntity<TE extends TileEntity> extends BioCircuitryBlock {
    /**
     * Create a new type of tile entity
     * @param material Material type of the entity
     * @param name Name of the entity (untranslated)
     */
    public BlockTileEntity(Material material, String name) {
        super(material, name);
    }
    
    /**
     * Fetch the tile entity instance's class for later user
     * @return the class of the tile entity
     */
    public abstract Class<TE> getTileEntityClass();
    
    /**
     * Get what tile entity is bound to the given block at the provided position
     * @param world World within which to search
     * @param pos Position to check
     * @return The tile entity instance, if any
     */
    public TE getTileEntity(IBlockAccess world, BlockPos pos) {
        return (TE)world.getTileEntity(pos);
    }
    
    /**
     * Check if a block in a given blockstate has the given tile entity
     * @param state Current state of the block
     * @return True (Should be modified by children to be more accurate)
     */
    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }
    
    /**
     * Makes the a new instance of the tile entity for the targeted block in its given state
     * @param world World in which the block exists
     * @param state The target block and its current state
     * @return A new instance of the requested tile entity
     */
    @Nullable
    @Override
    public abstract TE createTileEntity(World world, IBlockState state);
}
