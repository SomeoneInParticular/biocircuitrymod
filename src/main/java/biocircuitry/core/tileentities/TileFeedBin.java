/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.core.tileentities;

import biocircuitry.BioCircuitry;
import biocircuitry.capabilities.GeneticProducer;
import biocircuitry.proteomics.structures.WormPathway;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.CombinedInvWrapper;

import javax.annotation.Nonnull;
import java.util.ArrayList;

import static biocircuitry.capabilities.GeneticProducerProvider.GENETIC_PRODUCER_CAPABILITY;

public class TileFeedBin extends TileEntity implements ITickable {
    // Digestion parameters
    private int currentDigestion = 0;
    private static final int MAX_DIGESTION = 30;

    // Buffer for refreshing items, to avoid spam of the expensive metabolic pathway function
    private int currentRefresh = 0;
    private static final int MAX_REFRESH = 20; // 1 Second

    // Tracked pathway
    private WormPathway pathway = null;

    // Boolean flag to mark whether a change in input/worm slots have occurred
    private boolean hasChanged = true;
    // Boolean flag for whether the pathway is currently valid
    private boolean isPathValid = false;
    // Boolean flag for whether space remains in the output slots for another cycle
    private boolean isOutputOpen = true;

    // -- ItemStack handlers -- //
    // Custom handler for food slot(s)
    private class FoodHandler extends ItemStackHandler {
        // Generic constructor
        public FoodHandler(int slots) {
            super(slots);
        }

        @Nonnull
        @Override
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
            ItemStack retStack = super.insertItem(slot, stack, simulate);
            markChanged();
            return retStack;
        }

        @Nonnull
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            ItemStack retStack =  super.extractItem(slot, amount, simulate);
            markChanged();
            return retStack;
        }
    }

    // Custom handler for worm slot(s)
    private class WormHandler extends ItemStackHandler {

        public WormHandler(int slots) {
            super(slots);
        }

        @Override
        protected void onContentsChanged(int slot) {
            super.onContentsChanged(slot);
            // If the resulting slot is empty, disable the entity outright
            if (this.getStackInSlot(0).isEmpty()) {
                disable();
            } else {
                markChanged();
                checkOutputs();
            }
        }
    }

    // Custom handler for output slot(s)
    private class OutputHandler extends ItemStackHandler {

        public OutputHandler(int slots) {
            super(slots);
        }

        @Nonnull
        @Override
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
            ItemStack retStack = super.insertItem(slot, stack, simulate);
            checkOutputs();
            return retStack;
        }

        @Nonnull
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            ItemStack retStack =  super.extractItem(slot, amount, simulate);
            checkOutputs();
            return retStack;
        }
    }

    // Inventory compartments
    public ItemStackHandler foodInventory = new FoodHandler(4);
    public ItemStackHandler wormInventory = new WormHandler(1);
    public ItemStackHandler productInventory = new OutputHandler(4);

    // Full inventory management
    public CombinedInvWrapper fullInventory = new CombinedInvWrapper(wormInventory, foodInventory, productInventory);

    // -- NBT Functions -- //
    /**
     * Save the entity's data to NBT (for example, on world save)
     * @param tag The data to save to NBT, in NBT tag format
     * @return The resulting tag produced, in compound NBT format
     */
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tag) {
        tag.setTag("worm_inventory", wormInventory.serializeNBT());
        tag.setTag("food_inventory", foodInventory.serializeNBT());
        tag.setTag("product_inventory", productInventory.serializeNBT());
        if (pathway != null) {
            tag.setTag("pathway", pathway.serializeNBT());
        }
        tag.setTag("has_changed", new NBTTagByte(hasChanged ? (byte) 1 : (byte) 0));
        tag.setTag("path_valid", new NBTTagByte(isPathValid ? (byte) 1 : (byte) 0));
        tag.setTag("output_open", new NBTTagByte(isOutputOpen ? (byte) 1 : (byte) 0));
        return super.writeToNBT(tag);
    }

    /**
     * Load the entity's data from an existing NBT tag (for example, on world load)
     * @param tag The data to load, from
     */
    @Override
    public void readFromNBT(NBTTagCompound tag) {
        wormInventory.deserializeNBT(tag.getCompoundTag("worm_inventory"));
        foodInventory.deserializeNBT(tag.getCompoundTag("food_inventory"));
        productInventory.deserializeNBT(tag.getCompoundTag("product_inventory"));
        if (tag.hasKey("pathway")) {
            this.pathway = new WormPathway(tag.getCompoundTag("pathway"));
        }
        this.hasChanged = tag.getBoolean("has_changed");
        this.isPathValid = tag.getBoolean("path_valid");
        this.isOutputOpen = tag.getBoolean("output_open");
        super.readFromNBT(tag);
    }

    // -- Capability Functions -- //
    /**
     * Check if the side of the block being tested can hold items
     * @param capability The capability to test against, if any
     * @param facing The face of the block to test
     * @return True if the block face is capable of holding items
     */
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    /**
     * Get the inventory of the brood bin, if requested
     * @param capability The capability to test for
     * @param facing The face being tested for the capability
     * @param <T> The inventory type to be returned
     * @return The inventory or, if other capabilities are attached, that capability instance (if any)
     */
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T) fullInventory : super.getCapability(capability, facing);
    }

    // -- Controller Functions -- //
    // Marks that the entity has had a (pathway pertinent) change
    public void markChanged() {
        this.hasChanged = true;
    }

    // Toggle to immediately disable the TileEntity, for use on worm removal
    public void disable() {
        this.isPathValid = false;
        this.pathway = null;
    }

    /**
     * Check the given inputs for changes which may require the pathway to be rebuilt.
     * Should be called when a stack of a new item type is introduced, or when a stack
     * has its total item count reduced.
     */
    public void checkInputs() {
        // If no pathway exists simply skip entirely now
        if (pathway == null) {
            return;
        }
        // Check if the items in the input meet the cost prerequisites now
        ArrayList<ItemStack> costs = pathway.getCosts();
        for (ItemStack cost : costs) {
            boolean isSatisfied = false;
            int leftToGet = cost.getCount();
            // Iterate through the food inventory until the cost is satisfied, if possible
            for (int i = 0; i < this.foodInventory.getSlots(); i++) {
                ItemStack foodStack = this.foodInventory.getStackInSlot(i);
                if (foodStack.getItem() == cost.getItem()) {
                    if (leftToGet <= foodStack.getCount()) {
                        isSatisfied = true;
                        break;
                    }
                    else {
                        leftToGet -= foodStack.getCount();
                    }
                }
            }
            // If the cost remained unfulfilled, mark the pathway to be rebuilt
            if (!isSatisfied) {
                this.isPathValid = false;
                this.currentDigestion = 0;
                return;
            }
        }
    }

    // Helper function for updating the pathway based on current entity status
    public void updatePathway() {
        ItemStack worm = wormInventory.getStackInSlot(0);
        GeneticProducer wormCap = worm.getCapability(GENETIC_PRODUCER_CAPABILITY, null);
        ArrayList<ItemStack> foods = getFoods();
        if (wormCap != null) {
            this.pathway = new WormPathway(foods, wormCap.getProteins());
            this.isPathValid = !this.pathway.getCosts().isEmpty();
        } else {
            this.pathway = null;
            this.isPathValid = false;
        }
        if (this.isPathValid) {
            this.checkOutputs();
        } else {
            this.pathway = null; // Clear some memory for efficiency sake
        }
        this.hasChanged = false;
    }

    /**
     * Force check the output slots for an opening.
     * Should be called when a stack is removed from the one of the output slots
     */
    public void checkOutputs() {
        // If no pathway exists, simply skip entirely
        if (pathway == null) {
            return;
        }
        // Track the number of empty slots for later
        int emptyCount = 0;
        for (int i = 0; i < this.productInventory.getSlots(); i++) {
            if (this.productInventory.getStackInSlot(i).isEmpty()) {
                emptyCount++;
            }
        }
        // Check for a spot for each product's existence
        this.isOutputOpen = true;
        for (ItemStack product : this.pathway.getProducts()) {
            int case_val = this.placeInProducts(product.copy(), true);
            switch (case_val) {
                case 1:
                    --emptyCount;
                    if (emptyCount < 0) {
                        this.isOutputOpen = false;
                        return;
                    }
                    break;
                case 2:
                    this.isOutputOpen = false;
                    return;
            }
        }
    }

    // -- Getters/Setters -- //
    public int getCurrentDigestion() {
        return this.currentDigestion;
    }

    public void setCurrentDigestion(int newDigest) {
        this.currentDigestion = newDigest;
    }

    // -- Debug Info -- //
    public String getDebugString() {
        String debugString = "DIGESTION: " + this.currentDigestion + '\n';
        debugString += "ITERATION: " + this.currentRefresh + '\n';
        debugString += "PATH_VALID: " + this.isPathValid + '\n';
        debugString += "OUTPUT_OPEN: " + this.isOutputOpen + '\n';
        debugString += "HAS_CHANGED: " + this.hasChanged;
        return debugString;
    }

    // -- Internal Helper Functions -- //
    // Helper function which gets all food items available to the worm, if it exists
    private ArrayList<ItemStack> getFoods() {
        ArrayList<ItemStack> foods = new ArrayList<>(4);
        for (int i = 0; i < foodInventory.getSlots(); i++) {
            ItemStack food = foodInventory.getStackInSlot(i);
            if (!food.isEmpty()) {
                foods.add(food);
            }
        }
        return foods;
    }

    /**
     * Place an itemstack into the product inventory
     * @param productStack The stack to be placed within the inventory
     * @param isSimulated Whether to actually insert the stack, or simply simulate it
     * @return An integer representing the success:
     *  0: Insertion successful, merged two stacks in doing so
     *  1: Insertion successful, placed the stack into an empty slot
     *  2: Insertion was not possible, stack not placed
     */
    private int placeInProducts(ItemStack productStack, boolean isSimulated) {
        for (int i = 0; i < this.productInventory.getSlots(); i++) {
            ItemStack currentStack = this.productInventory.getStackInSlot(i);
            // If the slot we're on is empty, simply place it there and finish
            if (currentStack.isEmpty()) {
                if (!isSimulated) { this.productInventory.setStackInSlot(i, productStack); }
                // Placed stack via replacing empty slot
                return 1;
            }
            // Otherwise, if the items are the same, try to merge the stacks
            if (currentStack.getItem() == productStack.getItem()) {
                int maxCount = currentStack.getMaxStackSize();
                // If the stacks can be merged, do so, and finish
                if (currentStack.getCount() + productStack.getCount() <= maxCount) {
                    if (!isSimulated) { currentStack.grow(productStack.getCount()); }
                    // Placed stack via merge
                    return 0;
                }
                // Otherwise, max out the stack and continue
                else {
                    if (!isSimulated) {
                        int diff = maxCount - currentStack.getCount();
                        productStack.shrink(diff);
                        currentStack.grow(diff);
                    }
                }
            }
        }
        // If the stack was unable to be placed, return 2 (unable to place stack)
        return 2;
    }

    private void digestFood() {
        // Increment digestion
        ++currentDigestion;
        // If the cycle is complete, produce our products and reset
        if (currentDigestion == MAX_DIGESTION) {
            // Reset the digestion timer
            this.currentDigestion = 0;
            // Remove our costs from the food inventory
            this.consumeInputs();
            // Check to see if the inputs changed enough to warrant rebuilding the pathway
            this.checkInputs();
            // Produce products
            if (pathway != null) {
                for (ItemStack product : pathway.getProducts()) {
                    ItemStack prodCopy = product.copy();
                    this.placeInProducts(prodCopy, false);
                }
            }
            // Check the products for overflow
            this.checkOutputs();
        }
    }

    private void consumeInputs() {
        // Safety check for null pathway
        if (pathway == null) {
            return;
        }
        for (ItemStack cost : pathway.getCosts()) {
            Item item = cost.getItem();
            int required = cost.getCount();
            // Find the slot where the food may be consumed
            for (int i = 0; i < foodInventory.getSlots(); i++) {
                ItemStack foodStack = foodInventory.getStackInSlot(i);
                if (foodStack.getItem() == item) {
                    int leftover = foodStack.getCount() - required;
                    // If the stack alone does not satiate the requirement, empty it and continue
                    if (leftover < 0) {
                        foodInventory.setStackInSlot(i, ItemStack.EMPTY);
                        required = -leftover;
                    }
                    // Otherwise, update the stack and move to the next requirement
                    else {
                        foodStack.setCount(leftover);
                        break;
                    }
                }
            }
        }
        // Mark the tile as changed
        this.hasChanged = true;
    }

    // -- Per-tick Updating -- //
    @Override
    public void update() {
        if (!world.isRemote) {
            // If the tile was modified recently, invalidating the pathway, update it
            if (!isPathValid && hasChanged && !this.wormInventory.getStackInSlot(0).isEmpty()) {
                ++currentRefresh;
                // When the iteration is good-to-go, attempt to update the pathway
                if (currentRefresh == MAX_REFRESH) {
                    this.currentRefresh = 0;
                    // Attempt to update the pathway
                    this.updatePathway();
                    this.markDirty();
                }
            }
            // If the worm is capable of eating something, and we have space for the products, run a cycle
            else if (this.isPathValid && this.isOutputOpen) {
                this.digestFood();
            }
            // Barring that, simply force reset the current digestion and iterator values
            else {
                currentDigestion = 0;
                currentRefresh = 0;
            }
        }
    }
}
