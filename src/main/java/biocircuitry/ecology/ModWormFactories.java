/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.ecology;

import biocircuitry.ecology.overworld.ForestFactory;
import biocircuitry.ecology.overworld.PlainsFactory;

import static biocircuitry.api.BioCircuitryAPI.WORM_ECOLOGY_REGISTRY;

public class ModWormFactories {

    private static ForestFactory FOREST = new ForestFactory();
    private static PlainsFactory PLAINS = new PlainsFactory();

    public static void init() {
        WORM_ECOLOGY_REGISTRY.addAll(
                FOREST, PLAINS
        );
    }
}
