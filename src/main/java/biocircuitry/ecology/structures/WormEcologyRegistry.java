/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.ecology.structures;

import biocircuitry.BioCircuitry;
import biocircuitry.api.ecology.IGenomeFactory;
import biocircuitry.genomics.WormGenome;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;

/**
 * Manages worm generation within the mod, allowing stob worm
 * spawns to be easily determined
 */
public class WormEcologyRegistry {
    private static ArrayList<IGenomeFactory<WormGenome>> data = new ArrayList<>();

    public void addFactory(IGenomeFactory<WormGenome> factory) {
        data.add(factory);
    }

    @SafeVarargs
    public final void addAll(IGenomeFactory<WormGenome>... factories) {
        for (IGenomeFactory<WormGenome> factory : factories) {
            addFactory(factory);
        }
    }

    public ArrayList<IGenomeFactory<WormGenome>> getValidFactories(World world, BlockPos pos, EntityPlayer player, Item bait) {
        ArrayList<IGenomeFactory<WormGenome>> results = new ArrayList<>();
        for (IGenomeFactory<WormGenome> factory : data) {
            BioCircuitry.LOGGER.info(world.toString(), pos.toString(), player.toString(), bait.toString());
            if (factory.canSpawnWorm(world, pos, player, bait)) {
                results.add(factory);
            }
        }
        return results;
    }
}
