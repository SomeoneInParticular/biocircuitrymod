/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.ecology;

import biocircuitry.api.ecology.IGenomeFactory;
import biocircuitry.genomics.WormGenome;
import biocircuitry.genomics.structures.Base;

import java.util.ArrayList;

public abstract class ProtoUniversalFactory implements IGenomeFactory<WormGenome> {
    protected static final ArrayList<ArrayList<Base>> UNIVERSAL_SPACERS = new ArrayList<ArrayList<Base>>() {{
        for (int i = 0; i < 20; i++) {
            add(Base.randomSequence(controlledRandomizer.nextInt(10) + 15, controlledRandomizer));
        }
    }};
}
