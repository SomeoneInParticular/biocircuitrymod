/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.ecology.overworld;

import biocircuitry.api.ecology.IGenomeFactory;
import biocircuitry.genomics.WormGenome;
import biocircuitry.genomics.structures.Base;
import biocircuitry.genomics.structures.WormChromosome;
import biocircuitry.proteomics.ModProteins;
import biocircuitry.proteomics.structures.component.AminoAcid;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import java.util.ArrayList;

public class ForestFactory extends ProtoOverworldFactory {
    // -- Coding sequences -- //
    // Wood metabolism
    private static final ArrayList<Base> rumS = AminoAcid.reverseTranslateRandom(ModProteins.rumS.getSequence());
    private static final ArrayList<Base> celR = AminoAcid.reverseTranslateRandom(ModProteins.celR.getSequence());
    private static final ArrayList<Base> gluR = AminoAcid.reverseTranslateRandom(ModProteins.gluR.getSequence());
    private static final ArrayList<Base> atpS = AminoAcid.reverseTranslateRandom(ModProteins.atpS.getSequence());
    private static final ArrayList<Base> myoF = AminoAcid.reverseTranslateRandom(ModProteins.myoF.getSequence());

    // Keratin Synthase pathway
    private static final ArrayList<Base> proS = AminoAcid.reverseTranslateRandom(ModProteins.proS.getSequence());
    private static final ArrayList<Base> kerS = AminoAcid.reverseTranslateRandom(ModProteins.kerS.getSequence());

    // Output sequences
    // Paper
    private static final ArrayList<Base> defP_F = AminoAcid.reverseTranslateRandom(ModProteins.defP_F.getSequence());
    // Silk (string)
    private static final ArrayList<Base> defP_S = AminoAcid.reverseTranslateRandom(ModProteins.defP_S.getSequence());

    // -- Spacer (non-coding) sequences -- //
    private static final ArrayList<ArrayList<Base>> UNIQUE_SPACERS = new ArrayList<ArrayList<Base>>() {{
        for (int i = 0; i < 3; i++) {
            add(Base.randomSequence(20, controlledRandomizer));
        }
    }};

    // List of valid biomes for this factory
    private static final ArrayList<Biome> validBiomes = new ArrayList<Biome>() {{
        add(Biomes.FOREST);
        add(Biomes.BIRCH_FOREST);
        add(Biomes.MUTATED_FOREST);
        add(Biomes.MUTATED_BIRCH_FOREST);
        add(Biomes.MUTATED_ROOFED_FOREST);
        add(Biomes.FOREST_HILLS);
        add(Biomes.BIRCH_FOREST_HILLS);
        add(Biomes.MUTATED_BIRCH_FOREST_HILLS);
    }};

    @Override
    public boolean canSpawnWorm(World world, BlockPos pos, EntityPlayer player, Item bait) {
        if (validBiomes.contains(world.getBiome(pos))) {
            return bait == Item.getItemFromBlock(Blocks.PLANKS) || bait == Item.getItemFromBlock(Blocks.LOG) || bait == Items.STICK;
        }
        return false;
    }

    @Override
    public WormGenome generateGenome() {
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(buildChrom1());
        wormGenome.addSequence(buildChrom1());
        wormGenome.addSequence(buildChrom2());
        wormGenome.addSequence(buildChrom2());
        return wormGenome;
    }

    private WormChromosome buildChrom1() {
        int indexUniversalSpacer = 0;
        ArrayList<Base> sequence = new ArrayList<>(UNIVERSAL_SPACERS.get(indexUniversalSpacer++));
        sequence.addAll(walS);
        sequence.addAll(UNIVERSAL_SPACERS.get(indexUniversalSpacer++));
        sequence.addAll(wafS);
        sequence.addAll(UNIVERSAL_SPACERS.get(indexUniversalSpacer++));
        sequence.addAll(rnaS);
        sequence.addAll(UNIVERSAL_SPACERS.get(indexUniversalSpacer));
        return new WormChromosome(sequence);
    }

    // Contains the cellulose -> myosin pathway
    private WormChromosome buildChrom2() {
        int indexOverworldSpacer = 0;
        int indexUniqueSpacer = 0;
        // Pathway start (enables wood consumption)
        ArrayList<Base> sequence = new ArrayList<>(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        sequence.addAll(rumS);
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        sequence.addAll(celR);
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        // String synthase pathway (keratin)
        if (IGenomeFactory.randomRoll(50)) {
            sequence.addAll(kerS);
        } else {
            sequence.addAll(UNIQUE_SPACERS.get(indexUniqueSpacer++));
        }
        sequence.addAll(UNIQUE_SPACERS.get(indexUniqueSpacer++));
        // String synthase pathway (meat slurry)
        if (IGenomeFactory.randomRoll(40)) {
            sequence.addAll(proS);
        } else {
            sequence.addAll(UNIQUE_SPACERS.get(indexUniqueSpacer));
        }
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        // Defensive padding allele (paper OR string)
        if (IGenomeFactory.randomRoll(20)) {
            sequence.addAll(defP_S);

        } else {
            sequence.addAll(defP_F);
        }
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        sequence.addAll(gluR);
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        sequence.addAll(atpS);
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer++));
        sequence.addAll(myoF);
        sequence.addAll(OVERWORLD_SPACERS.get(indexOverworldSpacer));
        return new WormChromosome(sequence);
    }
}
