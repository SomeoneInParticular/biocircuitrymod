/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.ecology.overworld;

import biocircuitry.ecology.ProtoUniversalFactory;
import biocircuitry.genomics.structures.Base;
import biocircuitry.proteomics.ModProteins;
import biocircuitry.proteomics.structures.component.AminoAcid;

import java.util.ArrayList;

public abstract class ProtoOverworldFactory extends ProtoUniversalFactory {
    // The overworld variants of the life-sustaining genes
    protected static final ArrayList<Base> walS = AminoAcid.reverseTranslateRandom(ModProteins.walS.getSequence());
    protected static final ArrayList<Base> wafS = AminoAcid.reverseTranslateRandom(ModProteins.wafS.getSequence());
    protected static final ArrayList<Base> rnaS = AminoAcid.reverseTranslateRandom(ModProteins.rnaS.getSequence());

    // A set of spacers
    protected static final ArrayList<ArrayList<Base>> OVERWORLD_SPACERS = new ArrayList<ArrayList<Base>>() {{
        for (int i = 0; i < 20; i++) {
            add(Base.randomSequence(controlledRandomizer.nextInt(10) + 15, controlledRandomizer));
        }
    }};
}
