/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.ecology;

import biocircuitry.BioCircuitry;
import biocircuitry.api.genomics.IGenome;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;

public interface IGenomeFactory<G extends IGenome<?>> {

    Random controlledRandomizer = new Random(109436L);

    static boolean randomRoll(int chance) {
        return BioCircuitry.RANDOM.nextInt(100) < chance;
    }

    /**
     * Determine whether it would be appropriate to generate a genome
     * from this factory
     * @param world World to check
     * @param pos Position in the world to check
     * @param player Player instigating the check
     * @param bait Item being used as bait
     * @return True if the factory's genomes would be appropriate for the conditions
     */
    boolean canSpawnWorm(World world, BlockPos pos, EntityPlayer player, Item bait);

    /**
     * Generate a random genome appropriate to the factory genome
     * @return The generated genome
     */
    G generateGenome();
}
