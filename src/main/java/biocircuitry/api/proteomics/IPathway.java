/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.proteomics;

import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Super-interface which should manage the determination of
 * what outputs (items) should be created given a set of
 * @param <P> Protein type this pathway associates with
 * @param <M> Metabolite type this pathway mediates
 */
public interface IPathway<
        P extends IProtein<?, ?, M>,
        M extends IMetabolite> {
    // Rebuild this pathway from a set of proteins and food items
    void rebuildPathway(Collection<ItemStack> food, Collection<P> proteins);

    // Provide the items required by this pathway
    ArrayList<ItemStack> getCosts();

    // Provide the products produce by this pathway
    ArrayList<ItemStack> getProducts();
}
