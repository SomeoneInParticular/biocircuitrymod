/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.proteomics;

import javafx.util.Pair;
import net.minecraft.item.Item;

import java.util.ArrayList;

/**
 * Registry for nutrients by item. Used when an item
 * is consumed to determine the starting nutrients an
 * entity has to work with, during pathway generation.
 */
public interface IFoodNutrientRegistry<M extends IMetabolite> {
    void registerNutrient(Item item, M nutrient, int amount);

    ArrayList<Pair<M, Integer>> getNutrients(Item item);
}
