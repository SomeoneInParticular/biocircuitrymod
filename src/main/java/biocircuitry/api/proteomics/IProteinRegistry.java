/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.proteomics;

import java.util.Collection;

/**
 * The basic protein registry interface for an entity
 * Allows for custom data storage and querying, by label or sequence
 * @param <P> The type of protein this registry should store
 * @param <L> The label type of the protein
 * @param <S> The sequence type of the protein
 * @param <I> The intermediate product type of the protein
 */
public interface IProteinRegistry<
        P extends IProtein<L, S, I>,
        L extends Comparable<L>,
        S extends Comparable<S>,
        I extends IMetabolite> {

    /**
     * Add a new protein entity to the registry
     * @param value Protein to add
     */
    void add(P value);

    /**
     * Fetch a registered protein by its label
     * @param query Label to query with
     * @return The protein with that label, null if none exists with the label
     */
    P getByLabel(L query);

    /**
     * Fetch a registered protein by its sequence
     * @param query Sequence to query with
     * @return The protein with that sequence, null if none exists with the sequence
     */
    P getBySequence(Collection<S> query);

    /**
     * Find all proteins registered in the registry currently
     * @return A collection of every protein currently registered
     */
    Collection<P> getAll();
}
