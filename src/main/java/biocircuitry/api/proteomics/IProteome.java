/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.proteomics;

import biocircuitry.api.genomics.IGenome;

import java.util.Collection;

public interface IProteome<
        P extends IProtein,
        G extends IGenome> {

    /**
     * Get a collection of all proteins tracked by this proteome
     */
    Collection<P> getProteins();

    /**
     * Get a string representing the position of proteins relative to one another
     * (at the inter- and intra-chromosome level)
     */
    String getProtString();

    /**
     * See if the worm has a proteome which allows it to survive
     * @return True if yes, false otherwise
     */
    boolean isAlive();

    /**
     * Build the list of proteins for this proteome based on an existing genome.
     * You likely want to set the ProtString at the same time.
     * @param genome Genome to build from
     */
    void buildFromGenome(G genome);

}
