/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.proteomics;

/**
 * Simply a data container for intermediate (non-item) products created
 * and used by proteins. Also used to for "flavor" in the user manual.
 * @see IProtein
 */
public interface IMetabolite {
    String getLabel();
}
