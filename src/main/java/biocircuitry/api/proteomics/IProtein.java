/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.proteomics;

import net.minecraft.item.ItemStack;

import java.util.Collection;
import java.util.HashMap;

/**
 * A protein for use in automated biological pathway generation
 * @param <L> Label type, for the name of the sequence (String is recommended)
 * @param <S> Sequence type, for use w/ sequence-based querying (Enum recommended)
 * @param <M> Metabolite type this protein uses/produces when active
 */
public interface IProtein<
        L extends Comparable<L>,
        S extends Comparable<S>,
        M extends IMetabolite> {
    L getLabel();
    Collection<S> getSequence();
    HashMap<M, Integer> getReaction();
    Integer getBiomass();
    Collection<ItemStack> getProducts();
}
