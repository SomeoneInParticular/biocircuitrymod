/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api;

import biocircuitry.api.genomics.IChromosome;
import biocircuitry.api.proteomics.IProtein;

import java.util.Collection;

public interface IGeneticProducer<C extends IChromosome<?>, P extends IProtein<?, ?, ?>> {
    /**
     * Provide a set of chromosomes for use in creating a new capability instance
     */
    Collection<C> createGameteChromList();

    /**
     * Build a new genome from two parental collections of chromosomes
     */
    void buildFromParents(Collection<C> maternal, Collection<C> paternal);

    /**
     * Check whether this entity has the genes/proteins required for life
     */
    boolean isAlive();

    /**
     * Fetch the information for the entity this capability is attached to
     */
    String getInfo();

    /**
     * Fetch the proteins available to this entity
     * @return A collection of proteins; duplicates ARE possible
     */
    Collection<P> getProteins();
}
