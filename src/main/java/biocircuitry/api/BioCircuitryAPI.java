/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api;

import biocircuitry.ecology.structures.WormEcologyRegistry;
import biocircuitry.proteomics.structures.WormFoodNutrientRegistry;
import biocircuitry.proteomics.structures.WormMetabolite;
import biocircuitry.proteomics.structures.WormProteinRegistry;

import java.util.HashMap;

/**
 * Container for all structures which may need to be accessed
 * externally, be it by our mod or another. Don't edit this
 * unless you REALLY know what you're doing
 */
public final class BioCircuitryAPI {
    /**
     * Internal registry for <b>WORM</b> proteins;
     * If you attach a proteomic system to another entity, unless you explicitly designate
     * that it should share the the shame proteins as the worm, it will not catch and you
     * will have a very sad (and likely very dead) entity.
     * @see biocircuitry.api.proteomics.IProteinRegistry
     */
    public static final WormProteinRegistry WORM_PROTEIN_REGISTRY = new WormProteinRegistry();

    /**
     * Tracks the metabolites added by and used within this mod, each acting as an
     * intermediate for reactions, linking various proteins together in metabolic
     * pathways dynamically
     * @see biocircuitry.api.proteomics.IPathway
     */
    public static final HashMap<String, WormMetabolite> WORM_METABOLITE_REGISTRY = new HashMap<>();

    /**
     * Tracks the metabolites provided by any given item when used as a food source for a
     * worm. Aids in determining what foods any given worm can eat, and what byproducts are
     * produced by the digestion. Worm pathway objects are used for this purpose.
     * @see biocircuitry.api.proteomics.IPathway
     * @see biocircuitry.proteomics.structures.WormFoodNutrientRegistry
     */
    public static final WormFoodNutrientRegistry WORM_FOOD_NUTRIENT_REGISTRY = new WormFoodNutrientRegistry();

    /**
     * Tracks worm factories, easing querying during determining when and if certain factories
     * should be used given a context (their ecology)
     * @see biocircuitry.ecology.structures.WormEcologyRegistry
     */
    public static final WormEcologyRegistry WORM_ECOLOGY_REGISTRY = new WormEcologyRegistry();
}
