/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.genomics;

import java.util.Collection;

/**
 * Generate alignments and store the resulting data for later use
 * @author SomeoneInParticular
 */
public interface IAlignment<C extends IChromosome> {
    /**
     * Produce a recombinant chromosome from the alignment's associated chromosomes
     */
    C makeRecombinant();

    /**
     * Fetch the score for the alignment
     * @return An integer representation of how "good" the set alignment is (higher being better)
     */
    int getScore();

    /**
     * Fetch a collection of the chromosomes tracked by this alignment
     * @return All Chromosomes this alignment pertains too (always size >= 2)
     */
    Collection<C> getTrackedChromosomes();
}
