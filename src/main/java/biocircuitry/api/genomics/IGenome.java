/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.genomics;

import java.util.Collection;

/**
 * Genome Capability; Manages genetics of an organism
 * @author SomeoneInParticular
 */
public interface IGenome<C extends IChromosome> {
    /**
     * Get the full set of chromosomes in this genome
     */
    Collection<C> getSomaticChromosomes();
    
    /**
     * Generate a germ-line genome component
     */
    Collection<C> buildGamete();
    
    /**
     * Add a new chromosome to the genome (for use w/ genome editing)
     */
    void addSequence(C chrom);
    
    /**
     * Set the chromosome at position i (for use w/ genome editing)
     */
    void setSequence(int i, C chrom);

    /**
     * Rebuilds the genome from two germ-line chromosome lists (used during breeding)
     * @param chrom_list_1 Maternal contribution
     * @param chrom_list_2 Paternal contribution
     */
    void reformGenome(Collection<C> chrom_list_1, Collection<C> chrom_list_2);


}
