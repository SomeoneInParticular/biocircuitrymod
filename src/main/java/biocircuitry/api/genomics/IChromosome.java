/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.api.genomics;

import java.util.ArrayList;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByteArray;

/**
 * Holds genes and manages recombination + pairing
 * @param <B> The base type the chromosome manages
 *           (I would personally recommend an enum unless you want to do something crazy)
 * @author SomeoneInParticular
 */
public interface IChromosome<B> {
    /**
     * Duplicate a clone of this chromosome
     */
    IChromosome<B> duplicate();
    
    /**
     * Get the full sequence of the chromosome
     * @return The list of all genes this chromosome has
     */
    ArrayList<B> getFullSequence();
    
    /**
     * Get a subsequence of the chromosome
     * @param start_pos Position to start at (inclusive)
     * @param end_pos Position to end at (inclusive)
     * @return Gene at given position (null if out of bounds) 
     */
    ArrayList<B> getSubSequence(int start_pos, int end_pos);
    
    /**
     * Insert gene into given position
     * @param sequence Sequence to insert into the
     * @param pos Position to add it
     * @return Whether the function succeeded or not (false on index error, for example)
     */
    boolean insertSubSequence(ArrayList<B> sequence, int pos);
    
    /**
     * Tack on a gene to the end of the chromosome
     * @param sequence Sequence to add
     * @return Whether the function succeeded or not (false on index error, for example)
     */
    boolean addSequence(ArrayList<B> sequence);
    
    /**
     * Removes the gene at the given position (providing it to the user)
     * @param start_pos Position of the gene to be removed
     * @return The gene removed (null on failure)
     */
    ArrayList<B> exciseSequence(int start_pos, int end_pos);
}
