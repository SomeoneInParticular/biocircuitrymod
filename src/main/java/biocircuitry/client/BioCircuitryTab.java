/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client;

import biocircuitry.BioCircuitry;
import biocircuitry.core.items.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

/**
 * Creative mode tab to hold mod-specific blocks & items, including variants
 * @author SomeoneInParticular
 */
public class BioCircuitryTab extends CreativeTabs {
    
    public BioCircuitryTab() {
        super(BioCircuitry.MOD_ID);
    }

    /**
     * Fetch the icon for this tab (in our case, a generic worm)
     * @return The worm's icon
     */
    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(ModItems.WORM);
    }
}
