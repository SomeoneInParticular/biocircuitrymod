/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.handbook;

import biocircuitry.BioCircuitry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

public class HandbookSectionGUI extends HandbookBaseGUI {
    // Resource location of the page GUI texture
    private static final ResourceLocation HANDBOOK_GUI_TEXTURE = new ResourceLocation(BioCircuitry.MOD_ID,"textures/gui/handbook.png");

    // Notable constant values
    private static final float TITLE_TEXT_SCALE = 1.5f;
    private static final float BODY_TEXT_SCALE = 0.8f;

    private static final int PAGE_WIDTH = (int) (100/BODY_TEXT_SCALE);
    private static final int PAGE_HEIGHT = (int) (150/BODY_TEXT_SCALE);

    // Regex patterns
    private static Pattern paragraphRegex = Pattern.compile("\\$\\(p\\)");

    // Tracking variables
    private String currentSection;
    private int currentPage = 0;
    private ArrayList<String[]> paginatedText;

    // Buttons (see below)
    private PageSwapButton buttonNextPage;
    private PageSwapButton buttonPriorPage;
    private TableOfContentsButton buttonTableOfContentsJump;

    // Constructor
    public HandbookSectionGUI(String section) {
        super();
        this.fontRenderer = Minecraft.getMinecraft().fontRenderer;
        this.currentSection = section;
        String sectionResource = String.format("handbook.%s.content", section);
        String sectionContent = I18n.format(sectionResource);
        if (sectionContent.equals(sectionResource)) {
            this.currentSection = "error";
            sectionContent = I18n.format("handbook.error.content");
        }
        this.paginatedText = paginateText(sectionContent);
    }

    // Next/prior button class
    @SideOnly(Side.CLIENT)
    static class PageSwapButton extends GuiButton {

        private boolean isForward;

        public PageSwapButton(int buttonId, int x, int y, boolean isForward) {
            super(buttonId, x, y, 23, 13, "");
            this.isForward = isForward;
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
            if (this.visible) {
                // Determine if the button is hovered over or not
                boolean flag =
                        mouseX >= this.x &&
                        mouseY >= this.y &&
                        mouseX < (this.x+this.width) &&
                        mouseY < (this.y+this.height);
                // Prepare to draw the button
                GlStateManager.color(1, 1, 1, 1);
                mc.getTextureManager().bindTexture(HANDBOOK_GUI_TEXTURE);
                int i = 0;
                int j = 192;
                // Apply any transforms to our result
                if (flag) {
                    i += 23;
                }
                if (!this.isForward) {
                    j += 13;
                }
                // Draw the textured button
                this.drawTexturedModalRect(this.x, this.y, i, j, 23, 13);
            }
        }
    }

    // -- Internal functions -- //
    /**
     * Draw a (pre-paginated) set of strings as multi-line text
     * @param lines The lines to draw
     * @param x X position
     * @param y Y position
     */
    private void drawMultilineText(String[] lines, int x, int y) {
        // Setup
        float textScale = this.getTextScale();
        // Text drawing
        int xAdjusted = (int) (x/textScale);
        int yAdjusted = (int) (y/textScale);
        int yShift = 0;
        setUniformScale(textScale);
        for (String l : lines) {
            fontRenderer.drawString(l, xAdjusted, yAdjusted+yShift, 0x000000);
            yShift += fontRenderer.FONT_HEIGHT;
        }
        undoUniformScale(textScale);
    }

    private void drawLeftPageText() {
        int x = (this.width - 256) / 2 + 17;
        int y = (this.height - 192) / 2 + 17;
        drawPageText(x, y, currentPage);
    }

    private void drawRightPageText() {
        int x = (this.width - 256) / 2 + 137;
        int y = (this.height - 192) / 2 + 17;
        drawPageText(x, y, currentPage+1);
    }

    /**
     * Determine the text that should appear on each page
     * @param text The text to paginate
     * @return A list of arrays of text, each representing a page of lines
     */
    private ArrayList<String[]> paginateText(String text) {
        // Setup
        ArrayList<String[]> pages = new ArrayList<>();
        int linesPerPage = PAGE_HEIGHT/this.fontRenderer.FONT_HEIGHT;
        // Line delineation
        ArrayList<String> lines = new ArrayList<>();
        String[] paragraphs = paragraphRegex.split(text);
        for (String p : paragraphs) {
            lines.addAll(fontRenderer.listFormattedStringToWidth(p, PAGE_WIDTH));
        }
        // Pagination
        int count = 0;
        ArrayList<String> pageLines = new ArrayList<>();
        for (String l : lines) {
            // Add the line to our page
            pageLines.add(l);
            // Increment the count, and see if we need to reset our page
            ++count;
            if (count % linesPerPage == 0) {
                count = 0;
                pages.add(pageLines.toArray(new String[linesPerPage]));
                pageLines = new ArrayList<>();
            }
        }
        // Write the last page, since it may have not been written above
        if (pageLines.size() != 0 ) {
            pages.add(pageLines.toArray(new String[linesPerPage]));
        }
        // Return
        BioCircuitry.LOGGER.info(Arrays.deepToString(pages.toArray()));
        return pages;
    }

    private void drawPageText(int x, int y, int p) {
        if (p < this.paginatedText.size()) {
            this.drawMultilineText(this.paginatedText.get(p), x, y);
        }
    }

    // -- Element management -- //
    /**
     * Draw the title for the current section
     */
    private void drawTitle() {
        String resource = String.format("handbook.%s.title", currentSection);
        String title = I18n.format(resource);
        int textWidth = (int) (fontRenderer.getStringWidth(title) * TITLE_TEXT_SCALE);
        int xPos = (this.width - textWidth) / 2;
        int yPos = (this.height - 192) / 2 - 22;
        // Calculate the space required for the background component
        int noCenters = textWidth/5 + 1;
        int totalWidth = textWidth + 5;  // 5 Pixel offset for the leftmost buffer
        float xHeader = (float) ((this.width - totalWidth) / 2.0);
        // Draw left component
        this.drawTexturedModalRect(xHeader, yPos, 119, 184, 5, 19);
        // Draw central component(s)
        int offset = 5;
        for (int i = 0; i < noCenters; i++) {
            this.drawTexturedModalRect(xHeader+offset, yPos, 124, 184, 5, 19);
            offset += 5;
        }
        // Draw rightmost component
        this.drawTexturedModalRect(xHeader+offset, yPos, 129, 184, 5, 19);
        setUniformScale(TITLE_TEXT_SCALE);
        int xAdj = (int) ((xPos + 5) / TITLE_TEXT_SCALE);
        int yAdj = (int) ((yPos + 5) / TITLE_TEXT_SCALE);
        fontRenderer.drawString(title, xAdj, yAdj, 0xFFFFFF);
        undoUniformScale(TITLE_TEXT_SCALE);
    }

    @Override
    public void initGui() {
        this.buttonList.clear();

        int i = (this.width - 316) / 2;
        int j = (this.height + 160) / 2;

        this.buttonNextPage = this.addButton(new PageSwapButton(1, i+255, j, true));
        this.buttonPriorPage = this.addButton(new PageSwapButton(2, i+38, j, false));
        this.buttonTableOfContentsJump = this.addButton(new TableOfContentsButton(3, i+150, j));
        this.updateButtons();
    }

    private void updateButtons() {
        this.buttonNextPage.visible = (this.currentPage+2 < this.paginatedText.size());
        this.buttonPriorPage.visible = (this.currentPage > 0);
        this.buttonTableOfContentsJump.visible = true;
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.enabled) {
            if (button.id == this.buttonNextPage.id) {
                this.currentPage += 2;
            } else if (button.id == this.buttonPriorPage.id) {
                this.currentPage -= 2;
            } else if (button.id == this.buttonTableOfContentsJump.id) {
                HandbookDirectoryGUI newGUI = new HandbookDirectoryGUI();
                Minecraft.getMinecraft().displayGuiScreen(newGUI);
            }
            this.updateButtons();
        }
    }

    // -- Getters and Setters -- //

    @Override
    protected float getTextScale() {
        return BODY_TEXT_SCALE;
    }

    /**
     * Draw the GUI, in its entirety
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     * @param partialTicks The partial tick on which to render the GUI
     */
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // Bind the GUI texture
        this.mc.getTextureManager().bindTexture(HANDBOOK_GUI_TEXTURE);
        // Render the dark tint behind the GUI
        this.drawDefaultBackground();
        // Render our pages
        this.drawPagesBackground();
        // Draw the title of the section
        this.drawTitle();
        // Render the text for our page(s)
        this.drawLeftPageText();
        this.drawRightPageText();
        // Render the GUI itself
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
}
