/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.handbook;

import biocircuitry.BioCircuitry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

public class HandbookDirectoryGUI extends HandbookBaseGUI {
    // Notable constant values
    protected static final float TEXT_SCALE = 0.8f;

    private static final int PAGE_HEIGHT = 150;

    // Tracking variables
    private final int BUTTONS_PER_PAGE;
    private int startIndex = 0;

    private static String[] linkedSections = new String[] {
            "intro", "book_usage", "tutorial_finding", "tutorial_breeding", "tutorial_feeding", "genetics_detail",
            "genetic_math", "worm_metabolism"
    };

    // Buttons (see HandbookBaseGUI)
    private HandbookDirectoryGUI.PageSwapButton buttonNextPage;
    private HandbookDirectoryGUI.PageSwapButton buttonPriorPage;

    // -- Modified constructor -- //
    public HandbookDirectoryGUI() {
        super();
        this.fontRenderer = Minecraft.getMinecraft().fontRenderer;
        this.BUTTONS_PER_PAGE = (int) (PAGE_HEIGHT/((this.fontRenderer.FONT_HEIGHT+2)*TEXT_SCALE));
    }

    // -- Element management -- //
    @Override
    public void initGui() {
        this.buttonList.clear();

        // Add forward and backwards buttons
        int x = (this.width - 316) / 2;
        int y = (this.height + 160) / 2;

        this.buttonNextPage = this.addButton(new PageSwapButton(0, x + 255, y, true));
        this.buttonPriorPage = this.addButton(new PageSwapButton(1, x + 38, y, false));

        // Add redirect button(s)
        int xPos = (this.width - 256) / 2 + 16;
        int yPos = (this.height - 192) / 2 + 16;
        // Determine the number of buttons that should appear per page
        for (int idx = 0; idx < linkedSections.length; idx++) {
            // Shift positions as needed
            if (idx % (2* BUTTONS_PER_PAGE) == 0) {
                // Fully reset the position every time two pages are run through
                xPos = (this.width - 256) / 2 + 16;
                yPos = (this.height - 192) / 2 + 16;
            } else if (idx % BUTTONS_PER_PAGE == 0) {
                // Jump to the next page every time the page is filled
                xPos = (this.width) / 2 + 8;
                yPos = (this.height - 192) / 2 + 16;
            }
            // Add the button to our button list
            String linkedSection = linkedSections[idx];
            this.addButton(new HighlightedTextButton(idx+2, xPos, yPos, linkedSection));
            // Increment our position for the next button
            yPos += this.fontRenderer.FONT_HEIGHT;
        }

        this.updateButtons();
    }

    private void updateButtons() {
        // Update the page turning buttons
        this.buttonNextPage.visible = (this.startIndex+ BUTTONS_PER_PAGE *2 < linkedSections.length);
        this.buttonPriorPage.visible = (this.startIndex > 0);
        // Update the section link buttons for the appropriate page
        for (GuiButton button : buttonList) {
            // 2 offset to avoid next/prior buttons
            if (button.id >= startIndex+2 && button.id < startIndex+ BUTTONS_PER_PAGE *2+2) {
                button.visible = true;
            } else if (button.id > 1) {
                button.visible = false;
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.enabled) {
            if (button.id == this.buttonNextPage.id) {
                this.startIndex += BUTTONS_PER_PAGE *2;
            } else if (button.id == this.buttonPriorPage.id) {
                this.startIndex -= BUTTONS_PER_PAGE *2;
            } else {
                String section = ((HighlightedTextButton) (button)).linkedSection;
                try {
                    HandbookSectionGUI newGUI = new HandbookSectionGUI(section);
                    Minecraft.getMinecraft().displayGuiScreen(newGUI);
                } catch (Exception e) {
                    HandbookSectionGUI newGUI = new HandbookSectionGUI("error");
                    Minecraft.getMinecraft().displayGuiScreen(newGUI);
                    BioCircuitry.LOGGER.error("Handbook attempted to load a section which didn't exist:");
                    BioCircuitry.LOGGER.catching(e);
                }
            }
            this.updateButtons();
        }
    }

    // -- Getters and Setters -- //
    @Override
    protected float getTextScale() {
        return TEXT_SCALE;
    }

    // -- Drawing functions -- //
    /**
     * Draw the GUI, in its entirety
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     * @param partialTicks The partial tick on which to render the GUI
     */
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // Bind the GUI texture
        this.mc.getTextureManager().bindTexture(HANDBOOK_GUI_TEXTURE);
        // Render the dark tint behind the GUI
        this.drawDefaultBackground();
        // Render our pages
        this.drawPagesBackground();
        // Render the GUI itself
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
}
