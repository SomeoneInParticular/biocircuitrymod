/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.handbook;

import biocircuitry.BioCircuitry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class HandbookBaseGUI extends GuiScreen {
    // The texture to use for all textures based on this GUI
    protected static final ResourceLocation HANDBOOK_GUI_TEXTURE = new ResourceLocation(BioCircuitry.MOD_ID,"textures/gui/handbook.png");

    // -- Commonly used button types -- //
    /**
     * Button for turning the page in the handbook (depicted as left and right facing curved arrows)
     */
    @SideOnly(Side.CLIENT)
    protected static class PageSwapButton extends GuiButton {

        private boolean isForward;

        public PageSwapButton(int buttonId, int x, int y, boolean isForward) {
            super(buttonId, x, y, 23, 13, "");
            this.isForward = isForward;
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
            if (this.visible) {
                // Determine if the button is hovered over or not
                boolean flag =
                        mouseX >= this.x &&
                                mouseY >= this.y &&
                                mouseX < (this.x+this.width) &&
                                mouseY < (this.y+this.height);
                // Prepare to draw the button
                GlStateManager.color(1, 1, 1, 1);
                mc.getTextureManager().bindTexture(HANDBOOK_GUI_TEXTURE);
                int i = 0;
                int j = 192;
                // Apply any transforms to our result
                if (flag) {
                    i += 23;
                }
                if (!this.isForward) {
                    j += 13;
                }
                // Draw the textured button
                this.drawTexturedModalRect(this.x, this.y, i, j, 23, 13);
            }
        }
    }

    /**
     * Button for returning to the table of contents (depicted as an upward arrow)
     */
    @SideOnly(Side.CLIENT)
    protected static class TableOfContentsButton extends GuiButton {
        // Default constructor
        public TableOfContentsButton(int buttonId, int x, int y) {
            super(buttonId, x, y, 11, 12, "");
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
            if (this.visible) {
                // Determine if the button is hovered over or not
                boolean flag =
                        mouseX >= this.x &&
                                mouseY >= this.y &&
                                mouseX < (this.x+this.width) &&
                                mouseY < (this.y+this.height);
                // Prepare to draw the button
                GlStateManager.color(1, 1, 1, 1);
                mc.getTextureManager().bindTexture(HANDBOOK_GUI_TEXTURE);
                int i = 52;
                int j = 194;
                // Apply any transforms to our result
                if (flag) {
                    i += 12;
                }
                // Draw the textured button
                this.drawTexturedModalRect(this.x, this.y, i, j, 11, 12);
            }
        }
    }

    /**
     * Button which is simply text that, when moused over, gets highlighted
     */
    @SideOnly(Side.CLIENT)
    protected class HighlightedTextButton extends GuiButton {

        public String linkedSection;

        public HighlightedTextButton(int buttonId, int x, int y, String section) {
            // Initialization
            super(buttonId, x, y, "");
            // Determine the text
            this.linkedSection = section;
            String resource = String.format("handbook.%s.title", section);
            this.displayString = I18n.format(resource);
            FontRenderer renderer = Minecraft.getMinecraft().fontRenderer;
            // Single pixel buffers around the text
            this.width = renderer.getStringWidth(this.displayString) + 1;
            this.height = renderer.FONT_HEIGHT;
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
            // Setup
            float textScale = getTextScale();
            if (this.visible) {
                // Determine if the button is hovered over or not
                boolean flag =
                        mouseX >= this.x &&
                                mouseY >= this.y &&
                                mouseX < (this.x+this.width) &&
                                mouseY < (this.y+this.height);
                // Prepare to draw the button
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                // Draw the backdrop if moused over
                if (flag) {
                    int scaledWidth = (int) (this.width*textScale);
                    int scaledHeight = (int) (this.height*textScale);
                    drawRect(this.x, this.y, this.x+scaledWidth, this.y+scaledHeight, 0xAA79C0FF);
                }
                // Draw the label
                setUniformScale(textScale);
                int scaledX = (int) ((x+1)/textScale);
                int scaledY = (int) ((y+1)/textScale);
                mc.fontRenderer.drawString(this.displayString, scaledX, scaledY, 0x000000);
                undoUniformScale(textScale);
            }
        }
    }

    // -- Abstract Methods -- //
    /**
     * Get the scaling that should be applied to body text (highlighted buttons included)
     * @return A positive floating number, by which the buttons are scaled
     */
    protected abstract float getTextScale();

    // -- Drawing functions -- //
    /**
     * Draw the background pages (the book that appears behind the text)
     */
    protected void drawPagesBackground() {
        int x = (this.width - 256) / 2;
        int y = (this.height - 192) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, 256, 179);
    }

    // -- Utility functions -- //
    /**
     * Helper function to reduce directly calling GlStateManager
     * Sets the scale of elements uniformly (rather than along x, y, and z)
     * @param scale The scaling to apply
     */
    protected static void setUniformScale(float scale) {
        GlStateManager.scale(scale, scale, scale);
    }

    /**
     * Another helper function to reduce directly calling GlStateManager
     * Undoes the effects of the "setUniformScale" operation
     * @param scale The scaling to apply
     */
    protected static void undoUniformScale(float scale) {
        float undoneScale = 1/scale;
        GlStateManager.scale(undoneScale, undoneScale, undoneScale);
    }
}
