/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui;

import biocircuitry.client.gui.brood_bin.BroodBinContainer;
import biocircuitry.client.gui.brood_bin.BroodBinGUI;
import biocircuitry.client.gui.feed_bin.FeedBinContainer;
import biocircuitry.client.gui.feed_bin.FeedBinGUI;
import biocircuitry.client.gui.handbook.HandbookDirectoryGUI;
import biocircuitry.client.gui.handbook.HandbookSectionGUI;
import biocircuitry.core.tileentities.TileBroodBin;
import biocircuitry.core.tileentities.TileFeedBin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

/**
 * Manages GUI interpretation for this mod
 * @author SomeoneInParticular
 */
public class ModGUIHandler implements IGuiHandler {
    // The IDs for Tile Entity GUIs within our mod
    public static final int BROOD_BIN_ID = 0;
    public static final int FEED_BIN_ID = 1;

    // The IDs for Item GUIs within our mod
    public static final int HANDBOOK_NAVI_ID = 2;
    public static final int HANDBOOK_ID = 3;

    /**
     * Fetch the GUI's container from the server, for the requesting player
     * @param ID The GUI ID to fetch (see above)
     * @param player The player making the request
     * @param world The world the player is making the request in
     * @param x The X position of the player
     * @param y The Y position of the player
     * @param z The Z position of the player
     * @return The newly generated container instance for the brood bin
     */
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == BROOD_BIN_ID) {
            TileBroodBin broodBin = (TileBroodBin) world.getTileEntity(new BlockPos(x,y,z));
            assert (broodBin != null);
            return new BroodBinContainer(player.inventory, broodBin);
        } else if (ID == FEED_BIN_ID) {
            TileFeedBin feedBin = (TileFeedBin) world.getTileEntity(new BlockPos(x,y,z));
            assert (feedBin != null);
            return new FeedBinContainer(player.inventory, feedBin);
        }
        return null;
    }

    /**
     * Fetch the GUI's rendered element, for the requesting player
     * @param ID The GUI ID to fetch (see above)
     * @param player The player making the request
     * @param world The world the player is making the request in
     * @param x The X position of the player
     * @param y The Y position of the player
     * @param z The Z position of the player
     * @return The newly generated GUI render for the brood bin
     */
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == BROOD_BIN_ID) {
            return new BroodBinGUI((Container) getServerGuiElement(ID, player, world, x, y, z), player.inventory);
        } else if (ID == FEED_BIN_ID) {
            return new FeedBinGUI((Container) getServerGuiElement(ID, player, world, x, y, z), player.inventory);
        } else if (ID == HANDBOOK_NAVI_ID) {
            return new HandbookDirectoryGUI();
        } else if (ID == HANDBOOK_ID) {
            // Backup, should the player somehow open this GUI directly
            return new HandbookSectionGUI("error");
        }
        return null;
    }
}
