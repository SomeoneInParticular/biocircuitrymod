package biocircuitry.client.gui.feed_bin;

import biocircuitry.BioCircuitry;
import biocircuitry.core.blocks.ModBlocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class FeedBinGUI extends GuiContainer {
    // Resource location for the feed bin GUI
    private static final ResourceLocation BG_TEXTURE =
            new ResourceLocation(BioCircuitry.MOD_ID, "textures/gui/feed_bin.png");
    // FeedBinContainer bound to this GUI
    private final FeedBinContainer feedBin;

    public FeedBinGUI(Container container, InventoryPlayer playerInv) {
        super(container);
        this.feedBin = (FeedBinContainer) container;
    }

    /**
     * Draw the background layer of the GUI (slot divots)
     * @param partialTicks The partial tick on which to render the GUI
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     */
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        // Draw the base of the GUI
        GlStateManager.color(1,1,1,1);
        Minecraft mc = Minecraft.getMinecraft();
        mc.getTextureManager().bindTexture(BG_TEXTURE);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        super.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
        // Draw the progress bar, if any
        if (this.feedBin.getProgress() != 0) {
            x += xSize/2 - 16;
            y += ySize/2 - 26;
            super.drawTexturedModalRect(x, y, xSize, 0, this.feedBin.getProgress(), 13);
        }
    }

    /**
     * Draw the foreground layer of the GUI (text elements)
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     */
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        // Draw the text for the name of the GUI
        String name = I18n.format(ModBlocks.feed_bin.getUnlocalizedName() + ".name");
        fontRenderer.drawString(name, xSize/2 - fontRenderer.getStringWidth(name)/2, 6, 0x404040);
    }

    /**
     * Draw the GUI, in its entirety, with any special additions (tool tips etc.)
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     * @param partialTicks The partial tick on which to render the GUI
     */
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // Render the dark tint behind the GUI
        this.drawDefaultBackground();
        // Render the GUI itself
        super.drawScreen(mouseX, mouseY, partialTicks);
        // Render the item's tooltip (if any) with appropriate offsets
        this.renderHoveredToolTip(mouseX, mouseY);
    }
}
