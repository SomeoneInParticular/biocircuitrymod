/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.feed_bin.slots;

import biocircuitry.core.tileentities.TileFeedBin;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;

public class OutputSlot extends SlotItemHandler {
    // Entity this slot is bound too
    private TileFeedBin ent;

    /**
     * Build the GUI slot with generic values
     *
     * @param itemHandler Itemhandler to use for item checks in the slot
     * @param index       Index the slot should be designated by within the GUI
     * @param xPosition   Horizontal position to draw the slot at
     * @param yPosition   Vertical position to draw the slot at
     * @param ent         TileEntity to 'bind' to on creation
     */
    public OutputSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition, TileFeedBin ent) {
        super(itemHandler, index, xPosition, yPosition);
        this.ent = ent;
    }

    @Override
    public void onSlotChanged() {
        super.onSlotChanged();
        ent.markDirty();
        ent.checkOutputs();
    }

    /**
     * Validate that only worms can be placed in the slot, rejecting other items
     * @param stack Item stack to check for validity
     * @return Is it a worm, or not?
     */
    @Override
    public boolean isItemValid(@Nonnull ItemStack stack) {
        return false;
    }
}
