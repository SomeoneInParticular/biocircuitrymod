/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.feed_bin.slots;

import biocircuitry.core.tileentities.TileFeedBin;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

/**
 * Simply wraps up some valuable overrides for ease of use; can take any item
 * @author SomeoneInParticular
 */
public class InputSlot extends SlotItemHandler {
    // Entity this slot is bound too
    private TileFeedBin ent;

    /**
     * Build the GUI slot with generic values
     * @param itemHandler Itemhandler to use for item checks in the slot
     * @param index Index the slot should be designated by within the GUI
     * @param xPosition Horizontal position to draw the slot at
     * @param yPosition Vertical position to draw the slot at
     * @param ent TileEntity to 'bind' to on creation
     */
    public InputSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition, TileFeedBin ent) {
        super(itemHandler, index, xPosition, yPosition);
        this.ent = ent;
    }

    /**
     * Track when the inventory slot is modified, marking the 'bound' entity to be saved when it does so
     */
    @Override
    public void onSlotChanged() {
        super.onSlotChanged();
        ent.checkInputs();
    }
}

