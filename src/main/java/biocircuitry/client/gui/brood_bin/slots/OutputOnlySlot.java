/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.brood_bin.slots;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nonnull;

public class OutputOnlySlot extends GenericSlot {
    /**
     * Build the GUI slot with generic values
     *
     * @param itemHandler Itemhandler to use for item checks in the slot
     * @param index       Index the slot should be designated by within the GUI
     * @param xPosition   Horizontal position to draw the slot at
     * @param yPosition   Vertical position to draw the slot at
     * @param ent         TileEntity to 'bind' to on creation
     */
    public OutputOnlySlot(IItemHandler itemHandler, int index, int xPosition, int yPosition, TileEntity ent) {
        super(itemHandler, index, xPosition, yPosition, ent);
    }

    /**
     * Validate that only worms can be placed in the slot, rejecting other items
     * @param stack Item stack to check for validity
     * @return Is it a worm, or not?
     */
    @Override
    public boolean isItemValid(@Nonnull ItemStack stack) {
        return false;
    }
}
