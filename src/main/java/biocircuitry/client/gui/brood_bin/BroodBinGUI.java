/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.client.gui.brood_bin;

import biocircuitry.BioCircuitry;
import biocircuitry.core.blocks.ModBlocks;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

/**
 * Brood bin GUI; allows the user to interact with the brood bin to allow worm breeding
 * @author SomeoneInParticular
 */
public class BroodBinGUI extends GuiContainer {
    // Resource location for the brood bin GUI
    private static final ResourceLocation BG_TEXTURE =
            new ResourceLocation(BioCircuitry.MOD_ID, "textures/gui/brood_bin.png");
    
    public BroodBinGUI(Container container, InventoryPlayer playerInv) {
        super(container);
    }

    /**
     * Draw the background layer of the GUI (slot divots)
     * @param partialTicks The partial tick on which to render the GUI
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     */
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1, 1, 1, 1);
        Minecraft mc = Minecraft.getMinecraft();
        mc.getTextureManager().bindTexture(BG_TEXTURE);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        super.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
    }

    /**
     * Draw the foreground layer of the GUI (text elements)
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     */
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String name = I18n.format(ModBlocks.brood_bin.getUnlocalizedName() + ".name");
        fontRenderer.drawString(name, xSize/2 - fontRenderer.getStringWidth(name)/2, 6, 0x404040);
    }

    /**
     * Draw the GUI, in its entirety, with any special additions (tool tips etc.)
     * @param mouseX Horizontal position of the mouse on said tick
     * @param mouseY Vertical position of the mouse on said tick
     * @param partialTicks The partial tick on which to render the GUI
     */
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // Render the dark tint behind the GUI
        this.drawDefaultBackground();
        // Render the GUI itself
        super.drawScreen(mouseX, mouseY, partialTicks);
        // Render the item's tooltip (if any) with appropriate offsets
        this.renderHoveredToolTip(mouseX, mouseY);
    }
}
