/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry;

import biocircuitry.client.BioCircuitryTab;
import biocircuitry.core.blocks.ModBlocks;
import biocircuitry.core.items.ModItems;
import biocircuitry.core.proxy.ProxyCommon;
import biocircuitry.client.gui.ModGUIHandler;
import biocircuitry.ecology.ModWormFactories;
import biocircuitry.networking.NetworkManager;
import biocircuitry.proteomics.ModFoodValues;
import biocircuitry.proteomics.ModMetabolites;
import biocircuitry.proteomics.ModProteins;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

/**
 * BioCircuitry Minecraft Mod
 *
 * Breed worms to refine resources, with accurate genetic systems
 *
 * @author SomeoneInParticular
 */
@Mod(
    modid = BioCircuitry.MOD_ID,
    name = BioCircuitry.MOD_NAME,
    version = BioCircuitry.VERSION,
//    guiFactory = "TODO",
    acceptedMinecraftVersions = "[1.12.2]",
    useMetadata=true
    )
public class BioCircuitry {
    // Constants
    public static final String MOD_NAME = "BioCircuitry";
    public static final String MOD_ID = "biocircuitry";
    public static final String VERSION = "0.0.1";

    public static final Random RANDOM = new Random();

    // Mod attributes
    @Mod.Instance
    public static BioCircuitry instance;
    
    // Logical proxy
    @SidedProxy(serverSide = "biocircuitry.core.proxy.ProxyCommon", clientSide = "biocircuitry.core.proxy.ProxyClient")
    public static ProxyCommon proxy;
    
    // Logger for debugging/warnings
    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);
    
    // Creative tab for blocks
    public static final BioCircuitryTab CREATIVE_TAB = new BioCircuitryTab();

    /**
     * Initialize processes which other mods may rely on, and debugging methods
     * @param event Pre-Initialization event
     */
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        // Log that we've begun pre-init
        LOGGER.info(BioCircuitry.MOD_NAME + " entered pre-init");

        // Initialize Forge extended entities
        proxy.registerCapabilities();
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new ModGUIHandler());

        // Initialize our mod's entities
        ModProteins.init();
        ModMetabolites.init();
        ModFoodValues.init();
        ModWormFactories.init();

        // Initialize the network manager for this mod
        NetworkManager.init();
    }

    /**
     * Initialize mod processes
     * @param event Initialization event
     */
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        // Currently no initialization events
    }

    /**
     * Initialize processes which we rely on other mods to do, if any
     * @param event Post-Initialization event
     */
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        // Currently no post-initialization events
    }

    // Manages object registration when requested by Forge
    @Mod.EventBusSubscriber
    public static class RegistrationHandler {
        /**
         * Register item instances when requested
         * @param event The Item registration event
         */
        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            ModItems.register(event.getRegistry());
            ModBlocks.registerItemBlocks(event.getRegistry());
        }

        /**
         * Register our models when requested
         * @param event The Item model registration event
         */
        @SubscribeEvent
        public static void registerModels(ModelRegistryEvent event) {
            ModItems.registerModels();
            ModBlocks.registerModels();
        }

        /**
         * Register block instances when requested
         * @param event The Block registration event
         */
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event) {
            ModBlocks.register(event.getRegistry());
        }
    }
}
