/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.capabilities;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides instances of GeneticProducer capabilities for anything that requests it
 */
public class GeneticProducerProvider implements ICapabilitySerializable<NBTTagCompound> {
    @CapabilityInject(GeneticProducer.class)
    public static final Capability<GeneticProducer> GENETIC_PRODUCER_CAPABILITY = null;

    private GeneticProducer instance;

    // Default constructor
    public GeneticProducerProvider() {
        this.instance = new GeneticProducer();
    }

    @Override
    public boolean hasCapability(@Nonnull Capability<?> cpblt, @Nullable EnumFacing ef) {
        return cpblt == GENETIC_PRODUCER_CAPABILITY;
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> cpblt, @Nullable EnumFacing ef) {
        if (this.hasCapability(cpblt, ef)) {
            return GENETIC_PRODUCER_CAPABILITY.cast(this.instance);
        }
        return null;
    }

    @Override
    public NBTTagCompound serializeNBT() {
        return this.instance.serializeNBT();
    }

    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        this.instance = new GeneticProducer();
        this.instance.deserializeNBT(nbt);
    }
}
