/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.capabilities;

import biocircuitry.api.IGeneticProducer;
import biocircuitry.api.genomics.IGenome;
import biocircuitry.genomics.WormGenome;
import biocircuitry.genomics.structures.WormChromosome;
import biocircuitry.proteomics.structures.WormProtein;
import biocircuitry.proteomics.structures.WormProteome;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.INBTSerializable;

import java.util.ArrayList;
import java.util.Collection;

public class GeneticProducer implements IGeneticProducer<WormChromosome, WormProtein>, INBTSerializable<NBTTagCompound> {
    // Class attributes
    private WormGenome wormGenome;
    private WormProteome proteome;

    // Default constructor
    public GeneticProducer() {
        this.wormGenome = new WormGenome();
        this.proteome = new WormProteome();
    }

    @Override
    public ArrayList<WormChromosome> createGameteChromList() {
        return this.wormGenome.buildGamete();
    }

    public IGenome<WormChromosome> getWormGenome() {
        return this.wormGenome;
    }

    public void setWormGenome(WormGenome wormGenome) {
        this.wormGenome = wormGenome;
        this.proteome = new WormProteome();
        this.proteome.buildFromGenome(wormGenome);
    }

    @Override
    public Collection<WormProtein> getProteins() {
        return this.proteome.getProteins();
    }

    public boolean isAlive() {
        return this.proteome.isAlive();
    }

    @Override
    public void buildFromParents(Collection<WormChromosome> maternal, Collection<WormChromosome> paternal) {
        // Rebuild the genome
        this.wormGenome = new WormGenome();
        this.wormGenome.reformGenome(maternal, paternal);
        // Rebuild the proteome
        this.proteome = new WormProteome();
        this.proteome.buildFromGenome(this.wormGenome);
    }

    @Override
    public String getInfo() {
        String infoString = this.proteome.getProtString();
        return infoString == null ? "" : infoString;
    }

    @Override
    public NBTTagCompound serializeNBT() {
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setTag("genome", this.wormGenome.serializeNBT());
        nbt.setTag("proteome", this.proteome.writeNBT());
        return nbt;
    }

    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        this.wormGenome.deserializeNBT(nbt.getCompoundTag("genome"));
        this.proteome.readNBT(nbt.getCompoundTag("proteome"));
    }
}
