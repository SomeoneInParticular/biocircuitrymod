/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.genomics;

import biocircuitry.BioCircuitry;
import biocircuitry.api.genomics.IChromosome;
import biocircuitry.api.genomics.IGenome;
import biocircuitry.genomics.structures.WormChromosome;
import biocircuitry.genomics.structures.NeedlemanWunschAlignment;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByteArray;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.INBTSerializable;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Default genome implementation
 * Generates product tables and recombinant genomes for the next generation
 * @author SomeoneInParticular
 */
public class WormGenome implements IGenome<WormChromosome>, INBTSerializable<NBTTagCompound> {
    // -- Class Attributes -- //
    // List of chromosomes contained within the genome
    private ArrayList<WormChromosome> chromList;

    // Attributes saved from alignment, to avoid expensive recalculations later
    private ArrayList<NeedlemanWunschAlignment> alignmentList;
    private ArrayList<WormChromosome> orphanList;

    // Minimal alignment score required to allow recombination (non-disjunction occurs otherwise)
    private final int MIN_SCORE = 0;

    // -- Constructors -- //
    // Generic diploid genome constructor
    public WormGenome() {
        this.chromList = new ArrayList<>();
        this.alignmentList = new ArrayList<>();
        this.orphanList = new ArrayList<>();
    }

    /**
     * Build a default, ready-to-use genome implementation
     */
    public static WormGenome genericGenome() {
        WormGenome wormGenome = new WormGenome();
        WormChromosome chrom1 = new WormChromosome("CACACACAC");
        WormChromosome chrom2 = new WormChromosome("TATATATAT");
        wormGenome.addSequence(chrom1);
        wormGenome.addSequence(chrom2);
        wormGenome.buildAlignments();
        return wormGenome;
    }

    /**
     * Build a genome solely from its chromosome list
     * @param chromList List of chromosomes to use
     */
    public WormGenome(ArrayList<WormChromosome> chromList) {
        this.chromList = chromList;
        this.buildAlignments();
    }

    /**
     * Build a genome with explicit contents; primarily useful for avoiding the
     * expensive "buildAlignments" function running a million times per second
     * because Minecraft decided that, instead of caching creative tab entities,
     * it should rebuild them all every time the tab is opened instead.
     * @param chromList Base chromosome list (as a backup)
     * @param alignmentList Chromosome alignment list
     * @param orphanList Orphaned chromosome list
     */
    public WormGenome(ArrayList<WormChromosome> chromList, ArrayList<NeedlemanWunschAlignment> alignmentList, ArrayList<WormChromosome> orphanList) {
        this.chromList = chromList;
        this.alignmentList = alignmentList;
        this.orphanList = orphanList;
    }

    /**
     * Build a genome from scratch given two component chromosome lists
     * @param chrom_list_1 Chromosome list one ("paternal")
     * @param chrom_list_2 Chromosome list two ("maternal")
     */
    public WormGenome(ArrayList<WormChromosome> chrom_list_1, ArrayList<WormChromosome> chrom_list_2) {
        this.chromList = chrom_list_1;
        this.chromList.addAll(chrom_list_2);
        this.buildAlignments();
    }

    // -- Functions -- //
    // Rebuild the genome from scratch, given two component chromosome lists
    public void reformGenome(Collection<WormChromosome> chrom_list_1, Collection<WormChromosome> chrom_list_2) {
        this.chromList = new ArrayList<>(chrom_list_1);
        this.chromList.addAll(chrom_list_2);
        this.buildAlignments();
    }

    /**
     * Build the optimal alignment pairings for this genome based on
     * the chromosomes contained within
     */
    private void buildAlignments() {
        // List of chromosomes left to be tested for optimal alignments
        ArrayList<WormChromosome> todoList = new ArrayList<>(this.chromList);
        // Reset the alignment and orphan list to avoid infinite growth
        this.alignmentList = new ArrayList<>();
        this.orphanList = new ArrayList<>();
        // Find the best, valid alignment for each chromosome
        while (todoList.size() > 1) {
            // Currently targeted chromosome
            WormChromosome targetChrom = todoList.get(0);
            // Current best score, its associated chromosome, and its alignment (minimum score set above)
            NeedlemanWunschAlignment alignment = null;
            int currentScore = MIN_SCORE;
            int currentIndex = 0;
            WormChromosome current_chrom;
            // For each chromosome not tested
            for (int i = 1; i < todoList.size(); i++) {
                // Build the alignment for the chromosome
                current_chrom = todoList.get(i);
                NeedlemanWunschAlignment tempAlignment =
                        new NeedlemanWunschAlignment(targetChrom, current_chrom);
                // If the new alignment is better than the previous, update the values
                if (tempAlignment.getScore() > currentScore) {
                    alignment = tempAlignment;
                    currentScore = alignment.getScore();
                    currentIndex = i;
                }
            }
            // If this chromosome formed a valid alignment, save it for later use
            if (alignment != null) {
                this.alignmentList.add(alignment);
                todoList.remove(currentIndex);
            }
            // Otherwise, add the chromosome to the orphan list
            else {
                this.orphanList.add(targetChrom);
            }
            // Remove the chromosome from the queue
            todoList.remove(0);
        }
        // Add the last chromosome left in the list, if applicable
        if (!todoList.isEmpty()) {
            this.orphanList.add(todoList.get(0));
        }
    }

    // Get the full set of chromosomes for this genome
    @Override
    public ArrayList<WormChromosome> getSomaticChromosomes() {
        return this.chromList;
    }

    // Generate the germ-line genome to contribute to progeny
    @Override
    public ArrayList<WormChromosome> buildGamete() {
        // Initialization
        ArrayList<WormChromosome> retList = new ArrayList<>();
        // For each alignment, generate a recombinant
        for (NeedlemanWunschAlignment a : this.alignmentList) {
            retList.add(a.makeRecombinant());
        }
        // For each orphan, determine if non-disjunction occurred
        for (WormChromosome c : this.orphanList) {
            if (BioCircuitry.RANDOM.nextBoolean()) {
                retList.add(c);
            }
        }
        return retList;
    }

    /**
     * Replace the current genome with an existing genome's contents
     * NOTE: The lists themselves are distinct, but the contents
     * of said lists are *shallow* copies; modifying the chromosomes/
     * alignments after this operation can lead to whacky results
     * @param wormGenome Genome to copy data from
     */
    public void copyGenome(WormGenome wormGenome) {
        this.chromList = new ArrayList<>(wormGenome.chromList);
        this.alignmentList = new ArrayList<>(wormGenome.alignmentList);
        this.orphanList = new ArrayList<>(wormGenome.orphanList);
    }
    
    @Override
    public void addSequence(WormChromosome chrom) {
        this.chromList.add(chrom);
        this.buildAlignments();
    }

    @Override
    public void setSequence(int i, WormChromosome chrom) {
        this.chromList.set(i, chrom);
        this.buildAlignments();
    }

    /**
     * Serialize the genome as it is currently down into NBT format
     * @return The NBT tag which represents the genome
     */
    @Override
    public NBTTagCompound serializeNBT() {
        // Chromosome list
        NBTTagCompound nbt = new NBTTagCompound();
        // Alignment list
        NBTTagList alignList = new NBTTagList();
        for (NeedlemanWunschAlignment nwa : this.alignmentList) {
            alignList.appendTag(nwa.serializeNBT());
        }
        nbt.setTag("alignments", alignList);
        // Orphan list
        NBTTagList orphanList = new NBTTagList();
        for (WormChromosome c : this.orphanList) {
            orphanList.appendTag(c.serializeNBT());
        }
        nbt.setTag("orphans", orphanList);
        return nbt;
    }

    /**
     * Reconstruct this genome based on the serialized NBT tag information
     * @param nbt NBT tag to base the reconstruction on
     */
    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        // Setup
        this.chromList = new ArrayList<>();
        // Alignment reconstruction
        NBTTagList alignList = (NBTTagList) nbt.getTag("alignments");
        for (NBTBase alignNBT : alignList) {
            NeedlemanWunschAlignment nwa = new NeedlemanWunschAlignment((NBTTagCompound) alignNBT);
            this.chromList.addAll(nwa.getTrackedChromosomes());
            this.alignmentList.add(nwa);
        }
        // Orphaned chromosome reconstruction
        NBTTagList orphanList = (NBTTagList) nbt.getTag("orphans");
        for (NBTBase orphanNBT : orphanList) {
            WormChromosome c = new WormChromosome();
            c.deserializeNBT((NBTTagByteArray) orphanNBT);
            this.chromList.add(c);
            this.orphanList.add(c);
        }
    }

    public String toString() {
        StringBuilder str = new StringBuilder("(");
        for (IChromosome c : chromList) {
            str.append(" ").append(c.toString()).append("\n");
        }
        str.append(")");
        return str.toString();
    }
}
