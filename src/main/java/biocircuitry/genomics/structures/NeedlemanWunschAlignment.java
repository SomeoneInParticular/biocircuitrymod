/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.genomics.structures;

import biocircuitry.BioCircuitry;
import biocircuitry.api.genomics.IAlignment;
import biocircuitry.utils.GammaDistribution;
import javafx.util.Pair;
import net.minecraft.nbt.NBTTagByteArray;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.INBTSerializable;

import javax.management.InvalidAttributeValueException;
import java.util.*;


/**
 * Alignment generated via the Needleman-Wunsch algorithm
 * Very slow with large sequences, but extremely accurate
 */
public class NeedlemanWunschAlignment implements IAlignment<WormChromosome>, INBTSerializable<NBTTagCompound> {
    // -- Attributes -- //
    // Tracked Chromosomes
    private WormChromosome chromA;
    private WormChromosome chromB;

    // Similarity matrix (default)
    private final static int MATCH = 2;
    private final static int MISMATCH = -2;
    private final static int INDEL = -3;

    // Alignment score for current alignment schema
    private int score = 0;

    // Preserved alignment schema values
    private ArrayList<Pair<Integer, Integer>> alignmentSchema;

    // -- Constructors -- //
    // Create a NeedleWunschAlignment from a provided NBT Tag
    public NeedlemanWunschAlignment(NBTTagCompound nbt) {
        this.deserializeNBT(nbt);
    }

    // Generic constructor which calls the align function automatically
    public NeedlemanWunschAlignment(WormChromosome chromA, WormChromosome chromB) {
        this.chromA = chromA;
        this.chromB = chromB;
        this.align();
    }

    // -- Functions -- //
    /**
     * Build a score matrix for the two chromosomes this alignment corresponds too
     * @return The score matrix build (a 2d integer array)
     */
    private int[][] buildScoreMatrix() {
        // Initialization
        ArrayList<Base> seqA = chromA.getFullSequence();
        ArrayList<Base> seqB = chromB.getFullSequence();
        int a_size = seqA.size();
        int b_size = seqB.size();
        int[][] val_matrix = new int[a_size+1][b_size+1];
        // Initial matrix edge value setup
        for (int a = 0; a <= a_size; a++) {
            val_matrix[a][0] = a*INDEL;
        }
        for (int b = 0; b <= b_size; b++) {
            val_matrix[0][b] = (b*INDEL);
        }
        // Matrix scoring
        for (int a = 1; a <= a_size; a++) {
            for (int b = 1; b <= b_size; b++) {
                int a_shift = val_matrix[a-1][b] + INDEL;
                int b_shift = val_matrix[a][b-1] + INDEL;
                int ab_shift;
                Base baseA = seqA.get(a-1);
                Base baseB = seqB.get(b-1);
                if (baseA.equals(baseB)) {
                    ab_shift = val_matrix[a-1][b-1] + MATCH;
                } else {
                    ab_shift = val_matrix[a-1][b-1] + MISMATCH;
                }
                val_matrix[a][b] = Math.max(ab_shift, Math.max(a_shift, b_shift));
            }
        }
        // Return the matrix for further processing
        return val_matrix;
    }

    /**
     * Take a score matrix and generate our alignment (match-tracking) schema
     * @param score_matrix The score matrix guiding the schema construction
     */
    private void buildSchema(int[][] score_matrix) {
        // Setup
        int a = score_matrix.length - 1;
        int b = score_matrix[0].length - 1;
        this.alignmentSchema = new ArrayList<>();
        // Schema construction
        while (a > 0 && b > 0) {
            // If the score indicates a shift along sequence A
            if (score_matrix[a][b] == score_matrix[a-1][b] + INDEL) {
                a--;
            // If the score indicates a shift along sequence B
            } else if (score_matrix[a][b] == score_matrix[a][b-1] + INDEL) {
                b--;
            } // If the score indicates a match or mismatch
            else {
                if (score_matrix[a][b] == score_matrix[a-1][b-1] + MATCH) {
                    alignmentSchema.add(new Pair<>(a, b));
                }
                a--;
                b--;
            }
        }
        // Reverse the schema, as it is built from the tail up
        Collections.reverse(this.alignmentSchema);
    }

    /**
     * Generate an alignment, given two chromosomes, updating the schema and score
     * Simply a shortcut for a common chain of functions defined above
     */
    private void align() {
        // Build the alignment schema
        int[][] score_matrix = buildScoreMatrix();
        // Save the alignment score for later use
        this.score = score_matrix[score_matrix.length-1][score_matrix[0].length-1];
        // Build the alignment schema for use within the program
        buildSchema(score_matrix);
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public Collection<WormChromosome> getTrackedChromosomes() {
        ArrayList<WormChromosome> retList = new ArrayList<>(2);
        retList.add(this.chromA);
        retList.add(this.chromB);
        return retList;
    }

    /**
     * Generates a random recombinant chromosome.
     * Uses a simplified version of Voorrips and Maliepaard's meiosis simulation (2012),
     * always using double bivalent crossover mechanics (our worms aren't tetradic).
     * @return The resulting recombinant chromosome
     */
    @Override
    public WormChromosome makeRecombinant() {
        // Generate distribution
        GammaDistribution distribution;
        try {
            // Set up the gamma, with the mean crossover position being centered along the alignment
            double a = 2.63;
            double s = alignmentSchema.size() / (2.63);
            distribution = new GammaDistribution(a, s);
        } catch (InvalidAttributeValueException e) {
            // Shouldn't happen given the values above, but here for safety.
            e.printStackTrace();
            return null;
        }
        // Initialization
        Random rand = BioCircuitry.RANDOM;
        boolean save_first = rand.nextBoolean();
        ArrayDeque<Integer> cross_points = new ArrayDeque<>();
        double next_delta;
        int next_pos;
        // Initial 'virtual' cross-over, to avoid "late sequence" preference
        int current_pos = -alignmentSchema.size()*3 + (int) (Math.log(1-rand.nextDouble()) * -(alignmentSchema.size()));
        // Further 'virtual' generation until we reach a valid cross-over position
        while (current_pos < 0) {
            current_pos += (int) distribution.sample();
        }
        // Generate crossover points
        while (current_pos < alignmentSchema.size()) {
            // Add the cross-over point to our list
            cross_points.add(current_pos);
            // Calculate the next potential cross-over point
            next_delta = (int) distribution.sample();
            // Special case; if the sequence would roll the same position, return an unaltered chromosome
            if (next_delta < 1) {
                return BioCircuitry.RANDOM.nextBoolean() ? this.chromA : this.chromB;
            }
            // Otherwise, update the cross-over point
            else {
                current_pos += next_delta;
            }
        }
        return this.generateRecombinant(cross_points, save_first);
    }

    /**
     * Generate a new recombinant chromosome from the two chromosomes tracked by the alignment
     * Allows for debugging/testing with explicit crossover positions and preserv
     * @param cross_points Matching position indices to cross-over at
     * @param save_first Whether the chromosome starting at chromA or chromB should be saved
     * @return The resulting recombinant from the crossovers (if any)
     */
    private WormChromosome generateRecombinant(Queue<Integer> cross_points, boolean save_first) {
        // Initialization
        ArrayList<Base> seqA = this.chromA.getFullSequence();
        ArrayList<Base> seqB = this.chromB.getFullSequence();
        int current_pos = 0;
        ArrayList<Base> new_seq = new ArrayList<>();
        // Loop variables
        Pair<Integer, Integer> current_match_indices;
        // Sequence construction
        while (cross_points.peek() != null) {
            // Fetch the most recent crossover region
            current_match_indices = this.alignmentSchema.get(cross_points.remove());
            // If we're saving the first Chromosome's sequence (ChromA)...
            if (save_first) {
                // Add ChromA's subsequence between the last crossover and this crossover point
                new_seq.addAll(new ArrayList<>(seqA.subList(current_pos, current_match_indices.getKey())));
                // Prepare for the next crossover, setting the initial position on ChromB
                current_pos = current_match_indices.getValue();
                // Set ChromB to be saved next
                save_first = false;
            }
            // Otherwise, we're saving the second Chromosome's sequence (ChromB)
            else {
                // Add ChromB's subsequence between the last crossover and this crossover point
                new_seq.addAll(new ArrayList<>(seqB.subList(current_pos, current_match_indices.getValue())));
                // Prepare for the next crossover, setting the initial position on ChromA
                current_pos = current_match_indices.getKey();
                // Set ChromA to be saved next
                save_first = true;
            }
        }
        // Fill in the final sequence of the chromosome, after the last crossover
        if (save_first) {
            new_seq.addAll(new ArrayList<>(seqA.subList(current_pos, seqA.size())));
        } else {
            new_seq.addAll(new ArrayList<>(seqB.subList(current_pos, seqB.size())));
        }
        // Return the new sequence
        return new WormChromosome(new_seq);
    }

    @Override
    public NBTTagCompound serializeNBT() {
        // Non-schema saved data
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setTag("chrom_a", this.chromA.serializeNBT());
        nbt.setTag("chrom_b", this.chromB.serializeNBT());
        nbt.setInteger("score", this.score);
        // Schema saved data
        int[] leftVals = new int[this.alignmentSchema.size()];
        int[] rightVals = new int[this.alignmentSchema.size()];
        for (int i = 0; i < this.alignmentSchema.size(); i++) {
            Pair<Integer, Integer> p = this.alignmentSchema.get(i);
            leftVals[i] = p.getKey();
            rightVals[i] = p.getValue();
        }
        nbt.setIntArray("chrom_a_schema", leftVals);
        nbt.setIntArray("chrom_b_schema", rightVals);
        return nbt;
    }

    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        // Non-schema reconstruction
        this.chromA = new WormChromosome();
        this.chromA.deserializeNBT((NBTTagByteArray) nbt.getTag("chrom_a"));
        this.chromB = new WormChromosome();
        this.chromB.deserializeNBT((NBTTagByteArray) nbt.getTag("chrom_b"));
        this.score = nbt.getInteger("score");
        // Schema reconstruction
        int[] leftVals = nbt.getIntArray("chrom_a_schema");
        int[] rightVals = nbt.getIntArray("chrom_b_schema");
        this.alignmentSchema = new ArrayList<>(leftVals.length);
        for (int i = 0; i < leftVals.length; i++) {
            Pair<Integer, Integer> p = new Pair<>(leftVals[i], rightVals[i]);
            this.alignmentSchema.add(p);
        }
    }
}
