/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.genomics.structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public enum Base {
    A, T, C, G;

    public static ArrayList<Base> randomSequence(int length, Random random) {
        ArrayList<Base> sequence = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            sequence.add(Base.values()[random.nextInt(4)]);
        }
        return sequence;
    }

    public static String getSequenceString(ArrayList<Base> seq) {
        StringBuilder seq_string = new StringBuilder();
        for (Base b : seq) {
            seq_string.append(b.name());
        }
        return seq_string.toString();
    }

    public static ArrayList<Base> getSequenceFromString(String string) {
        String str = string.toUpperCase();
        ArrayList<Base> list = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            String c = str.substring(i, i+1);
            list.add(Base.valueOf(c));
        }
        return list;
    }
}
