/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.genomics.structures;

import biocircuitry.api.genomics.IChromosome;

import java.util.ArrayList;

import net.minecraft.nbt.NBTTagByteArray;
import net.minecraftforge.common.util.INBTSerializable;

/**
 * Manages genes, crossing over, and what gets passed to the next generation
 * @author SomeoneInParticular
 */
public class WormChromosome implements IChromosome<Base>, INBTSerializable<NBTTagByteArray> {
    // The list of genes on the chromosome, in their specific order
    private ArrayList<Base> base_sequence;
    
    // Default, blank Chromosome
    public WormChromosome() {
        this.base_sequence = new ArrayList<>();
    }

    // Build a chromosome with a specified gene sequence
    public WormChromosome(ArrayList<Base> base_sequence) {
        this.base_sequence = base_sequence;
    }

    // Build a chromosome based on a string sequence
    // For testing purposes only
    public WormChromosome(String str) {
        this.base_sequence = Base.getSequenceFromString(str);
    }

    /**
     * Build an NBT tag representing the Chromosome
     * @return The NBT tag  representing the Chromosome
     */
    @Override
    public NBTTagByteArray serializeNBT() {
        byte[] byte_arr = new byte[this.base_sequence.size()];
        for (int i = 0; i < this.base_sequence.size(); i++) {
            byte_arr[i] = (byte) this.base_sequence.get(i).ordinal();
        }
        return new NBTTagByteArray(byte_arr);
    }

    /**
     * Rebuild a Chromosome from an NBT tag
     * @param nbt NTB tag to read from
     */
    @Override
    public void deserializeNBT(NBTTagByteArray nbt) {
        byte[] byte_arr = nbt.getByteArray();
        for (byte b : byte_arr) {
            Base base = Base.values()[b];
            this.base_sequence.add(base);
        }
    }

    @Override
    public WormChromosome duplicate() {
        WormChromosome clone = new WormChromosome();
        clone.base_sequence = new ArrayList<>(this.base_sequence);
        return clone;
    }

    @Override
    public ArrayList<Base> getFullSequence() {
        return this.base_sequence;
    }

    @Override
    public ArrayList<Base> getSubSequence(int start_pos, int end_pos) {
        return new ArrayList<>(this.base_sequence.subList(start_pos, end_pos));
    }

    /**
     * Add a gene at a specific position in the chromosome, replacing the current gene if present
     * @param gene_sequence Genes to add
     * @param pos Position to add it
     * @return Whether the addition was successful
     */
    @Override
    public boolean insertSubSequence(ArrayList<Base> gene_sequence, int pos) {
        if (0 <= pos && pos < this.base_sequence.size()) {
            this.base_sequence.addAll(pos, gene_sequence);
            return true;
        }
        return false;
    }

    /**
     * Add a list of genes at the end of the chromosome
     * @param gene_sequence Genes to add
     * @return Whether the addition was successful
     */
    @Override
    public boolean addSequence(ArrayList<Base> gene_sequence) {
        this.base_sequence.addAll(gene_sequence);
        return true;
    }

    /**
     * Excise a sequence of genes from the chromosome
     * @param start_pos Position of the gene to be removed
     * @param end_pos Position of the gene to be removed
     * @return Whether the gene was removed successfully or not
     */
    @Override
    public ArrayList<Base> exciseSequence(int start_pos, int end_pos) {
        return new ArrayList<>(this.base_sequence.subList(start_pos, end_pos));
    }

    public String toString() {
        StringBuilder str = new StringBuilder("{");
        for (Base base : this.base_sequence) {
            str.append(base.toString());
        }
        str.append("}");
        return str.toString();
    }
}
