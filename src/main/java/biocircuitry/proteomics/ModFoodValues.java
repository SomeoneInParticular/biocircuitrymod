package biocircuitry.proteomics;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

import static biocircuitry.api.BioCircuitryAPI.WORM_FOOD_NUTRIENT_REGISTRY;

public class ModFoodValues {
    public static void init() {
        // -- Plant-based -- //
        // Wild
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.LEAVES), ModMetabolites.wood_pulp, 2);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.LEAVES), ModMetabolites.plant_fibre, 2);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.RED_FLOWER), ModMetabolites.plant_fibre, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.YELLOW_FLOWER), ModMetabolites.plant_fibre, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.VINE), ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.CACTUS), ModMetabolites.plant_fibre, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.DEADBUSH), ModMetabolites.wood_pulp, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.GRASS), ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.WATERLILY), ModMetabolites.plant_fibre, 8);
        // Crops
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BEETROOT, ModMetabolites.plant_fibre, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BEETROOT, ModMetabolites.glucose, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.POTATO, ModMetabolites.plant_fibre, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.POTATO, ModMetabolites.glucose, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.MELON, ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.MELON, ModMetabolites.glucose, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.CARROT, ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.CARROT, ModMetabolites.glucose, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.APPLE, ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.APPLE, ModMetabolites.glucose, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.PUMPKIN), ModMetabolites.plant_fibre, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.PUMPKIN), ModMetabolites.glucose, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.MELON_BLOCK), ModMetabolites.plant_fibre, 72);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.MELON_BLOCK), ModMetabolites.glucose, 144);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.BROWN_MUSHROOM), ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.RED_MUSHROOM), ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.BROWN_MUSHROOM_BLOCK), ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.RED_MUSHROOM_BLOCK), ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.SUGAR, ModMetabolites.glucose, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.REEDS, ModMetabolites.cellulose, 4);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.REEDS, ModMetabolites.glucose, 4);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.PAPER, ModMetabolites.cellulose, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.WHEAT, ModMetabolites.plant_fibre, 20);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.WHEAT, ModMetabolites.glucose, 4);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BEETROOT_SEEDS, ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.MELON_SEEDS, ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.PUMPKIN_SEEDS, ModMetabolites.plant_fibre, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.WHEAT_SEEDS, ModMetabolites.plant_fibre, 8);

        // --  Wood-based -- //
        // Raw wood
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.LOG), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.PLANKS), ModMetabolites.wood_pulp, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.STICK, ModMetabolites.wood_pulp, 4);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.SAPLING), ModMetabolites.wood_pulp, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.SAPLING), ModMetabolites.plant_fibre, 8);
        // Boat
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BOAT, ModMetabolites.wood_pulp, 80);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BIRCH_BOAT, ModMetabolites.wood_pulp, 80);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.SPRUCE_BOAT, ModMetabolites.wood_pulp, 80);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.JUNGLE_BOAT, ModMetabolites.wood_pulp, 80);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.DARK_OAK_BOAT, ModMetabolites.wood_pulp, 80);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.ACACIA_BOAT, ModMetabolites.wood_pulp, 80);
        // Door
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.OAK_DOOR, ModMetabolites.wood_pulp, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BIRCH_DOOR, ModMetabolites.wood_pulp, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.SPRUCE_DOOR, ModMetabolites.wood_pulp, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.JUNGLE_DOOR, ModMetabolites.wood_pulp, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.DARK_OAK_BOAT, ModMetabolites.wood_pulp, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.ACACIA_DOOR, ModMetabolites.wood_pulp, 32);
        // Stairs
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.OAK_STAIRS), ModMetabolites.wood_pulp, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.BIRCH_STAIRS), ModMetabolites.wood_pulp, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.SPRUCE_STAIRS), ModMetabolites.wood_pulp, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.JUNGLE_STAIRS), ModMetabolites.wood_pulp, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.DARK_OAK_STAIRS), ModMetabolites.wood_pulp, 24);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.ACACIA_STAIRS), ModMetabolites.wood_pulp, 24);
        // Fences
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.OAK_FENCE), ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.BIRCH_FENCE), ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.SPRUCE_FENCE), ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.JUNGLE_FENCE), ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.DARK_OAK_FENCE), ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.ACACIA_FENCE), ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.OAK_FENCE_GATE), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.BIRCH_FENCE_GATE), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.SPRUCE_FENCE_GATE), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.JUNGLE_FENCE_GATE), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.DARK_OAK_FENCE_GATE), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.ACACIA_FENCE_GATE), ModMetabolites.wood_pulp, 64);
        // Other wooden constructs
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.WOODEN_SLAB), ModMetabolites.wood_pulp, 8);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.LADDER), ModMetabolites.wood_pulp, 18);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.SIGN, ModMetabolites.wood_pulp, 34);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.BOWL, ModMetabolites.wood_pulp, 12);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.CHEST), ModMetabolites.wood_pulp, 128);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.CRAFTING_TABLE), ModMetabolites.wood_pulp, 64);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.WOODEN_BUTTON), ModMetabolites.wood_pulp, 16);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.WOODEN_PRESSURE_PLATE), ModMetabolites.wood_pulp, 32);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Item.getItemFromBlock(Blocks.TRAPDOOR), ModMetabolites.wood_pulp, 48);
    }
}
