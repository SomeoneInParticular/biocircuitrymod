/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics;

import biocircuitry.proteomics.structures.WormMetabolite;

import static biocircuitry.api.BioCircuitryAPI.WORM_METABOLITE_REGISTRY;

public class ModMetabolites {
    // -- COMMON -- //
    public static WormMetabolite glucose = new WormMetabolite("glucose");
    public static WormMetabolite glycogen = new WormMetabolite("glycogen");
    public static WormMetabolite atp = new WormMetabolite("atp");
    public static WormMetabolite plant_fibre = new WormMetabolite("plant_fibre");
    public static WormMetabolite wood_pulp = new WormMetabolite("wood_pulp");
    public static WormMetabolite cellulose = new WormMetabolite("cellulose");
    public static WormMetabolite flesh = new WormMetabolite("raw_flesh");
    public static WormMetabolite meat_slurry = new WormMetabolite("meat_slurry");
    public static WormMetabolite keratin = new WormMetabolite("keratin");

    private static void addAll(WormMetabolite... metabs) {
        for (WormMetabolite metab : metabs) {
            WORM_METABOLITE_REGISTRY.put(metab.getLabel(), metab);
        }
    }

    public static void init() {
        addAll(
                glucose, glycogen, atp, plant_fibre, wood_pulp, cellulose, flesh, meat_slurry,
                keratin
        );
    }
}
