/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures.component;

import biocircuitry.proteomics.structures.WormProtein;
import javafx.util.Pair;

import java.util.*;

/**
 * An optimized search Trie, build solely to allow for quick
 * "almost" match queries and value insertion
 */
public class ProteinTrie {
    // Starting point for tree searches
    private ProteinTrieNode root = new ProteinTrieNode();

    /**
     * Insert a new AminoAcid product sequence into the Trie
     * @param sequence Sequence to insert into the Trie
     */
    public void insert(ArrayList<AminoAcid> sequence, WormProtein value) {
        ProteinTrieNode current = this.root;

        for (AminoAcid a : sequence) {
            current = current.getChildren().computeIfAbsent(a, k -> new ProteinTrieNode());
        }

        current.setProduct(value);
    }

    /**
     * A data class to hold values in an easy to parse and interpret manner
     */
    private static class SearchDataTuple implements Comparable<SearchDataTuple>{
        int queryPos;
        int deviation;
        ProteinTrieNode node;

        SearchDataTuple(int queryPos, int deviation, ProteinTrieNode node) {
            this.queryPos = queryPos;
            this.deviation = deviation;
            this.node = node;
        }

        int getQueryPos() {
            return this.queryPos;
        }

        int getDeviation() {
            return this.deviation;
        }

        ProteinTrieNode getNode() {
            return this.node;
        }

        @Override
        public int compareTo(SearchDataTuple o) {
            if (this.deviation != o.deviation) {
                return this.deviation - o.deviation;
            } else {
                return o.queryPos - this.queryPos;
            }
        }
    }

    /**
     * Query the Trie and find the closest match for the query.
     * Uses a priority queue for optimization of partial matches
     * @param query The AminoAcid query to search for
     * @param threshold The maximum score deviation (BLOSSUM62) allowed before a result is rejected
     * @return The best matching valid product corresponding to the query. null if no valid product found
     */
    public WormProtein findProtein(ArrayList<AminoAcid> query, int threshold) {
        // Initialization
        // Position, deviation score, associated node
        PriorityQueue<SearchDataTuple> queryQueue = new PriorityQueue<>();
        // Start with 0 deviation at position 0 and the root node
        queryQueue.add(new SearchDataTuple(0, 0, this.root));
        // Result to return when found (if any)
        WormProtein result = null;
        HashSet<Pair<Integer, ProteinTrieNode>> checkedSet = new HashSet<>();
        int querySize = query.size();

        // Query the Trie until the queue of possible values is empty
        while (!queryQueue.isEmpty()) {
            // Initialization
            SearchDataTuple currentData = queryQueue.poll();
            int currentPosition = currentData.getQueryPos();
            int currentDeviation = currentData.getDeviation();
            ProteinTrieNode currentNode = currentData.getNode();
            EnumMap<AminoAcid, ProteinTrieNode> nodeChildren = currentNode.getChildren();

            // Check to see if we have deviated too much; if so, we're done
            if (currentDeviation > threshold) {
                break;
            }

            // Check to see if the node's product (if any) is a better match than the current value
            if (currentNode.isProduct()) {
                // Add in the mismatch penalty that would result given this match
                int delta = query.size() - currentPosition;
                int productDeviation = currentDeviation - (AminoAcid.BLOSSUM_GAP_PENALTY * delta);
                // Update the result and threshold if the product is valid
                if (productDeviation <= threshold) {
                    threshold = productDeviation;
                    result = currentNode.getProduct();
                }
                // If the delta is 0, there is no possible better alignment; we are done!
                if (delta == 0) {
                    break;
                }
            }

            // Record that the results of this score and position have now been tested
            checkedSet.add(new Pair<>(currentPosition, currentNode));

            // Discover valid next steps and add them to the queue
            Set<AminoAcid> nodeKeys = nodeChildren.keySet();
            // If we haven't already iterated through the query yet
            if (currentPosition < querySize) {
                // Match/mismatch options
                AminoAcid queryAcid = query.get(currentPosition);
                if (nodeChildren.get(query.get(currentPosition)) != null) {
                    queryQueue.add(new SearchDataTuple(currentPosition+1, currentDeviation, nodeChildren.get(queryAcid)));
                } else {
                    for (AminoAcid acid : nodeKeys) {
                        int tempDev = currentDeviation + AminoAcid.getBLOSSUMDeviation(acid, queryAcid);
                        if (tempDev < threshold) {
                            SearchDataTuple newVal = new SearchDataTuple(currentPosition+1, tempDev, nodeChildren.get(acid));
                            this.queueWithoutRedundancy(queryQueue, checkedSet, newVal);
                        }
                    }
                }
                // Insertion/deletion options
                int gapScore = currentDeviation - AminoAcid.BLOSSUM_GAP_PENALTY;
                if (gapScore <= threshold) {
                    // Insertion option (our position increases, but the node does not change)
                    SearchDataTuple newVal = new SearchDataTuple(currentPosition+1, gapScore, currentNode);
                    this.queueWithoutRedundancy(queryQueue, checkedSet, newVal);
                    // Deletion options (our node changes, but our position stays the same)
                    for (AminoAcid a : nodeKeys) {
                        newVal = new SearchDataTuple(currentPosition, gapScore, nodeChildren.get(a));
                        this.queueWithoutRedundancy(queryQueue, checkedSet, newVal);
                    }
                }
            }
            // Otherwise, only account for deletions (matches/mismatches/insertions are irrelevant)
            else {
                int gapScore = currentDeviation - AminoAcid.BLOSSUM_GAP_PENALTY;
                for (AminoAcid a : nodeKeys) {
                    SearchDataTuple newVal = new SearchDataTuple(currentPosition, gapScore, nodeChildren.get(a));
                    this.queueWithoutRedundancy(queryQueue, checkedSet, newVal);
                }
            }
        }
        return result;
    }

    // Helper function to reduce redundancy above
    private void queueWithoutRedundancy(PriorityQueue<SearchDataTuple> queue,
                                        HashSet<Pair<Integer, ProteinTrieNode>> set,
                                        SearchDataTuple val) {
        Pair<Integer, ProteinTrieNode> test_pair = new Pair<>(val.getDeviation(), val.getNode());
        if (!set.contains(test_pair)) {
            queue.add(val);
        }
    }

    public ArrayList<WormProtein> getAllProteins() {
        // Initialization
        LinkedList<ProteinTrieNode> nodeQueue = new LinkedList<>();
        nodeQueue.add(this.root);
        ArrayList<WormProtein> products = new ArrayList<>();

        // Full set querying
        while (!nodeQueue.isEmpty()) {
            ProteinTrieNode currentNode = nodeQueue.poll();
            if (currentNode.getProduct() != null) {
                products.add(currentNode.getProduct());
            }
            nodeQueue.addAll(currentNode.getChildren().values());
        }

        // Return the full set of elements
        return products;
    }

    public String toString() {
        return this.root.toTreeString();
    }
}
