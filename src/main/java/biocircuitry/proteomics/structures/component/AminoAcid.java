/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures.component;

import biocircuitry.BioCircuitry;
import biocircuitry.genomics.structures.Base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum AminoAcid {
    A ("Alanine", "Ala",
            new String[] {"GCT", "GCC", "GCA", "GCG"}),
    R ("Arginine", "Arg",
            new String[] {"CGT", "CGC", "CGA", "CGG", "AGA", "AGG"}),
    N ("Asparagine", "Asn",
            new String[] {"AAT", "AAC"}),
    D ("Aspartic Acid", "Asp",
            new String[] {"GAT", "GAC"}),
    C ("Cysteine", "Cys",
            new String[] {"TGT", "TGC"}),
    Q ("Glutamine", "Gln",
            new String[] {"CAA", "CAG"}),
    E ("Glutamic Acid", "Glu",
            new String[] {"GAA", "GAG"}),
    G ("Glycine", "Gly",
            new String[] {"GGT", "GGC", "GGA", "GGG"}),
    H ("Histidine", "His",
            new String[] {"CAT", "CAC"}),
    I ("Isoleucine", "Ile",
            new String[] {"ATT", "ATC", "ATA"}),
    L ("Leucine", "Leu",
            new String[] {"TTA", "TTG", "CTT", "CTC", "CTA", "CTG"}),
    K ("Lysine", "Lys",
            new String[] {"AAA", "AAG"}),
    M ("Methionine", "Met",
            new String[] {"ATG"}),
    F ("Phenylalanine", "Phe",
            new String[] {"TTT", "TTC"}),
    P ("Proline", "Pro",
            new String[] {"CCT", "CCC", "CCA", "CCG"}),
    S ("Serine", "Ser",
            new String[] {"TCT", "TCC", "TCA", "TCG", "AGT", "AGC"}),
    T ("Threonine", "Thr",
            new String[] {"ACT", "ACC", "ACA", "ACG"}),
    W ("Tryptophan", "Trp",
            new String[] {"TGG"}),
    Y ("Tyrosine", "Tyr",
            new String[] {"TAT", "TAC"}),
    V ("Valine", "Val",
            new String[] {"GTT", "GTC", "GTA", "GTG"}),
    X ("TERMINATE", "END",
            new String[] {"TAA", "TAG", "TGA"});

    private static Random rand = new Random(83642L);

    private String longName;
    private String shortName;
    private ArrayList<ArrayList<Base>> codons;

    /**
     * Convert a codon to its respective amino acid
     * @param codon The codon (set of three bases) to convert
     * @return The resulting amino acid, or null if an invalid codon is provided
     */
    public static AminoAcid codonToAminoAcid(ArrayList<Base> codon) {
        if (codon.size() !=3) {
            return null;
        }
        switch (codon.get(0)) {
            case A:
                switch (codon.get(1)) {
                    case A:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return N;
                        } else {
                            return K;
                        }
                    case T:
                        if (codon.get(2) == Base.G) {
                            return M;
                        } else {
                            return I;
                        }
                    case C:
                        return T;
                    case G:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return S;
                        } else {
                            return R;
                        }
                }
            case T:
                switch (codon.get(1)) {
                    case A:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return Y;
                        } else {
                            return X;
                        }
                    case T:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return F;
                        } else {
                            return L;
                        }
                    case C:
                        return S;
                    case G:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return C;
                        } else if (codon.get(2) == Base.A) {
                            return X;
                        } else {
                            return W;
                        }
                }
            case C:
                switch (codon.get(1)) {
                    case A:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return H;
                        } else {
                            return Q;
                        }
                    case T:
                        return L;
                    case C:
                        return P;
                    case G:
                        return R;
                }
            case G:
                switch (codon.get(1)) {
                    case A:
                        if (codon.get(2) == Base.T || codon.get(2) == Base.C) {
                            return D;
                        } else {
                            return E;
                        }
                    case T:
                        return V;
                    case C:
                        return A;
                    case G:
                        return G;
                }
        }
        return null;
    }

    AminoAcid(String longName, String shortName, String[] codons) {
        this.longName = longName;
        this.shortName = shortName;
        this.codons = new ArrayList<>(codons.length);
        for (String codon : codons) {
            this.codons.add(Base.getSequenceFromString(codon));
        }
    }

    /**
     * Construct the first valid protein sequence (if any) for the base sequence provided
     * @param sequence Sequence to use as the basis for the protein
     * @return An amino acid sequence (a protein)
     */
    public static ArrayList<AminoAcid> buildProtein(ArrayList<Base> sequence) {
        ArrayList<AminoAcid> proteinSequence = new ArrayList<>();
        // Offset by three to prevent codon overlap
        for (int i = 0; i < sequence.size()-2; i += 3) {
            ArrayList<Base> subSeq = new ArrayList<>(sequence.subList(i, i+3));
            AminoAcid newAcid = codonToAminoAcid(subSeq);
            if (newAcid == X) {
                return proteinSequence;
            } else if (newAcid == null) {
                return null;
            }
            proteinSequence.add(newAcid);
        }
        return null;
    }

    public String getLongName() {
        return this.longName;
    }

    public String getShortName() {
        return this.shortName;
    }

    public ArrayList<ArrayList<Base>> getCodons() {
        return this.codons;
    }

    // Build an amino acid sequence from its string representation
    public static ArrayList<AminoAcid> buildFromString(String baseString) {
        ArrayList<AminoAcid> retList = new ArrayList<>();
        char[] charArr = baseString.toUpperCase().toCharArray();
        for (char c : charArr) {
            retList.add(AminoAcid.valueOf(String.valueOf(c)));
        }
        return retList;
    }

    // Produce a random acid entirely from scratch
    public static AminoAcid randomAcid() {
        return randomAcid(rand);
    }

    // -- Reverse Translation helpers -- //
    public static ArrayList<Base> getGenericCodon(AminoAcid acid) {
        return acid.codons.get(0);
    }

    public static ArrayList<Base> getRandomCodon(AminoAcid acid) {
        return acid.codons.get(rand.nextInt(acid.codons.size()));
    }

    public static ArrayList<Base> reverseTranslateDefault(ArrayList<AminoAcid> acidSequence) {
        ArrayList<Base> result = new ArrayList<>();
        for (AminoAcid acid : acidSequence) {
            ArrayList<Base> codon = getGenericCodon(acid);
            result.addAll(codon);
        }
        result.addAll(X.codons.get(0));
        return result;
    }

    public static ArrayList<Base> reverseTranslateRandom(ArrayList<AminoAcid> acidSequence) {
        ArrayList<Base> result = new ArrayList<>();
        for (AminoAcid acid : acidSequence) {
            ArrayList<Base> codon = getRandomCodon(acid);
            result.addAll(codon);
        }
        result.addAll(X.codons.get(BioCircuitry.RANDOM.nextInt(X.codons.size())));
        return result;
    }

    // Produce a random acid from scratch, with a specified random instance
    public static AminoAcid randomAcid(Random rand) {
        int ordVal = rand.nextInt(19);
        return AminoAcid.values()[ordVal];
    }

    // BLOSSUM62 Scoring Matrix; taken from https://www.ncbi.nlm.nih.gov/Class/BLAST/BLOSUM62.txt
    private static final byte[][] BLOSSUM_MATRIX = new byte[][] {
            { 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -4},
            {-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -4},
            {-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3, -4},
            {-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3, -4},
            { 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -4},
            {-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2, -4},
            {-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2, -4},
            { 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -4},
            {-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3, -4},
            {-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -4},
            {-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4},
            {-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2, -4},
            {-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -4},
            {-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -4},
            {-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -4},
            { 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2, -4},
            { 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -4},
            {-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4},
            {-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -4},
            { 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -4},
            {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1}
    };

    public static final byte BLOSSUM_GAP_PENALTY = -4;

    // Simply queries the above w/ AminoAcid pairs (using the ordinal function)
    public static byte getBLOSSUMScore(AminoAcid a, AminoAcid b) {
        return BLOSSUM_MATRIX[a.ordinal()][b.ordinal()];
    }

    // Get the score difference between 'a' matching 'a' and 'a' matching 'b'
    public static byte getBLOSSUMDeviation(AminoAcid a, AminoAcid b) {
        return (byte) (getBLOSSUMScore(a, a) - getBLOSSUMScore(a, b));
    }

    // Get the maximum possible score for a sequence of AminoAcids
    public static int getMaxScore(List<AminoAcid> sequence) {
        int score = 0;
        for (AminoAcid a : sequence) {
            score += getBLOSSUMScore(a, a);
        }
        return score;
    }

}
