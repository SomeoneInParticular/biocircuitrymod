/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures.component;

import biocircuitry.proteomics.structures.WormProtein;

import java.util.*;


/**
 * A search Trie Node, based on Baedlung's search Trie implementation
 *
 * Stores data and directs movement through the Trie
 */
class ProteinTrieNode {
    private EnumMap<AminoAcid, ProteinTrieNode> children = new EnumMap<>(AminoAcid.class);
    private WormProtein product = null;

    EnumMap<AminoAcid, ProteinTrieNode> getChildren() {
        return children;
    }

    WormProtein getProduct() {
        return this.product;
    }

    void setProduct(WormProtein product) {
        this.product = product;
    }

    // Check if the node represents a project or not
    boolean isProduct() {
        return (this.product != null);
    }

    // Check if the node can be further queried or not
    boolean hasChildren() {
        return (!this.children.isEmpty());
    }

    String toTreeString() {
        StringBuilder buffer = new StringBuilder(50);
        buildPrettyString(buffer, "", "");
        return buffer.toString();
    }

    private void buildPrettyString(StringBuilder buffer, String prefix, String childrenPrefix) {
        buffer.append(prefix);
        buffer.append(this.product == null ? "┐" : this.product.toString());
        buffer.append('\n');
        Collection<ProteinTrieNode> nodeKeys = this.children.values();
        for (Iterator<ProteinTrieNode> iter = nodeKeys.iterator(); iter.hasNext();) {
            ProteinTrieNode next = iter.next();
            if (iter.hasNext()) {
                next.buildPrettyString(buffer, childrenPrefix + "├── ", childrenPrefix + "│   ");
            } else {
                next.buildPrettyString(buffer, childrenPrefix + "└── ", childrenPrefix + "    ");
            }
        }
    }
}