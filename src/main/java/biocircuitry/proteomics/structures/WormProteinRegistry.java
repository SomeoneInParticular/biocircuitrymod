/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;

import biocircuitry.BioCircuitry;
import biocircuitry.api.proteomics.IProteinRegistry;
import biocircuitry.proteomics.structures.component.AminoAcid;
import biocircuitry.proteomics.structures.component.ProteinTrie;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Registry for managing the proteins products instances in the mod
 * Uses a optimized search tree to
 */
public final class WormProteinRegistry
        implements IProteinRegistry<WormProtein, String, AminoAcid, WormMetabolite> {
    // Maps the protein's name to its product object
    private static final HashMap<String, WormProtein> proteinMap = new HashMap<>();
    // Allows approximate querying by protein sequence
    private static final ProteinTrie proteinTrie = new ProteinTrie();

    @Override
    public void add(WormProtein value) {
        // If the sequence already has a product associated with it, warn the user
        WormProtein existing = proteinTrie.findProtein(value.getSequence(), 0);
        if (existing != null) {
            BioCircuitry.LOGGER.warn("Warning: protein '" + existing.getLabel() +
                    "' was overridden by '" + value.getLabel() + "'.");
        }
        proteinMap.put(value.getLabel(), value);
        proteinTrie.insert(value.getSequence(), value);
    }

    // Convenience function for adding multiple proteins in one go
    public void addAll(WormProtein... values) {
        for (WormProtein value : values) {
            this.add(value);
        }
    }

    @Override
    public WormProtein getByLabel(String query) {
        return proteinMap.get(query);
    }

    @Override
    public WormProtein getBySequence(Collection<AminoAcid> query) {
        ArrayList<AminoAcid> acid = new ArrayList<>(query);
        return this.get(acid, 0);
    }

    /**
     * Alternative get for approximate matches
     * @param query Query sequence to search with
     * @param threshold Allowed score threshold before rejection
     * @return Closest matching WormProduct, null if no valid query within threshold found
     */
    public WormProtein get(ArrayList<AminoAcid> query, int threshold) {
        return proteinTrie.findProtein(query, threshold);
    }

    /**
     * Variant of {@link WormProteinRegistry#get(ArrayList, int)} which takes a query
     * string instead of an explicit AminoAcid sequence
     */
    public WormProtein get(String query, int threshold) {
        return this.get(AminoAcid.buildFromString(query), threshold);
    }

    @Override
    public Collection<WormProtein> getAll() {
        return proteinMap.values();
    }
}
