/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;

import biocircuitry.api.BioCircuitryAPI;
import biocircuitry.api.proteomics.IPathway;
import javafx.util.Pair;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import org.ojalgo.optimisation.Expression;
import org.ojalgo.optimisation.ExpressionsBasedModel;
import org.ojalgo.optimisation.Variable;
import org.ojalgo.structure.Structure1D;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Object representing a metabolic pathway for worms; determines
 * the "optimal" pathway given a set of input metabolites and
 * proteins based on whichever pathway maximizes the "life" of
 * the worm (as indicated by the "life value" provide by each
 * protein when active).
 *
 * Useful "byproducts" (items) of this pathway are tracked and
 * can be fetched as "products" for use in-game (IE when feeding
 * the worm)
 */
public class WormPathway implements IPathway<WormProtein, WormMetabolite>, INBTSerializable<NBTBase> {
    // Products produced by this pathway (if any) while active
    private ArrayList<ItemStack> costs;
    private ArrayList<ItemStack> products;

    // Constants which influence how the path should be built
    // Maximum number of times an individual protein can be used to convert materials
    private static final int PROT_MAX = 4;
    private static final int ITEM_MAX = 64;

    // Default 'biomass' for genes/resource sinks
    private static final double DEFAULT_GENE_WEIGHT = 0;
    private static final double DEFAULT_SINK_WEIGHT = -0.1;

    // NBT based Constructor
    public WormPathway(NBTTagCompound tag) {
        this.deserializeNBT(tag);
    }

    // Default Constructor
    public WormPathway(Collection<ItemStack> foodItems, Collection<WormProtein> proteins) {
        this.rebuildPathway(foodItems, proteins);
    }

    @Override
    public void rebuildPathway(Collection<ItemStack> food, Collection<WormProtein> proteins) {
        // Initialization
        ExpressionsBasedModel model = new ExpressionsBasedModel();
        ArrayList<Variable> foodVars = new ArrayList<>();
        ArrayList<Expression> productExprs = new ArrayList<>();
        // Creation of the limiter, which constrains the number of items consumable per cycle
        Expression limiter = model.addExpression("limiter");
        limiter.lower(0).upper(ITEM_MAX);
        // Add the input (food) expressions
        this.addFoodVariables(food, model, limiter, foodVars);
        // Add the transform (protein) expressions
        this.addProteinVariables(proteins, model, productExprs);
        // Add the "sinks" to prevent resource accumulation
        this.addSinkVariables(model);
        // Finally, determine the optimal path through the system
        System.out.println(model.maximise());
        // Determine the "costs" for this pathway
        this.determineCosts(foodVars);
        // Determine the "products" for this pathway
        this.determineProducts(model, productExprs);
    }

    /**
     * Helper function which adds food-item based variables to the model
     * @param itemStacks Items available to eat
     * @param model Model to update
     * @param limiter Expression representing the amount a given variable contributes to the consumption cap
     */
    private void addFoodVariables(Collection<ItemStack> itemStacks, ExpressionsBasedModel model, Expression limiter, Collection<Variable> foodVars) {
        // Update the model iteratively for each available item
        for (ItemStack stack : itemStacks) {
            // Metabolites provided by the food item
            ArrayList<Pair<WormMetabolite, Integer>> metaSets =
                    BioCircuitryAPI.WORM_FOOD_NUTRIENT_REGISTRY.getNutrients(stack.getItem());
            // If the food item lacks ANY nutrients, simply skip to the next item
            if (metaSets == null) {
                continue;
            }
            // Otherwise, initialize the representative variable
            ResourceLocation resource = stack.getItem().getRegistryName();
            assert resource != null;
            Variable var = model.addVariable(resource.toString());
            var.lower(0).upper(stack.getCount()); // 64 is the stack size for an item; thus, nothing can be larger
            // Tell the model that this counts towards the consumption cap
            limiter.set(var, 1);
            // As we can only consume full items, only integer counts of items are allowed
            var.integer(true);
            // Add it to our tracked lists of food variables
            foodVars.add(var);
            // Set the expressions appropriately for the variable, if any
            for (Pair<WormMetabolite, Integer> metaPair : metaSets) {
                createOrUpdateExpression(model, var, metaPair.getKey().getLabel(), metaPair.getValue());
            }
        }
    }

    /**
     * Helper function which adds protein based variables to the model
     * @param proteins Items available to eat
     * @param model Model to update
     */
    private void addProteinVariables(Collection<WormProtein> proteins, ExpressionsBasedModel model, Collection<Expression> productExprs) {
        // Update the model iteratively for each available item
        for (WormProtein protein : proteins) {
            // Metabolite map for the protein
            HashMap<WormMetabolite, Integer> reaction = protein.getReaction();
            // Variable representing the protein
            Variable protVar = model.addVariable(protein.getLabel());
            protVar.lower(0).upper(PROT_MAX);
            // Convert the mapped reaction values into an expression for use in the model
            for (Map.Entry<WormMetabolite, Integer> metaPair : reaction.entrySet()) {
                createOrUpdateExpression(model, protVar, metaPair.getKey().getLabel(), metaPair.getValue());
            }
            // Add the products this produces to the table as well, as integer expressions
            for (ItemStack stack : protein.getProducts()) {
                ResourceLocation resource = stack.getItem().getRegistryName();
                assert resource != null;
                String label = resource.toString();
                if (createOrUpdateExpression(model, protVar, label, stack.getCount())) {
                    Expression expr = model.getExpression(label);
                    expr.upper(ITEM_MAX);
                    productExprs.add(expr);
                }
            }
            // Add the proteins unique "weight" (biomass contribution)
            double biomass = protein.getBiomass();
            if (biomass == 0) {
                protVar.weight(DEFAULT_GENE_WEIGHT);
            } else {
                protVar.weight(protein.getBiomass());
            }
        }
    }

    /**
     * Helper with automatically adds "sinks" for excess metabolites, at an expense to the worm
     * @param model Model to add the sinks too
     */
    private void addSinkVariables(ExpressionsBasedModel model) {
        for (Expression expr : model.getExpressions()) {
            // If the expression is allowed to exist passively (its a product or unique in some way), no sink is needed
            if (expr.getUpperLimit().intValueExact() > 0) {
                continue;
            }
            Variable var = model.addVariable(expr.getName() + "_sink");
            expr.set(var, -1);
            var.lower(0).upper(100);
            var.weight(DEFAULT_SINK_WEIGHT);
        }
    }

    /**
     * Helper function which updates the item costs based on an optimized model's values
     * @param foodVars Variables representing input (food) items available for consumption
     */
    private void determineCosts(Collection<Variable> foodVars) {
        ArrayList<ItemStack> costs = new ArrayList<>();
        for (Variable food : foodVars) {
            int amount = food.getValue().intValue();
            // Only account for foods which were designated as consumed by the optimization
            if (amount > 0) {
                // Otherwise, find the amount consumed per ingestion cycle
                Item type = Item.REGISTRY.getObject(new ResourceLocation(food.getName()));
                assert type != null;
                ItemStack cost = new ItemStack(type, amount);
                costs.add(cost);
            }
        }
        this.costs = costs;
    }

    /**
     * Helper function which determines the item byproducts of a given optimized model
     * @param productExpr Product expressions representing an output item
     */
    private void determineProducts(ExpressionsBasedModel model, Collection<Expression> productExpr) {
        ArrayList<ItemStack> products = new ArrayList<>();
        for (Expression expr : productExpr) {
            int amount = 0;
            for (Structure1D.IntIndex index : expr.getLinearKeySet()) {
                Variable contribVar = model.getVariable(index);
                float proteinCount = contribVar.getValue().floatValue();
                int perProtein = expr.get(index).intValue();
                amount += Math.round(proteinCount * perProtein);
            }
            Item type = Item.REGISTRY.getObject(new ResourceLocation(expr.getName()));
            assert type != null;
            ItemStack product = new ItemStack(type, amount);
            if (amount > 0) {
                products.add(product);
            }
        }
        this.products = products;
    }

    /**
     * Creates or updates an expression for a variable in our model (as a shortcut)
     * @param model Model to update
     * @param var Variable to create/update
     * @param label Label to give said variable within the model
     * @param value The amount (of the variable) this expression should be valued at
     * @return True if a new expression was made, false otherwise
     */
    private boolean createOrUpdateExpression(ExpressionsBasedModel model, Variable var, String label, int value) {
        boolean retVal = false;
        Expression expr = model.getExpression(label);
        if (expr == null) {
            expr = model.addExpression(label);
            expr.lower(0).upper(0);
            retVal = true;
        }
        expr.set(var, value);
        return retVal;
    }

    @Override
    public ArrayList<ItemStack> getCosts() {
        return this.costs;
    }

    @Override
    public ArrayList<ItemStack> getProducts() {
        return this.products;
    }

    // -- NBT Serialization parameters -- //
    @Override
    public NBTBase serializeNBT() {
        // Cost serialization
        NBTTagList cost_tag = new NBTTagList();
        for (ItemStack c : costs) {
            cost_tag.appendTag(c.serializeNBT());
        }
        // Product serialization
        NBTTagList prod_tag = new NBTTagList();
        for (ItemStack p : products) {
            prod_tag.appendTag(p.serializeNBT());
        }
        // Fully formed super tag construction
        NBTTagCompound tag = new NBTTagCompound();
        tag.setTag("costs", cost_tag);
        tag.setTag("products", prod_tag);
        return tag;
    }

    @Override
    public void deserializeNBT(NBTBase nbt) {
        // Tag formatting
        NBTTagCompound tagCompound = (NBTTagCompound) nbt;
        // Costs retrieval
        NBTTagList cost_tag = tagCompound.getTagList("costs", 10);
        this.costs = new ArrayList<>(cost_tag.tagCount());
        for (NBTBase c : cost_tag) {
            this.costs.add(new ItemStack((NBTTagCompound) c));
        }
        // Product retrieval
        NBTTagList prod_tag = tagCompound.getTagList("products", 10);
        this.products = new ArrayList<>(prod_tag.tagCount());
        for (NBTBase p : prod_tag) {
            this.products.add(new ItemStack((NBTTagCompound) p));
        }
    }
}
