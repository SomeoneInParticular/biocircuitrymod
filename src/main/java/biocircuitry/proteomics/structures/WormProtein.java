/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;


import biocircuitry.api.proteomics.IProtein;
import biocircuitry.proteomics.structures.component.AminoAcid;
import net.minecraft.item.ItemStack;
import net.minecraftforge.registries.IForgeRegistryEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class WormProtein
        extends IForgeRegistryEntry.Impl<WormProtein>
        implements IProtein<String, AminoAcid, WormMetabolite> {
    // Sequence label, name, and sequence
    private String label;
    private ArrayList<AminoAcid> sequence;
    // Reaction this protein mediates
    private HashMap<WormMetabolite, Integer> reaction;
    // Biomass this protein creates while active (used by the optimizer)
    private int biomass;
    // What items the protein produces, if any, when active
    private ArrayList<ItemStack> products;

    // Generic constructor which creates limited proteins (for debug use ONLY)
    public WormProtein(String label, Collection<AminoAcid> sequence) {
        this(label, "");
        this.sequence = new ArrayList<>(sequence);
    }

    // Constructor which builds the sequence from a string for convenience
    public WormProtein(String label, String sequence) {
        this(label, sequence, new HashMap<>());
    }

    // Constructor which sets the reactions of the protein explicitly
    public WormProtein(String label, String sequence, HashMap<WormMetabolite, Integer> reactions) {
        this(label, sequence, 0, new ArrayList<>(), reactions);
    }

    // Constructor with both reactions and a biomass contribution
    public WormProtein(String label, String sequence, int biomass, HashMap<WormMetabolite, Integer> reactions) {
        this(label, sequence, biomass, new ArrayList<>(), reactions);
    }

    // Constructor with a reaction and product, but no biomass
    public WormProtein(String label, String sequence,
                       Collection<ItemStack> products, Map<WormMetabolite, Integer> reaction) {
        this(label, sequence, 0, products, reaction);
    }

    // Detailed constructor (w/ biomass, reactions, & products)
    public WormProtein(String label, String sequence, int biomass,
                       Collection<ItemStack> products, Map<WormMetabolite, Integer> reaction) {
        this.label = label;
        this.sequence = AminoAcid.buildFromString(sequence);
        this.biomass = biomass;
        this.products = new ArrayList<>(products);
        this.reaction = new HashMap<>(reaction);
    }

    public ArrayList<AminoAcid> getSequence() {
        return this.sequence;
    }

    @Override
    public HashMap<WormMetabolite, Integer> getReaction() {
        return this.reaction;
    }

    @Override
    public Integer getBiomass() {
        return this.biomass;
    }

    public Collection<ItemStack> getProducts() {
        return this.products;
    }

    public String getLabel() {
        return this.label;
    }

    public String toString() {
        return (this.label + "(" + this.sequence + ")");
    }
}
