/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;

import biocircuitry.api.proteomics.IFoodNutrientRegistry;
import javafx.util.Pair;
import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.HashMap;

public class WormFoodNutrientRegistry implements IFoodNutrientRegistry<WormMetabolite> {

    private HashMap<Item, ArrayList<Pair<WormMetabolite, Integer>>> nutrientMap = new HashMap<>();

    @Override
    public void registerNutrient(Item item, WormMetabolite nutrient, int amount) {
        Pair<WormMetabolite, Integer> nutrientPair = new Pair<>(nutrient, amount);
        ArrayList<Pair<WormMetabolite, Integer>> nutrientList = nutrientMap.computeIfAbsent(item, k -> new ArrayList<>());
        nutrientList.add(nutrientPair);
    }

    @Override
    public ArrayList<Pair<WormMetabolite, Integer>> getNutrients(Item item) {
        return nutrientMap.get(item);
    }
}
