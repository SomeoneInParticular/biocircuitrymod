/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;

import biocircuitry.api.proteomics.IMetabolite;

public class WormMetabolite implements IMetabolite {
    // String variable containers
    private String label, description;

    // Generic constructor
    public WormMetabolite(String label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return this.label;
    }
}
