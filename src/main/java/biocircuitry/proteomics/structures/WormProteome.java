/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;

import biocircuitry.api.BioCircuitryAPI;
import biocircuitry.api.proteomics.IProteome;
import biocircuitry.genomics.WormGenome;
import biocircuitry.genomics.structures.Base;
import biocircuitry.genomics.structures.WormChromosome;
import biocircuitry.proteomics.ModProteins;
import biocircuitry.proteomics.structures.component.AminoAcid;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import static biocircuitry.api.BioCircuitryAPI.WORM_PROTEIN_REGISTRY;

public class WormProteome implements IProteome<WormProtein, WormGenome> {
    // Tracked information
    private ArrayList<WormProtein> proteins;
    private String protString;
    private boolean isAlive;

    // BLOSSUM62 score deviation allowed for protein match
    private double MAX_PERCENT_DEVIATION = 0.1;

    // Generic constructor
    public WormProteome() {
        this.proteins = new ArrayList<>();
        this.protString = "NONE";
    }

    @Override
    public Collection<WormProtein> getProteins() {
        return this.proteins;
    }

    @Override
    public String getProtString() {
        return this.protString;
    }

    @Override
    public boolean isAlive() {
        return this.isAlive;
    }

    @Override
    public void buildFromGenome(WormGenome wormGenome) {
        // Initialization
        StringBuilder protString = new StringBuilder();
        ArrayList<WormProtein> protList = new ArrayList<>();
        ArrayList<WormChromosome> chroms = wormGenome.getSomaticChromosomes();
        for (WormChromosome chrom : chroms) {
            buildFromSequence(protString, protList, chrom);
        }
        // Update the proteome values
        this.proteins = protList;
        this.protString = protString.toString();
        // Check to see if this list of proteins allows the worms to survive
        this.isAlive = this.checkIfAlive();
    }

    /**
     * Check the current protein list to see if it contains
     * proteins required for life
     * @return True if the proteins allow for life, false otherwise
     */
    private boolean checkIfAlive() {
        HashSet<WormProtein> protSet = new HashSet<>(this.proteins);
        return protSet.containsAll(Arrays.asList(ModProteins.requiredForLife));
    }

    private static class SequenceDataTuple {
        int startPos;
        int endPos;
        String label;

        SequenceDataTuple(int startPos, int endPos, String label) {
            this.startPos = startPos;
            this.endPos = endPos;
            this.label = label;
        }
    }

    /**
     * Build the the proteome (String and Collection)
     * @param protString Representative string to build onto
     * @param protList Collection of proteins to add to the collection
     * @param chrom Chromosome to query from
     */
    private void buildFromSequence(StringBuilder protString, ArrayList<WormProtein> protList, WormChromosome chrom) {
        // Initialization
        ArrayList<SequenceDataTuple> protTupleList = new ArrayList<>();
        // Protein searching
        ArrayList<Base> sequence = chrom.getFullSequence();
        for (int i = 0; i < sequence.size()-2; i++) {
            // If a start codon was found, see if it codes for a known protein sequence
            if (sequence.get(i) == Base.A &&
                    sequence.get(i+1) == Base.T &&
                    sequence.get(i+2) == Base.G) {
                ArrayList<Base> subSeq = new ArrayList<>(sequence.subList(i, sequence.size()));
                ArrayList<AminoAcid> protSeq = AminoAcid.buildProtein(subSeq);
                // If it codes for a valid protein sequence, see if that sequence has been registered
                if (protSeq != null) {
                    WormProtein prot = WORM_PROTEIN_REGISTRY.get(protSeq, (int) (AminoAcid.getMaxScore(protSeq) * 0.05));
                    if (prot != null) {
                        /*
                        Each codon is coded by 3 bases (the 3x multiplier), but each
                        protein does not account for its own stop codon (the +1)
                         */
                        // Each codon is coded by 3 bases, hence the 3x multiplier, but excludes the stop codon
                        protTupleList.add(new SequenceDataTuple(i, (i+(protSeq.size()+1)*3), prot.getLabel()));
                        protList.add(prot);
                    }
                }
            }
        }
        // Build the protein string from our collected data
        int currentBufferPos = 0;
        // Add the initial value for the gene
        protString.append('[');
        // For each tuple, add to the string
        for (SequenceDataTuple tuple : protTupleList) {
            /*
            If the protein's start position is past the current buffer starting position,
            the protein has non-coding gap between it and the previous protein.
            This means we should display said gap's size, giving a CentiMorgan estimate
            to the user, prior to the label
             */
            if (tuple.startPos > currentBufferPos) {
                int approxCentiMorgan = (int) ((tuple.startPos - currentBufferPos) * 100.0 / sequence.size());
                protString.append("-(").append(approxCentiMorgan).append("cM)-");
            }
            /*
            Otherwise, the protein's coding region overlaps (or is directly besides) the
            prior protein's coding region, and thus no gap exists. For this we just
            insert a spacing character before the to-be-added label.
             */
            else {
                protString.append('|');
            }
            /*
            In either case, we need to add the protein's label and update the current
            buffer's starting position.
             */
            protString.append(tuple.label);
            currentBufferPos = tuple.endPos;
        }
        // Make sure to account for the the trailing sequence (if any)
        if (currentBufferPos < sequence.size()) {
            int approxCentiMorgan = (int) ((sequence.size() - currentBufferPos) * 100.0 / sequence.size());
            protString.append("-(").append(approxCentiMorgan).append("cM)-");
        } else {
            protString.append("|");
        }
        // Add the bounds to indicate the end of the chromosome
        protString.append(']');
        // Add a newline character to the string to clearly delineated between chromosomes
        protString.append('\n');
    }

    /**
     * Serialize relevant information down into NBT format
     * @return The NBT tag which represents the genome
     */
    public NBTTagCompound writeNBT() {
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setTag("protein_map", new NBTTagString(this.protString));
        NBTTagList protList = new NBTTagList();
        for (WormProtein protein : this.proteins) {
            NBTTagString seqString = new NBTTagString(protein.getLabel());
            protList.appendTag(seqString);
        }
        nbt.setTag("protein_list", protList);
        return nbt;
    }

    /**
     * Reconstruct the proteome based on the serialized NBT tag information
     * @param nbt NBT tag to base the reconstruction on
     */
    public void readNBT(NBTBase nbt) {
        NBTTagCompound compNBT = (NBTTagCompound) nbt;
        this.protString = compNBT.getString("protein_map");
        NBTTagList protListTag = compNBT.getTagList("protein_list", 8);
        this.proteins = new ArrayList<>();
        for (int i = 0; i < protListTag.tagCount(); i++) {
            String label = protListTag.getStringTagAt(i);
            WormProtein protein = WORM_PROTEIN_REGISTRY.getByLabel(label);
            if (protein != null) {
                this.proteins.add(protein);
            }
        }
    }
}
