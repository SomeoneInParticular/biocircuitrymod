/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics;

import biocircuitry.proteomics.structures.WormMetabolite;
import biocircuitry.proteomics.structures.WormProtein;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

import static biocircuitry.api.BioCircuitryAPI.WORM_PROTEIN_REGISTRY;

public class ModProteins {
    // -- ESSENTIAL -- //
    // Cell Wall Structure
    public static WormProtein walS = new WormProtein("walS", "MGRLYWEVIP");
    // Waste Filtration Structure
    public static WormProtein wafS = new WormProtein("wafS", "MEKFHLILSF");
    // RNA Synthase
    public static WormProtein rnaS = new WormProtein("rnaS", "MKLVTWVMQH");

    public static WormProtein[] requiredForLife = new WormProtein[] {walS, wafS, rnaS};

    // -- METABOLIC -- //
    // - Overworld - //
    // Glucose Reductase
    public static WormProtein gluR = new WormProtein("gluR", "MKSPREQSPS",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.glucose, -16);
                put(ModMetabolites.glycogen, 16);
    }});
    // ATP Synthase
    public static WormProtein atpS = new WormProtein("atpS", "MHCMAPPITE",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.glycogen, -16);
                put(ModMetabolites.atp, 16);
    }});
    // Myosin Filament structure
    public static WormProtein myoF = new WormProtein("myoF", "MHMGQTIYFF", 10,
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.atp, -4);
    }});
    // Chlorophyll Reductase
    public static WormProtein chlR = new WormProtein("chlR", "MIDLILPFIH",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.plant_fibre, -16);
                put(ModMetabolites.glucose, 16);
    }});
    // Ruminant Stomach
    public static WormProtein rumS = new WormProtein("rumS", "MEQGYPKCDS",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.wood_pulp, -32);
                put(ModMetabolites.cellulose, 32);
    }});
    // Cellulose Reductase
    public static WormProtein celR = new WormProtein("celR", "MRYYMAWEDK",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.cellulose, -16);
                put(ModMetabolites.glucose, 16);
    }});
    // Calcified Maw
    public static WormProtein calM = new WormProtein("calM", "MWFANQNTWH",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.flesh, -32);
                put(ModMetabolites.meat_slurry, 32);
    }});
    // Protease
    public static WormProtein proT = new WormProtein("proT", "MQLKTMYKMA",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.meat_slurry, -16);
                put(ModMetabolites.atp, 16);
    }});

    // -- MODIFIER -- //
    // - Overworld - //
    // Proteosynthase
    public static WormProtein proS = new WormProtein("proS", "MLAGSCGEAV",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.glucose, -16);
                put(ModMetabolites.meat_slurry, 16);
            }});
    // Hypermuscularization (porky)
    public static WormProtein hypM_P = new WormProtein("hypM-P", "MCERYDRGDV", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.PORKCHOP, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.meat_slurry, -32);
            }});
    // Hypermuscularization (beefy)
    public static WormProtein hypM_B = new WormProtein("hypM-B", "MCFWYDRGDV", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.BEEF, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.meat_slurry, -32);
            }});
    // Hypermuscularization (mutton)
    public static WormProtein hypM_M = new WormProtein("hypM-M", "MCERRDRGDV", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.MUTTON, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.meat_slurry, -24);
            }});
    // Hypermuscularization (rabbit)
    public static WormProtein hypM_R = new WormProtein("hypM-R", "MCEKYDRRDV", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.RABBIT, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.meat_slurry, -24);
            }});
    // Keratin Synthase
    public static WormProtein kerS = new WormProtein("kerS", "MPAMIIIQQY",
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.meat_slurry, -8);
                put(ModMetabolites.keratin, 8);
            }});
    // Defensive Padding (straight)
    public static WormProtein defP_S = new WormProtein("defP-S", "MVGLMFMHCN", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.STRING, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.keratin, -10);
            }});
    // Defensive Padding (curled)
    public static WormProtein defP_C = new WormProtein("defP-C", "MVILMFMHCN", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Blocks.WOOL, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.keratin, -16);
            }});
    // Defensive Padding (branched)
    public static WormProtein defP_B = new WormProtein("defP-B", "MVGEMFMMCN", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.FEATHER, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.keratin, -12);
            }});
    // Defensive Padding (epidermal)
    public static WormProtein defP_E = new WormProtein("defP-E", "MVGLMFPHMN", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.LEATHER, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.keratin, -32);
            }});
    // Defensive Padding (flaky)
    public static WormProtein defP_F = new WormProtein("defP-F", "MVGLFFMHCE", 1,
            new ArrayList<ItemStack>() {{
                add(new ItemStack(Items.PAPER, 1));
            }},
            new HashMap<WormMetabolite, Integer>() {{
                put(ModMetabolites.cellulose, -10);
            }});

    public static void init() {
        WORM_PROTEIN_REGISTRY.addAll(
                walS, wafS, rnaS, gluR, atpS, myoF, chlR, rumS, celR, calM, proT, proS, hypM_P, hypM_B, hypM_M, hypM_R,
                kerS, defP_S, defP_C, defP_B, defP_E, defP_F
        );
    }
}
