/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.networking;

import biocircuitry.BioCircuitry;
import biocircuitry.networking.feed_bin.FeedBinClientHandler;
import biocircuitry.networking.feed_bin.FeedBinGUIUpdateMessage;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkManager {
    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(BioCircuitry.MOD_ID);

    static int id = 0;

    public static void init() {
        INSTANCE.registerMessage(FeedBinClientHandler.class, FeedBinGUIUpdateMessage.class, id++, Side.CLIENT);
    }
}
