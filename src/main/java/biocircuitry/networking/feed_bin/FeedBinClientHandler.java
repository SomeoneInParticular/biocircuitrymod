/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.networking.feed_bin;

import biocircuitry.client.gui.feed_bin.FeedBinContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.inventory.Container;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FeedBinClientHandler implements IMessageHandler<FeedBinGUIUpdateMessage, IMessage> {

    @Override
    @SideOnly(Side.CLIENT)
    public IMessage onMessage(FeedBinGUIUpdateMessage message, MessageContext ctx) {
        // Update the progress on the client side
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        Container container = player.openContainer;
        if (container instanceof FeedBinContainer) {
            Minecraft.getMinecraft().addScheduledTask(
                    () -> container.updateProgressBar(0, message.currentProgress)
            );
        }
        // No return message needed
        return null;
    }
}
