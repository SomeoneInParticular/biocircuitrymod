/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.networking.feed_bin;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class FeedBinGUIUpdateMessage implements IMessage {
    // Generic default constructor
    public FeedBinGUIUpdateMessage(){}

    // -- Parameters -- //
    int currentProgress;
    // TODO: Add a state signal for whether the tile is evaluating new inputs or not

    // -- Constructor -- //
    public FeedBinGUIUpdateMessage(int currentProgress) {
        this.currentProgress = currentProgress;
    }

    // -- Parsing Methods -- //
    @Override
    public void fromBytes(ByteBuf buf) {
        this.currentProgress = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.currentProgress);
    }
}
