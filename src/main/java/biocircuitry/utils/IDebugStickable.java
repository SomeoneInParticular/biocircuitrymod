/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.utils;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface IDebugStickable {
    /**
     * Return a debug message when right-clicked with the debug stick
     * @return The debug message
     */
    String hitWithDebugStick(World world, BlockPos pos);
}
