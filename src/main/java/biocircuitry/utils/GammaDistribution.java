/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.utils;

import javax.management.InvalidAttributeValueException;
import java.lang.Math;
import java.util.Random;

/**
 * A rewrite of the Apache Commons GammaDistribution class, made to function statically
 * Preserves a single Random instance across all instances of the class, as well as getting a default shape and
 * scale parameter.
 *
 * Uses the following method for random value generation: Marsaglia and Tsang, A Simple Method for Generating
 * Gamma Variables. ACM Transactions on Mathematical Software, Volume 26 Issue 3, September, 2000.)
 */
public class GammaDistribution {
    // -- Attributes -- //
    // Random instance for generating random double values (uniform and normal)
    private static Random random = new Random();

    // a = shape (alpha), s = scale (theta), d and c = function variables
    private double a, d, c, s;

    // If the shape is offset (that is, was initially between 0.0 and 1.0)
    private boolean boosted;

    // -- Constructors -- //
    /**
     * Generic constructor, building a Gamma Distribution from a shape (a) and scale (k)
     * @param a The shape of the distribution (alpha)
     * @param s The scale parameter (theta)
     */
    public GammaDistribution(double a, double s) throws InvalidAttributeValueException {
        // Error checking
        if (a < 0.0) {
             throw new InvalidAttributeValueException("Invalid shape value (alpha); was less than 0");
        }
        if (s < 0.0) {
            throw new InvalidAttributeValueException("Invalid scale value (theta); was less than 0");
        }
        // Offsetting a, if need be
        if (a < 1.0) {
            this.a = a + 1.0;
            this.boosted = true;
        } else {
            this.a = a;
            this.boosted = false;
        }
        // Further attribute values set
        this.s = s;
        this.d = a - (1.0/3.0);
        this.c = 1 / (3 * Math.sqrt(d));
    }

    // -- Getters/Setters -- //
    /**
     * Set the shape value for this distribution
     * @param a Shape parameter (alpha) greater than 0.0 exclusive
     */
    public void setShape(double a) {
        if (a < 1.0) {
            this.a = a + 1.0;
            this.boosted = true;
        } else {
            this.a = a;
            this.boosted = false;
        }
        this.d = a - (1.0/3.0);
        this.c = 1 / (3*Math.sqrt(d));
    }

    /**
     * Get the shape value for this distribution
     * @return The shape value (alpha), a double value greater than 0.0 exclusive
     */
    public double getShape() {
        if (boosted) {
            return this.a - 1.0;
        } else {
            return this.a;
        }
    }

    /**
     * Set the scale value for this distribution
     * @param s Scale parameter (theta) greater than 0.0 exclusive
     */
    public void setScale(double s) {
        this.s = s;
    }

    /**
     * Get the shape value for this distribution
     * @return The scale value (theta), a double value greater than 0.0 exclusive
     */
    public double getScale() {
        return this.s;
    }

    /**
     * Set the random number generator GammaDistributions should use from now on. Use for debugging only!
     * @param rand The Random instance which should replace our current Random instance
     */
    public static void setGammaRandom(Random rand) {
        random = rand;
    }

    // -- Functions -- //
    /**
     * Based on the Apache Commons GammaDistribution sample function
     * https://bit.ly/2lfsxq3
     * @return A random double, generated with the current parameters
     */
    public double sample() {
        while (true) {
            // Initialization
            final double x = random.nextGaussian();
            final double v = (1 + c * x) * (1 + c * x) * (1 + c * x);

            // Re-initialize if the resulting values are invalid
            if (v <= 0) {
                continue;
            }

            // Further initialization
            final double x2 = x * x;
            final double u = random.nextDouble();

            // Squeeze
            if ((u < 1 - 0.0331 * x2 * x2) || (Math.log(u) < 0.5 * x2 + d * (1 - v + Math.log(v)))) {
                double ret = s * d * v;
                if (boosted) {
                    return ret * Math.pow(u, 1.0/(1.0-this.a));
                } else {
                    return ret;
                }
            }
        }
    }
}
