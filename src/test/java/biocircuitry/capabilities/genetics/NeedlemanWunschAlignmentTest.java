/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.capabilities.genetics;

import biocircuitry.genomics.structures.Base;
import biocircuitry.genomics.structures.NeedlemanWunschAlignment;
import biocircuitry.genomics.structures.WormChromosome;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class NeedlemanWunschAlignmentTest {
    // -- Scoring Tests -- //
    @Test
    public void getScoreIdentical() {
        // Confirm that scoring is correct, for identical sequences
        WormChromosome chromA = new WormChromosome("ACTG");
        WormChromosome chromB = new WormChromosome("ACTG");

        NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);
        Assert.assertEquals(alignment.getScore(), 8); // 4 * 2 = 8
    }

    @Test
    public void getScoreMismatch() {
        // Confirm that scoring is correct, given a mismatch
        WormChromosome chromA = new WormChromosome("ACTG");
        WormChromosome chromB = new WormChromosome("ACTT");

        NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);
        Assert.assertEquals(alignment.getScore(), 4); // 4 * 3 - 2 = 4
    }

    @Test
    public void getScoreShift() {
        // Confirm scoring is correct, given an insertion/deletion
        WormChromosome chromA = new WormChromosome("ACTG");
        WormChromosome chromB = new WormChromosome("ATG");

        NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);
        Assert.assertEquals(alignment.getScore(), 3); // 4 * 3 - 3 = 3
    }

    // -- Cross-over Evaluations -- //

    private static final List<Integer> DISTANCES = Arrays.asList(1, 2, 5, 10, 20, 50, 100);

    /**
     * Simulate recombination events near the beginning of the chromosome
     * See Voorrips et. al, 2012 for details
     */
    @Test
    public void simulateStartRecombination() {
        for (int spacerDist : DISTANCES) {
            // Setup
            int flankDist = 100 - spacerDist;
            double[] samples = new double[100];
            // Replications
            for (int i = 0; i < 100; i++) {
                // Replicate setup
                String spacer = new String(new char[spacerDist]).replace("\0", "T");
                String flank = new String(new char[flankDist]).replace("\0", "A");
                WormChromosome chromA = new WormChromosome("C" + spacer + "C" + flank);
                WormChromosome chromB = new WormChromosome("G" + spacer + "G" + flank);
                NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);
                int pos1 = 0;
                int pos2 = spacerDist + 1;
                // Replicate simulation
                int count = 0;
                for (int j = 0; j < 10000; j++) {
                    ArrayList<Base> recombSeq = alignment.makeRecombinant().getFullSequence();
                    // If a recombination occurred, count it
                    if (recombSeq.get(pos1) != recombSeq.get(pos2)) {
                        count++;
                    }
                }
                samples[i] = (double) count / 10000;
            }
            // Final evaluation for the distance
            double mean = 0;
            for (double val : samples) {
                mean += val;
            }
            mean /= 100;
            double std = 0;
            for (double val : samples) {
                double delta = val - mean;
                std += delta * delta;
            }
            std /= 99;
            System.out.println(String.format("Distance: %d, Mean: %f, STD: %f", spacerDist, mean, std));
        }
    }

    /**
     * Simulate recombination events near the middle of the chromosome
     * See Voorrips et. al, 2012 for details
     */
    @Test
    public void simulateCenterRecombination() {
        for (int spacerDist : DISTANCES) {
            // Setup
            int flankDist1 = 50 - spacerDist/2;
            int flankDist2 = 100 - spacerDist/2 - flankDist1; // Required to avoid rounding errors
            if (spacerDist % 2 == 0) {
                flankDist2--;
            }
            double[] samples = new double[100];
            // Replications
            for (int i = 0; i < 100; i++) {
                // Replicate setup
                String spacer = new String(new char[spacerDist]).replace("\0", "T");
                String flank1 = new String(new char[flankDist1]).replace("\0", "A");
                String flank2 = new String(new char[flankDist2]).replace("\0", "A");
                WormChromosome chromA = new WormChromosome(flank1 + "C" + spacer + "C" + flank2);
                WormChromosome chromB = new WormChromosome(flank1 + "G" + spacer + "G" + flank2);
                NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);
                int pos1 = flankDist1;
                int pos2 = flankDist1 + spacerDist + 1;
                // Replicate simulation
                int count = 0;
                for (int j = 0; j < 10000; j++) {
                    ArrayList<Base> recombSeq = alignment.makeRecombinant().getFullSequence();
                    // If a recombination occurred, count it
                    if (recombSeq.get(pos1) != recombSeq.get(pos2)) {
                        count++;
                    }
                }
                samples[i] = (double) count / 10000;
            }
            // Final evaluation for the distance
            double mean = 0;
            for (double val : samples) {
                mean += val;
            }
            mean /= 100;
            double std = 0;
            for (double val : samples) {
                double delta = val - mean;
                std += delta * delta;
            }
            std /= 99;
            System.out.println(String.format("Distance: %d, Mean: %f, STD: %f", spacerDist, mean, std));
        }
    }

    /**
     * Simulate recombination events near the beginning of the chromosome
     * See Voorrips et. al, 2012 for details
     */
    @Test
    public void simulateTailRecombination() {
        for (int spacerDist : DISTANCES) {
            // Setup
            int flankDist = 100 - spacerDist;
            double[] samples = new double[100];
            // Replications
            for (int i = 0; i < 100; i++) {
                // Replicate setup
                String spacer = new String(new char[spacerDist]).replace("\0", "T");
                String flank = new String(new char[flankDist]).replace("\0", "A");
                WormChromosome chromA = new WormChromosome(flank + "C" + spacer + "C");
                WormChromosome chromB = new WormChromosome(flank + "G" + spacer + "G");
                NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);
                int pos1 = flankDist;
                int pos2 = flankDist + spacerDist + 1;
                // Replicate simulation
                int count = 0;
                for (int j = 0; j < 10000; j++) {
                    ArrayList<Base> recombSeq = alignment.makeRecombinant().getFullSequence();
                    // If a recombination occurred, count it
                    if (recombSeq.get(pos1) != recombSeq.get(pos2)) {
                        count++;
                    }
                }
                samples[i] = (double) count / 10000;
            }
            // Final evaluation for the distance
            double mean = 0;
            for (double val : samples) {
                mean += val;
            }
            mean /= 100;
            double std = 0;
            for (double val : samples) {
                double delta = val - mean;
                std += delta * delta;
            }
            std /= 99;
            System.out.println(String.format("Distance: %d, Mean: %f, STD: %f", spacerDist, mean, std));
        }
    }

    @Test
    public void makeRecombinantSimpleTrio() {
        // Simulate a 3-allele recombination, to evalaute our simulation
        // Chromosomal setup
        int spacerLen = 50;
        int flankLen = 500;

        String spacer = new String(new char[spacerLen]).replace("\0", "T");

        String flank = new String(new char[flankLen]).replace("\0", "A");

        WormChromosome chromA = new WormChromosome(flank + "C" + spacer + "C" + spacer + "C" + flank);
        WormChromosome chromB = new WormChromosome(flank + "G" + spacer + "G" + spacer + "G" + flank);

        // Alignment construction
        NeedlemanWunschAlignment alignment = new NeedlemanWunschAlignment(chromA, chromB);

        // Assessment setup
        HashMap<String, Integer> counts = new HashMap<>();

        // No cross-overs (or equivalent)
        counts.put("CCC", 0);
        counts.put("GGG", 0);
        // Single cross-overs (or equivalent)
        counts.put("CCG", 0);
        counts.put("GGC", 0);
        counts.put("CGG", 0);
        counts.put("GCC", 0);
        // Double cross-overs (or-equivalent)
        counts.put("CGC", 0);
        counts.put("GCG", 0);
        // Backup flag, should errors arise
        counts.put("other", 0);

        // Key positions
        int headPos = flankLen;
        int midPos = flankLen+spacerLen+1;
        int tailPos = flankLen+spacerLen*2+2;

        // Generate 5000 recombinants, counting the results
        int iterCount = 5000;
        for (int i = 0; i < iterCount; i++) {
            WormChromosome recomChrom = alignment.makeRecombinant();
            try {
                Base first = recomChrom.getFullSequence().get(headPos);
                Base middle = recomChrom.getFullSequence().get(midPos);
                Base last = recomChrom.getFullSequence().get(tailPos);
                String key = first.name() + middle.name() + last.name();
                counts.replace(key, (counts.get(key) + 1));
            } catch (Exception e) {
                counts.replace("other", (counts.get("other") + 1));
            }
        }

        // Theoretical predictions (based on the Alberta Cirrulum's Chromosomal genetic unit's math)
        double expectedSingleSpliceOdds = (double) spacerLen / ((spacerLen + flankLen) * 2);
        double expectedDoubleSpliceOdds = expectedSingleSpliceOdds * expectedSingleSpliceOdds;
        expectedSingleSpliceOdds *= 2; // Doubling odds, since their are two possible scenarios!
        expectedSingleSpliceOdds -= expectedDoubleSpliceOdds; // A double splice is two single splices; thus they overlap, and we need to compensate
        double expectedZeroSpliceOdds = 1 - expectedSingleSpliceOdds - expectedDoubleSpliceOdds; // Only occurs when other events do not

        // Observed ratios
        double observedDoubleSpliceOdds = (double) (counts.get("CGC") + counts.get("GCG")) / (double) iterCount;
        double observedSingleSpliceOdds = (double) (counts.get("CCG") + counts.get("GGC") + counts.get("CGG") + counts.get("GCC"))
                / (double) iterCount;
        double observedZeroSpliceOdds = (double) (counts.get("CCC") + counts.get("GGG")) / (double) iterCount;

        System.out.println("Expected: " + expectedZeroSpliceOdds + ", Observed: " + observedZeroSpliceOdds);
        System.out.println("Expected: " + expectedSingleSpliceOdds + ", Observed: " + observedSingleSpliceOdds);
        System.out.println("Expected: " + expectedDoubleSpliceOdds + ", Observed: " + observedDoubleSpliceOdds);
    }
}