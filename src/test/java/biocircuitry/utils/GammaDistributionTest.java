/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.utils;

import org.junit.Assert;
import org.junit.Test;
import org.ojalgo.random.Gamma;

import javax.management.InvalidAttributeValueException;
import javax.sound.midi.SysexMessage;
import java.util.ArrayList;

import static org.junit.Assert.fail;

public class GammaDistributionTest {
    @Test
    public void assertDistroMatches() {
        // Build our distribution
        int shape = 2;
        int scale = 5;
        GammaDistribution dist = null;
        try {
            dist = new GammaDistribution(shape, scale);
        } catch (InvalidAttributeValueException e) {
            fail("Gamma distribution failed to be initialized");
        }
        // Get our assertable values
        double meanExp = shape*scale;
        double varExp = shape*scale*scale;
        // Get 500 samples to test with
        int numSamples = 1000;
        ArrayList<Double> samples = new ArrayList<>(numSamples);
        for (int i = 0; i < numSamples; i++) {
            samples.add(dist.sample());
        }
        // Evaluate mean and variance
        double meanObs = samples.stream().mapToDouble(Double::doubleValue).sum()/numSamples;
        double varObs = samples.stream().mapToDouble(x -> (x - meanObs) * (x - meanObs)).sum()/(numSamples-1);
        // Confirm that our samples are within reasonable accuracy
        Assert.assertTrue(
                "The mean observed from the samples of Gamma was more than 10% off expected! " +
                "Observed: " + meanObs  + ", Expected: " + meanExp + ". This may be a fluke!",
                0.5d > meanObs-meanExp && -0.5d < meanObs-meanExp);
        Assert.assertTrue(
                "The variance observed from the samples of Gamma was more than 10% off expected! " +
                        "Observed: " + varObs  + ", Expected: " + varExp + ". This may be a fluke!",
                5.0d > varObs-varExp && -5.0d < varObs-varExp);
    }
}