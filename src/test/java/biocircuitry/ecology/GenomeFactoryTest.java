/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.ecology;

import biocircuitry.BioCircuitry;
import biocircuitry.api.ecology.IGenomeFactory;
import biocircuitry.genomics.WormGenome;
import biocircuitry.genomics.structures.WormChromosome;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

public class GenomeFactoryTest {
    // The test class
    private static class TestFactory implements IGenomeFactory<WormGenome> {
        // Spacer sequence
        public static String spacer = new String(new char[20]).replace("\0", "T");
        public static String flank1 = new String(new char[10]).replace("\0", "A");
        public static String flank2 = new String(new char[30]).replace("\0", "A");

        // "Genetic" sequences
        public static int geneLen = 5;
        public static String geneA = new String(new char[geneLen]).replace("\0", "C");
        public static String geneB = new String(new char[geneLen]).replace("\0", "G");

        @Override
        public boolean canSpawnWorm(World world, BlockPos pos, EntityPlayer player, Item bait) {
            // Only allow spawning when bait is an Apple
            return (bait == Items.APPLE);
        }

        @Override
        public WormGenome generateGenome() {
            // Generates a simple diploid organism
            WormGenome genome = new WormGenome();
            genome.addSequence(buildChrom());
            genome.addSequence(buildChrom());
            return genome;
        }

        private WormChromosome buildChrom() {
            String sequence = flank1;
            // 25% chance of geneA, 75% chance of geneB
            if (BioCircuitry.RANDOM.nextBoolean() && BioCircuitry.RANDOM.nextBoolean()) {
                sequence += geneA;
            } else {
                sequence += geneB;
            }
            sequence += spacer;
            // 87.5% chance of geneA, 12.5% chance of geneB
            if (
                    BioCircuitry.RANDOM.nextBoolean() &&
                    BioCircuitry.RANDOM.nextBoolean() &&
                    BioCircuitry.RANDOM.nextBoolean()
            ) {
                sequence += geneB;
            } else {
                sequence += geneA;
            }
            sequence += flank2;
            return new WormChromosome(sequence);
        }
    }

    /**
     * Helper function to identify the key a given genome should be assigned
     * @param genome Genome to find the key for
     * @return The key identified for the genome
     */
    private String determineGenomeKey(WormGenome genome) {
        // Setup
        int samplePos1 = TestFactory.flank1.length();
        int samplePos2 = samplePos1 + TestFactory.geneLen + TestFactory.spacer.length();
        // Label generation
        List<WormChromosome> chromList = genome.getSomaticChromosomes();
        WormChromosome chrom1 = chromList.get(0);
        WormChromosome chrom2 = chromList.get(1);
        String alleleA1 = chrom1.getFullSequence().get(samplePos1).name();
        String alleleB1 = chrom1.getFullSequence().get(samplePos2).name();
        String alleleA2 = chrom2.getFullSequence().get(samplePos1).name();
        String alleleB2 = chrom2.getFullSequence().get(samplePos2).name();
        return String.format("%s%s/%s%s", alleleA1, alleleB1, alleleA2, alleleB2);
    }

    @Test
    public void testVariationDistribution() {
        // Generate our test factory
        TestFactory factory = new TestFactory();
        // Prepare our counting HashMap
        HashMap<String, Integer> categoryCounts = new HashMap<>();
        // Test 10,000 samples to assess the distributions
        for (int i = 0; i < 10000; i++) {
            WormGenome genome = factory.generateGenome();
            String label = determineGenomeKey(genome);
            // Generate a key in our dictionary, if it does not already exist
            if (!categoryCounts.containsKey(label)) {
                categoryCounts.put(label, 1);
            } else {
                categoryCounts.replace(label, categoryCounts.get(label)+1);
            }
        }
        System.out.println(categoryCounts);
    }
}