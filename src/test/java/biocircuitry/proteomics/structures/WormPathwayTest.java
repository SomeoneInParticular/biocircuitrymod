/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures;

import javafx.util.Pair;
import net.minecraft.init.Bootstrap;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ojalgo.netio.BasicLogger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static biocircuitry.api.BioCircuitryAPI.WORM_FOOD_NUTRIENT_REGISTRY;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;

public class WormPathwayTest {
    // Test metabolites for use in the system
    private final static WormMetabolite A = new WormMetabolite("A");
    private final static WormMetabolite B = new WormMetabolite("B");
    private final static WormMetabolite C = new WormMetabolite("C");
    private final static WormMetabolite D = new WormMetabolite("D");

    // Simulation test metabolites for distribution assessment
    private final static WormMetabolite sugar = new WormMetabolite("sugar");
    private final static WormMetabolite glucose = new WormMetabolite("glucose");
    private final static WormMetabolite fructose = new WormMetabolite("fructose");
    private final static WormMetabolite kitin = new WormMetabolite("kitin");

    @BeforeClass
    public static void init() {
        // Initialize the bootstrap so Items/ItemStacks can be worked with
        Bootstrap.register();
        // Addition of appropriate entries
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.APPLE, A, 1);
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.POTATO, B, 1);
    }

    @Before
    public void printBoundaryLine() {
        System.out.println("-----------------------------------------------------------------------------------------");
    }

    // -- CONSTRUCTION HELPERS -- //
    // Helper function to speed up the construction of arbitrary reactions
    private static HashMap<WormMetabolite, Integer> buildReaction(WormMetabolite[] metabolites, Integer[] values) {
        // Quick check to make sure that each array is the same size
        if (metabolites.length != values.length) {
            return null;
        }
        // And now build the HashMap proper
        HashMap<WormMetabolite, Integer> reaction = new HashMap<>();
        for (int i = 0; i < metabolites.length; i++) {
            reaction.put(metabolites[i], values[i]);
        }
        return reaction;
    }

    // Helper function to clean up nutrient map initialization
    private static ArrayList<Pair<WormMetabolite, Integer>> buildNutrientMapEntry(WormMetabolite[] metas, int[] values) {
        // Quick check to make sure that each array is the same size
        if (metas.length != values.length) {
            return null;
        }
        // Otherwise, build the collection of reactions
        ArrayList<Pair<WormMetabolite, Integer>> reaction = new ArrayList<>();
        for (int i = 0; i < metas.length; i++) {
            Pair<WormMetabolite, Integer> pair = new Pair<>(metas[i], values[i]);
            reaction.add(pair);
        }
        return reaction;
    }

    // -- TEST HELPERS -- //
    /**
     * Check a collection of stacks for the presence of a designated range of stack values
     * NOTE: This WILL modify the input collection; Create a copy before using this function if that matters!
     * @param col Collection to test
     * @param type Item type expected
     * @param min Minimum count of said item (inclusive)
     * @param max Maximum count of said item (inclusive)
     * @return true if a stack meeting the criteria exists in the collection, false otherwise
     */
    private static boolean checkStackCollectionForStack(Collection<ItemStack> col, Item type, int min, int max) {
        // Check every stack in the collection for the designated properties
        for (ItemStack stack : col) {
            if (stack.getItem().equals(type) && stack.getCount() <= max && stack.getCount() >= min) {
                col.remove(stack);
                return true;
            }
        }
        // Post cycle sanity check; if min = 0, its true whether a representative stack is present or note
        return min == 0;
    }

    // Wrapper of the above function for easy error message generation
    private static String checkStackMissing(Collection<ItemStack> col, Item type, int min, int max, String sourceName) {
        // If the stack lacked the designated type of itemstack
        String msg = "";
        if (!checkStackCollectionForStack(col, type, min, max)) {
            // Begin the error message
            // Add the appropriate designator
            msg = "\t" + sourceName + " collection lacks stack (" +
                    // Add the stack specific text
                    min + " <= " +
                    type.getRegistryName() + " <= " +
                    max + ")\n";
        }
        return msg;
    }

    // Actual error checking
    private static void assertNoStackErrors(
            Collection<ItemStack> leftoverResults, Collection<ItemStack> productResults, String errorMSG, WormPathway refPath
    ) {
        // Confirm no excess stacks were created
        if (!leftoverResults.isEmpty()) {
            errorMSG += "Excess costs:\n\t" + leftoverResults.toString();
        }
        if (!productResults.isEmpty()) {
            errorMSG += "\nExcess products:\n\t" + productResults.toString();
        }
        // If any errors were caught, report them
        boolean noErrors = errorMSG.equals("");
        if (!noErrors) {
            errorMSG += "\n\nPathway dump:" +
                    "\n\tCosts: " + refPath.getCosts() +
                    "\n\tProducts: " + refPath.getProducts() +
                    '\n';
        }
        assertTrue("Missing:\n" + errorMSG, noErrors);
    }


    @Test
    public void testConstructor_basic() {
        System.out.println("Basic pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> BtoLife = buildReaction(
                new WormMetabolite[] {B},
                new Integer[] {-1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protBLife = new WormProtein("BtoLife", "", 1,
                singletonList(new ItemStack(Items.IRON_INGOT)), BtoLife);
        // Finally, build the pathway for this setup
        long runTime = System.nanoTime();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 10)),
                asList(protAB, protBLife)
        );
        // Cache the runtime for manual review
        runTime = System.nanoTime() - runTime;
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        ArrayList<ItemStack> costResults = new ArrayList<>(pathway.getCosts());
        ArrayList<ItemStack> productResults = new ArrayList<>(pathway.getProducts());
        errorMSG += checkStackMissing(costResults, Items.APPLE, 4, 4, "Cost");
        errorMSG += checkStackMissing(productResults, Items.IRON_INGOT, 4, 4, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
        // Print out the run-time for review
        BasicLogger.debug("Run time: " + (runTime / 1000000) + "ms");
    }

    @Test
    public void testConstructor_merge() {
        System.out.println("Merging pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoC = buildReaction(
                new WormMetabolite[] {A, C},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> BtoD = buildReaction(
                new WormMetabolite[] {B, D},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> CDtoLife = buildReaction(
                new WormMetabolite[] {C, D},
                new Integer[] {-1, -1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAC = new WormProtein("AtoC", "", AtoC);
        WormProtein protBD = new WormProtein("BtoD", "", BtoD);
        WormProtein protCDLife = new WormProtein("CDtoLife", "", 1,
                singletonList(new ItemStack(Items.PAPER, 3)), CDtoLife);
        // Finally, build the pathway for this setup
        long runTime = System.nanoTime();
        WormPathway pathway = new WormPathway(
                asList(new ItemStack(Items.APPLE, 10), new ItemStack(Items.POTATO, 10)),
                asList(protAC, protBD, protCDLife)
        );
        // Cache the runtime for manual review
        runTime = System.nanoTime() - runTime;
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        ArrayList<ItemStack> costResults = new ArrayList<>(pathway.getCosts());
        ArrayList<ItemStack> productResults = new ArrayList<>(pathway.getProducts());
        errorMSG += checkStackMissing(costResults, Items.APPLE, 0, 4, "Cost");
        errorMSG += checkStackMissing(costResults, Items.POTATO, 0, 4, "Cost");
        errorMSG += checkStackMissing(productResults, Items.PAPER, 12, 12, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
        // Print out the run-time for review
        BasicLogger.debug("Run time: " + (runTime / 1000000) + "ms");
    }

    @Test
    public void testConstructor_split() {
        System.out.println("Splitting pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> AtoC = buildReaction(
                new WormMetabolite[] {A, C},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> BtoLife = buildReaction(
                new WormMetabolite[] {B},
                new Integer[] {-1}
        );
        HashMap<WormMetabolite, Integer> CtoLife = buildReaction(
                new WormMetabolite[] {C},
                new Integer[] {-1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protAC = new WormProtein("AtoC", "", AtoC);
        WormProtein protBLife = new WormProtein("BtoLife", "", 1,
                singletonList(new ItemStack(Items.FISH, 2)), BtoLife);
        WormProtein protCLife = new WormProtein("CtoLife", "", 1,
                singletonList(new ItemStack(Items.SUGAR, 2)), CtoLife);
        // Finally, build the pathway for this setup
        long runTime = System.nanoTime();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 10)),
                asList(protAB, protAC, protBLife, protCLife)
        );
        // Cache the runtime for manual review
        runTime = System.nanoTime() - runTime;
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        ArrayList<ItemStack> costResults = new ArrayList<>(pathway.getCosts());
        ArrayList<ItemStack> productResults = new ArrayList<>(pathway.getProducts());
        errorMSG += checkStackMissing(costResults, Items.APPLE, 8, 8, "Cost");
        errorMSG += checkStackMissing(productResults, Items.FISH, 0, 8, "Products");
        errorMSG += checkStackMissing(productResults, Items.SUGAR, 0, 8, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
        // Print out the run-time for review
        BasicLogger.debug("Run time: " + (runTime / 1000000) + "ms");
    }

    @Test
    public void testConstructor_invalid() {
        System.out.println("Invalid pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> CtoLife = buildReaction(
                new WormMetabolite[] {C},
                new Integer[] {-1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protC = new WormProtein("CtoLife", "", CtoLife);
        // Finally, build the pathway for this setup
        long runTime = System.nanoTime();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 10)),
                asList(protAB, protC)
        );
        // Cache the runtime for manual review
        runTime = System.nanoTime() - runTime;
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        ArrayList<ItemStack> costResults = new ArrayList<>(pathway.getCosts());
        ArrayList<ItemStack> productResults = new ArrayList<>(pathway.getProducts());
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
        // Print out the run-time for review
        BasicLogger.debug("Run time: " + (runTime / 1000000) + "ms");
    }

    @Test
    public void testDistribution() {
        // Designate nutrition of sugar input
        WORM_FOOD_NUTRIENT_REGISTRY.registerNutrient(Items.SUGAR, sugar, 1);

        // Build the reactions available to the worm
        HashMap<WormMetabolite, Integer> sugar_glucoA = buildReaction(
                new WormMetabolite[] {sugar, glucose, fructose},
                new Integer[] {-1, 3, 2}
        );
        HashMap<WormMetabolite, Integer> sugar_glucoa = buildReaction(
                new WormMetabolite[] {sugar, glucose},
                new Integer[] {-1, 5}
        );
        HashMap<WormMetabolite, Integer> gluco_kitin = buildReaction(
                new WormMetabolite[] {glucose, kitin},
                new Integer[] {-10, 10}
        );
        HashMap<WormMetabolite, Integer> kitin_leather = buildReaction(
                new WormMetabolite[] {kitin},
                new Integer[] {-5}
        );
        HashMap<WormMetabolite, Integer> kitin_string = buildReaction(
                new WormMetabolite[] {kitin},
                new Integer[] {-3}
        );

        // Build the proteins for these reactions
        WormProtein sugR_A = new WormProtein("sugR_A", "", 2, sugar_glucoA);
        WormProtein sugR_a = new WormProtein("sugR_a", "", 1, sugar_glucoa);
        WormProtein gluK = new WormProtein("gluK", "", gluco_kitin);
        WormProtein defP_E = new WormProtein("defP_E", "", 1,
                singletonList(new ItemStack(Items.LEATHER, 1)), kitin_leather);
        WormProtein defP_S = new WormProtein("defP_S", "", 3,
                singletonList(new ItemStack(Items.STRING, 1)), kitin_string);

        // Test the pathways for sugar input counts of 1 to 8
        for (int i = 1; i < 11; i++) {
            WormPathway pathway = new WormPathway(
                    singletonList(new ItemStack(Items.SUGAR, i)),
                    asList(sugR_A, sugR_a, gluK, defP_E, defP_S)
            );
            ArrayList<ItemStack> productResults = new ArrayList<>(pathway.getProducts());
            float stringCount = 0;
            float leatherCount = 0;
            for (ItemStack s : productResults) {
                if (s.getItem() == Items.STRING) {
                    stringCount = s.getCount();
                } else if (s.getItem() == Items.LEATHER) {
                    leatherCount = s.getCount();
                }
            }
            System.out.println(String.format("%d: %f, %f", i, stringCount, leatherCount));
        }
    }
}