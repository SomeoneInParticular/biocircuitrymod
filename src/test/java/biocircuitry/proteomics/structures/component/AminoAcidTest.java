/*
 * Copyright (c) 2019-2020 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.structures.component;

import biocircuitry.genomics.structures.Base;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class AminoAcidTest {
    /**
     * Confirm that the forward and reverse translation systems result
     * in the same original sequence (using the default translations)
     */
    @Test
    public void translationParallel_Normal() {
        // Should encode for "SVCC"
        ArrayList<AminoAcid> initAcid = AminoAcid.buildFromString("SVCCX");
        ArrayList<Base> interSeq = AminoAcid.reverseTranslateDefault(initAcid);
        ArrayList<AminoAcid> resultAcid = AminoAcid.buildProtein(interSeq);
        // Ignore the final position, as its an end codon that's lost in the pairing
        assert resultAcid != null;
        assertEquals(initAcid.subList(0, initAcid.size()-1).toString(), resultAcid.toString());
    }

    /**
     * Confirm that the forward and reverse translation systems result
     * in the same original sequence (using the random translations)
     */
    @Test
    public void translationParallel_Random() {
        // Should encode for "SVCC"
        ArrayList<AminoAcid> initAcid = AminoAcid.buildFromString("SVCCX");
        ArrayList<Base> interSeq = AminoAcid.reverseTranslateRandom(initAcid);
        ArrayList<AminoAcid> resultAcid = AminoAcid.buildProtein(interSeq);
        // Ignore the final position, as its an end codon that's lost in the pairing
        assert resultAcid != null;
        assertEquals(initAcid.subList(0, initAcid.size()-1).toString(), resultAcid.toString());
    }
}