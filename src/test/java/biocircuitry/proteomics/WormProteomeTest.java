/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics;

import biocircuitry.api.BioCircuitryAPI;
import biocircuitry.genomics.WormGenome;
import biocircuitry.genomics.structures.WormChromosome;
import biocircuitry.proteomics.structures.WormProtein;
import biocircuitry.proteomics.structures.WormProteinRegistry;
import biocircuitry.proteomics.structures.WormProteome;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class WormProteomeTest {
    @BeforeClass
    public static void setUpClass() {
        // Build the default protein registry
        WormProteinRegistry registry = BioCircuitryAPI.WORM_PROTEIN_REGISTRY;
        registry.add(new WormProtein("metH", "METH"));
        registry.add(new WormProtein("matH", "MATH"));
        registry.add(new WormProtein("metA", "META"));
        registry.add(new WormProtein("mamM", "MAMMAL"));
        registry.add(new WormProtein("ageM", "MYAGE"));
    }

    @Test
    public void test_getProteins_singleChromSingleProt() {
        // Setup
        WormChromosome chrom = new WormChromosome(
                "CATTGTAATAGCCACCAAAAGAGTG" + // 25bp (~38cM)
                        "atggaaacccattaa" + // 'METH*', 15bp
                        "GACGGCGTCGGTGCATAGCGGACTT"); // 25bp (~38cM)
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("metH")));
        Assert.assertEquals("-(~38cM)-metH-(~38cM)-\n", proteome.getProtString());

    }

    @Test
    public void test_getProteins_singleChromSpacedProts() {
        // Setup
        // NOTE; Due to rounding errors, this comes out as 22cM, though its actually 22.522... (23cM)
        WormChromosome chrom = new WormChromosome(
                "CATTGTAATAGCCACCAAAAGAGTG" + // 25bp (~22cM)
                        "atggaaacccattaa" + // 'METH*', 15bp
                        "GACGGCGTCGGTGCATAGCGGACTT" + // 25bp (~22cM)
                        "atggcgatgatggcgctgtaa" + // 'MAMMAL*', 21bp
                        "TCGGTCAGTCGCAATTCCTCACGAG"); // 25bp (~22cM)
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("metH")));
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("mamM")));
        Assert.assertEquals("-(~22cM)-metH-(~22cM)-mamM-(~22cM)-\n", proteome.getProtString());
    }

    @Test
    public void test_getProteins_singleChromUnspacedProts() {
        // Setup
        WormChromosome chrom = new WormChromosome(
                "CATTGTAATAGCCACCAAAAGAGTG" + // 25bp (~29cM)
                        "atggaaacccattaa" + // 'METH*', 15bp
                        "atggcgatgatggcgctgtaa" + // 'MAMMAL*', 21bp
                        "TCGGTCAGTCGCAATTCCTCACGAG"); // 25bp (~29cM)
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("metH")));
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("mamM")));
        Assert.assertEquals("-(~29cM)-metH|mamM-(~29cM)-\n", proteome.getProtString());
    }

    @Test
    public void test_getProteins_singleChromNoLead() {
        // Setup
        // Another floating point rounding error here (should be 63, ends up as 62)
        WormChromosome chrom = new WormChromosome(
                        "atggaaacccattaa" + // 'METH*', 15bp
                        "GACGGCGTCGGTGCATAGCGGACTT"); // 25bp (~62cM)
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("metH")));
        Assert.assertEquals("|metH-(~62cM)-\n", proteome.getProtString());
    }

    @Test
    public void test_getProteins_singleChromNoTrail() {
        // Setup
        // Another floating point rounding error here (should be 63, ends up as 62)
        WormChromosome chrom = new WormChromosome(
                "ACTGGTCCTGTTGAGCGCATCACTC" + // 25bp (~62cM)
                "atggaaacccattaa"); // 'METH*', 15bp
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("metH")));
        Assert.assertEquals("-(~62cM)-metH|\n", proteome.getProtString());
    }

    @Test
    public void test_getProteins_multiDupChrom() {
        // Setup
        WormChromosome chrom1 = new WormChromosome(
                "GACGGCGTCGGTGCATAGCGGACTT" + // 25bp (~36cM)
                        "atgtatgctggcgagtaa" + // 'MYAGE*', 15bp
                        "TCAATGTACAAGCAACCCGAGAAGG"); // 25bp (~36cM)
        WormChromosome chrom2 = new WormChromosome(
                "GACGGCGTCGGTGCATAGCGGACTT" + // 25bp (~36cM)
                        "atgtatgctggcgagtaa" + // 'MYAGE*', 15bp
                        "TCAATGTACAAGCAACCCGAGAAGG"); // 25bp (~36cM)
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom1);
        wormGenome.addSequence(chrom2);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("ageM")));
        Assert.assertEquals("-(~36cM)-ageM-(~36cM)-\n-(~36cM)-ageM-(~36cM)-\n", proteome.getProtString());
    }

    @Test
    public void test_getProteins_multiDiffChrom() {
        // Setup
        WormChromosome chrom1 = new WormChromosome(
                "GACGGCGTCGGTGCATAGCGGACTT" + // 25bp (~36cM)
                        "atgtatgctggcgagtaa" + // 'MYAGE*', 18bp
                        "TCAATGTACAAGCAACCCGAGAAGG"); // 25bp (~36cM)
        WormChromosome chrom2 = new WormChromosome(
                "GACGGCGTCGGTGCATAGCGGACTT" + // 25bp (~38cM)
                        "atggcgacccattaa" + // 'MATH*', 15bp
                        "TCAATGTACAAGCAACCCGAGAAGG"); // 25bp (~38cM)
        WormGenome wormGenome = new WormGenome();
        wormGenome.addSequence(chrom1);
        wormGenome.addSequence(chrom2);
        WormProteome proteome = new WormProteome();
        proteome.buildFromGenome(wormGenome);

        // Test
        Assert.assertTrue("The proteome was built, but did not detect the metH protein!",
                proteome.getProteins().contains(BioCircuitryAPI.WORM_PROTEIN_REGISTRY.getByLabel("ageM")));
        Assert.assertEquals("-(~36cM)-ageM-(~36cM)-\n-(~38cM)-matH-(~38cM)-\n", proteome.getProtString());
    }
}