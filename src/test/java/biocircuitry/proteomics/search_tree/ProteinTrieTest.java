/*
 * Copyright (c) 2019-2019 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package biocircuitry.proteomics.search_tree;

import biocircuitry.proteomics.structures.component.AminoAcid;
import biocircuitry.proteomics.structures.component.ProteinTrie;
import biocircuitry.proteomics.structures.WormProtein;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ProteinTrieTest {
    // Static randomizer for use below, with a set key for reliable testing (if desired)
    private static long seed = 1095813247;
    private static Random random = new Random(seed);

    @Test
    public void test_insert() {
        // Setup
        ProteinTrie trie = new ProteinTrie();

        // Test
        // Initial node addition
        WormProtein protein = new WormProtein("basic", "MGVVSGTALV");
        ArrayList<AminoAcid> acid = protein.getSequence();
        trie.insert(acid, protein);
        // Non-redundant node addition
        protein = new WormProtein("clone", "MGHNSHELFN");
        acid = protein.getSequence();
        trie.insert(acid, protein);
        // Redundant node addition (should overwrite B)
        protein = new WormProtein("rep", "MGHNSHELFN");
        acid = protein.getSequence();
        trie.insert(acid, protein);
        // Split redundant addition
        protein = new WormProtein("split", "MGHNSHTALV");
        acid = protein.getSequence();
        trie.insert(acid, protein);
        // Sequence deletion
        protein = new WormProtein("delete", "MGHNALV");
        acid = protein.getSequence();
        trie.insert(acid, protein);
        // Sequence insertion
        protein = new WormProtein("insert", "MGHNSHTALVTY");
        acid = protein.getSequence();
        trie.insert(acid, protein);
        // Flipped pair (the S and H)
        protein = new WormProtein("flip", "MGHNHSTALV");
        acid = protein.getSequence();
        trie.insert(acid, protein);
        // Transposed character (H over to behind L)
        protein = new WormProtein("trans", "MGNSHELHFN");
        acid = protein.getSequence();
        trie.insert(acid, protein);
    }

    @Test
    public void test_findProduct() {
        // Initialization
        WormProtein proteinA = new WormProtein("protA", "MGVVSGTALV");
        WormProtein proteinB = new WormProtein("protB", "MNNFTVSGIQ");
        WormProtein proteinC = new WormProtein("protC", "MGHNSHELFN");
        WormProtein proteinD = new WormProtein("protD", "MVLLLIPKSC");
        WormProtein proteinE = new WormProtein("protE", "MSLGKAGIRQ");
        ProteinTrie trie = new ProteinTrie();
        trie.insert(proteinA.getSequence(), proteinA);
        trie.insert(proteinB.getSequence(), proteinB);
        trie.insert(proteinC.getSequence(), proteinC);
        trie.insert(proteinD.getSequence(), proteinD);
        trie.insert(proteinE.getSequence(), proteinE);

        // Test
        // No valid match
        WormProtein result = trie.findProtein(AminoAcid.buildFromString("XXXXXXXXX"), 10);
        assert result == null;
        // Exact match of ProtA
        result = trie.findProtein(AminoAcid.buildFromString("MGVVSGTALV"), 0);
        Assert.assertSame(proteinA, result);
        // Slight mismatch of ProtB
        result = trie.findProtein(AminoAcid.buildFromString("MNMFTVSGIQ"), 10);
        Assert.assertSame(proteinB, result);
        // Massive mismatch of ProtC
        result = trie.findProtein(AminoAcid.buildFromString("MGHNWWTLFN"), 80); // Nothing but gaps
        Assert.assertSame(proteinC, result);
        // Internal deletion in ProtD
        result = trie.findProtein(AminoAcid.buildFromString("MVLKSC"), 16);
        Assert.assertSame(proteinD, result);
        // Internal insertion in ProtE
        result = trie.findProtein(AminoAcid.buildFromString("MSLGKLTMAGIRQ"), 12);
        Assert.assertSame(proteinE, result);
    }

    @Test
    public void test_getAllProteins() {
        ProteinTrie trie = new ProteinTrie();
        ArrayList<WormProtein> proteinList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ArrayList<AminoAcid> acid = new ArrayList<>(10);
            for (int j = 0; j < 10; j++) {
                acid.add(AminoAcid.randomAcid());
            }
            WormProtein prot = new WormProtein("", acid);
            proteinList.add(prot);
            trie.insert(prot.getSequence(), prot);
        }
        ArrayList<WormProtein> resultList = trie.getAllProteins();
        Assert.assertTrue("The result list did not contain all values placed into the tree" ,
                resultList.containsAll(proteinList));
        resultList.removeAll(proteinList);
        Assert.assertTrue("The result list contained values which were not placed into the tree",
                resultList.size() == 0);
    }

    // Helper function to randomly generate products
    private static ArrayList<AminoAcid> generateRandomProduct(int size) {
        // Setup and sanity check
        ArrayList<AminoAcid> newProduct = new ArrayList<>();
        if (size <= 0) {
            return newProduct;
        }
        // All products start with M
        newProduct.add(AminoAcid.M);
        // Build the rest of the product
        for (int i = 1; i < size; i++) {
            newProduct.add(AminoAcid.randomAcid(random));
        }
        return newProduct;
    }

    // Helper function to randomly modify products
    private static void mutateProduct(ArrayList<AminoAcid> product, int mutCount) {
        for (int i = 0; i < mutCount; i++) {
            int pos = random.nextInt(product.size()-1);
            product.set(pos+1, AminoAcid.randomAcid(random));
        }
    }

    @Test
    public void test_findProduct_largeRandom() {
        // Setup
        // Product Trie construction (1000 products of size 15; MUCH larger than we'd expecting)
        HashMap<String, WormProtein> initialMap = new HashMap<>(1000);
        ProteinTrie proteinTrie = new ProteinTrie();
        for (int i = 0; i < 1000; i++) {
            ArrayList<AminoAcid> sequence = generateRandomProduct(15);
            WormProtein protein = new WormProtein("prot"+i, sequence);
            initialMap.put(protein.getLabel(), protein);
            proteinTrie.insert(sequence, protein);
        }
        // Query list construction (30 queries, each size 15 with 1 or 2 mutations
        HashMap<String, ArrayList<AminoAcid>> queryMap = new HashMap<>(30);
        for (int i = 0; i < 30; i++) {
            int pos = random.nextInt(1000);
            ArrayList<AminoAcid> mutProduct = new ArrayList<>(initialMap.get("prot"+pos).getSequence());
            mutateProduct(mutProduct, random.nextInt(2) + 1);
            queryMap.put("prot"+pos, mutProduct);
        }
        // Query testing
        long measuredTime = System.nanoTime();
        HashMap<String, String> mismatches = new HashMap<>();
        for (Map.Entry<String, ArrayList<AminoAcid>> entry : queryMap.entrySet()) {
            int threshold = (int) Math.round(AminoAcid.getMaxScore(entry.getValue()) * 0.2); // 20% score variation allowed
            WormProtein result = proteinTrie.findProtein(entry.getValue(), threshold);
            if (result != null && (result != initialMap.get(entry.getKey()))) {
                mismatches.put(result.getLabel(), entry.getKey());
            }
        }
        // Check the running time, warning the user if it might be painful for players under horrific circumstances
        measuredTime = System.nanoTime() - measuredTime;
        if (measuredTime > 50_000_000) {
            System.out.println("WARNING: ProteinTrie querying was slow. This may lead to player frustration! " +
                    "Query time was " + measuredTime / 1_000_000.0 + "ms (ideally less than 50ms, at most 200ms)");
        }
        Assert.assertTrue("The number of mismatches was more than would usually be expected (1). " +
                        "This could be a fluke (due to the randomness of the test). Mismatches included: " +
                        mismatches.toString(),
                mismatches.size() <= 1);
    }
}